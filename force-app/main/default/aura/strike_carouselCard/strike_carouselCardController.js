({
    navigateToObject: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId":component.get("v.product").itemId,
            "slideDevName": "related"
        });
        navEvt.fire();
    }
})