({
	
	showHide : function (component, event, helper) {
     
        var a = component.get('v.parentCmp').find(event.getSource().get("v.name"));
        $A.util.toggleClass(a, "slds-is-open");
        
        var sectionStatus = component.get('v.sectionStatus');
        if( sectionStatus == 'closed'){
        	component.set("v.sectionStatus",'open');
        }
        else {
        	component.set("v.sectionStatus",'closed');
        }
        
     },
    
    
})