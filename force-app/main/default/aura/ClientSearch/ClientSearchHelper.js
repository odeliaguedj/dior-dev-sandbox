({
	getPageDetails : function(component, event, helper) { 
		let action = component.get("c.getPageDetails");
    
        action.setParams({
            recordId : component.get("v.recordId")
        });
    
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	
            	let pageDetails = a.getReturnValue();
            	let filter = component.get("v.searchParam");
            	if(pageDetails.currentCampaign){
            		//component.set('v.message', 'Campaigm successfully retrieved');
            		let campaign = pageDetails.currentCampaign;
            		component.set("v.Campaign", campaign);
					this.initRestrictions(component, event, helper);
				
            	}
            	else {
            		filter.userZone = pageDetails.currentUser.Zone__c;
            	}
            		
                component.set("v.currentUser", pageDetails.currentUser);
                
                component.set('v.searchParam', filter);
                helper.getClientStores(component, event, helper);
                helper.getUsers(component, event, helper);
                
                //console.log( a.getReturnValue());
                
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError()); 
                //this.showToaster ('Incredible Error');
            }
        });
    
        $A.enqueueAction(action);  
       
	},

	initRestrictions: function (component, event, helper) {
		let campaign = component.get("v.Campaign");
		if(campaign){
			let filter = component.get("v.searchParam");
			
			if(campaign.Store__c)
				filter.mainStoreIds = campaign.Store__c;
			if(campaign.Country__c)
				filter.userCountries =  campaign.Country__c;
			if(campaign.Zone__c)
				filter.userZone  = campaign.Zone__c;
			
			
			component.set('v.searchParam', filter);
		}
	}, 
	getClientStores : function(component, event, helper) {
//    	component.set("v.spinner", true);
		let action = component.get("c.getClientStores"); 
		//component.set("v.spinner", true);  
		let filter = component.get("v.searchParam");
		let campaign = component.get('v.Campaign');
		
        action.setParams({
            filter : JSON.stringify(filter),
            currentCampaign: campaign
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	let stores = JSON.parse( a.getReturnValue());
            	//component.set('v.message', 'Stores successfully retrieved');
            	
                let storesOptions = [];
                if(component.get("v.searchParam.userCountries") != undefined && component.get("v.searchParam.userCountries") != ''){
            	stores.forEach(elem => {
            		storesOptions.push( { label: elem.Name, value : elem.Id } );
                })
                }
                component.set("v.clientStoresList", storesOptions);
                //console.log( a.getReturnValue());
//                helper.helper.getUsers(component, event, helper);
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
            //component.set("v.spinner", false);  
        });
    
        $A.enqueueAction(action); 
    },
    
    getUsers : function(component, event, helper) {

    	component.set("v.spinner", true);
		let action = component.get("c.getUsers");
		let filter = component.get("v.searchParam");
		let campaign = component.get('v.Campaign');
		if(campaign && campaign.Store__c)
			filter.mainStoreIds = campaign.Store__c;
        action.setParams({
            filter : JSON.stringify(filter)
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	let users = JSON.parse( a.getReturnValue());
            	
            	let usersOptions = [];
            	users.forEach(elem => {
            		usersOptions.push( {label: elem.Name, value : elem.Id } );
            	})
                component.set("v.usersList", usersOptions);
                //console.log( a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
                console.log(a.getError());
                //this.showToaster ('Incredible Error');
            }
            component.set("v.spinner", false);
        });
    
        $A.enqueueAction(action);
    },
    
    getTransactionStores : function(component, event, helper) {
		let action = component.get("c.getTransactionStores");
    
		component.set("v.spinner", true);
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
            	let stores = JSON.parse( a.getReturnValue());
            	//component.set('v.message', 'Stores successfully retrieved');
            	let storesOptions = [];
            	stores.forEach(elem => {
            		storesOptions.push( { label: elem.Name, value : elem.Id } );
            	})
                component.set("v.transactionStoresList", storesOptions);
                //console.log( a.getReturnValue());
                
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
            component.set("v.spinner", false);
        });
    
        $A.enqueueAction(action);
    },

    
    getPickListVal: function(component,event,helper, objName,fieldName, optionsList, useCountry) {
        component.set("v.spinner", true);
        let action = component.get("c.getPickListVal");
        action.setParams({
            "obj": objName,
            "fld": fieldName,
            "fldEx":""
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = JSON.parse(response.getReturnValue());
                
                if (allValues != undefined && allValues.length > 0) {
                    allValues.forEach( elem => {
                        if(useCountry){
                            opts.push({label: elem.label, value : elem.label });
                        }
                        else{
                            opts.push({label: elem.label, value : elem.value });
                        }
                        
                    })        
                }

                component.set(optionsList, opts);
                if(objName == 'User' && fieldName == 'Zone__c')
                	helper.getDependentPickListVal(component,event,helper, {'sobjectType' : 'User'} ,  'Zone__c', 'Country__c', 'v.userContriesList');
                	
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    },
    
    getDependentPickListVal: function (component,event,helper,objDetails,controllingFieldAPI, dependingFieldAPI, depOptionsList){
    	component.set("v.spinner", true);
        let action = component.get("c.getDependentPickListVal");
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllingFieldAPI,
            'depfieldApiName': dependingFieldAPI 
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                let allValues = JSON.parse(response.getReturnValue());
                let searchParam = component.get('v.searchParam');
                let currentValues = allValues [searchParam.userZone];
                if (currentValues != undefined && currentValues.length > 0) {
                    currentValues.forEach( elem => {
                        opts.push({label: elem.label, value : elem.value });
                    })        
                }

                component.set(depOptionsList, opts);
                this.getClientStores(component,event,helper);
            }
            component.set("v.spinner", false);
        });
        $A.enqueueAction(action);
    },
    
    addClientsToCampaignHelper : function(component, event, helper) {
    	let clientsTable = component.find('clientsTable');
		clientsTable.setSelectedRows();
    	let action = component.get("c.addClientsToCampaign");
		let filter = component.get("v.searchParam");
		let campaign = component.get("v.Campaign");
		
		let nbOfShownClients = component.get('v.showCount');
		filter.selectAllClients = false;
		if (filter.selectedRows && filter.selectedRows.length == nbOfShownClients )
			filter.selectAllClients = true;
		component.set("v.spinner", true);
        action.setParams({
            filter : JSON.stringify(filter),
            campaignId: campaign.Id
        });
        
        
        action.setCallback(this, function(response) {
            component.set("v.spinner", false);
            if (response.getState() === "SUCCESS") {
           // component.set('v.message', 'Cleints successfully added');
            	let response2 =  response.getReturnValue();
            	component.set("v.campaignMembersSummary",response2);
            	console.log(response2.errorMessage);
            	var step = component.get("v.step");    
            	var step3 = "step3";
            	component.set("v.step",step3);
            	//console.log (step3);
            	//console.log (step);
                //console.log( response.getReturnValue());
            } else if (response.getState() === "ERROR") {
                $A.log("Errors", response.getError());
                console.log(response.getError());
            }
            
        });
    
        $A.enqueueAction(action);
		
    }
    
    ,
    "showToaster" : function(errorMessage) {
    	// Display toast message to indicate load status
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            
            toastEvent.setParams({
                    "title": "Error!",
                    "message": errorMessage,
                    "duration": 1000000
            });
            
            toastEvent.fire();
            
        }
        else {
        	console.log ('ERROR: '+ errorMessage);
        }
    }
    
    
     
})