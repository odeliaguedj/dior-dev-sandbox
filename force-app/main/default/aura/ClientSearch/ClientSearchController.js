({
	doInit : function (component, event, helper) {
        console.log('getting Campaign....')
        component.set('v.searchParam',{topExpectedCategory:[],segments:[],accountTypes:[],contactability:[],purchasedProductCategoriesH1:[],purchasedProductCategoriesH2:[],purchaseCurrency: 'EUR',didNotPurchaseProductCategories:[]});
		helper.getPageDetails(component, event, helper); 
		//helper.getClientStores(component, event, helper);
        //helper.getUsers(component, event, helper);
        helper.getTransactionStores(component, event, helper);
        helper.getPickListVal(component,event,helper, 'Contact','FdaTemperature__c', 'v.temperaturesList', false);
        helper.getPickListVal(component,event,helper, 'ClientAddress__c','Country__c', 'v.countriesList', true);
        helper.getPickListVal(component,event,helper, 'Contact','Nationality__c', 'v.nationalitiesList', false);
        helper.getPickListVal(component,event,helper, 'Contact','CurrentSegment__c', 'v.segmentsTypesList', false);
        helper.getPickListVal(component,event,helper, 'Contact', 'AccountType__c', 'v.accountTypesList', false);
        helper.getPickListVal(component,event,helper, 'Contact', 'FdaCategory1__c', 'v.fdaCategoryList1', false);
        helper.getPickListVal(component,event,helper, 'TransactionLine__c', 'ProductCategoryH1__c', 'v.fdaCategoryList4', false);
        helper.getPickListVal(component,event,helper, 'TransactionLine__c', 'ProductCategoryH1__c', 'v.productCategoriesListH1', false);
        helper.getPickListVal(component,event,helper, 'TransactionLine__c', 'ProductCategoryH2__c', 'v.productCategoriesListH2', false);
        helper.getPickListVal(component,event,helper, 'Contact', 'Birthmonth__c', 'v.birthmonthsList', false);
        helper.getPickListVal(component,event,helper, 'User', 'Zone__c', 'v.userZoneList', false);
        
     },
     
     showHide : function (component, event, helper) {
     
        var a = component.find(event.currentTarget.id);
        $A.util.toggleClass(a, "slds-is-open");
        
        var button = component.find(event.currentTarget.id+"button");
        
        var sectionStatus = button.get('v.iconName');
        if( sectionStatus == 'utility:right'){
        	button.set('v.iconName','utility:down');
        }
        else {
        	button.set('v.iconName','utility:right');
        }
     },
     
  
    
    printFilter: function (component, event, helper) {
        console.log(JSON.parse(JSON.stringify( component.get('v.searchParam'))));                             
    },
    
    getCountries: function (component, event, helper) {
    	helper.getDependentPickListVal(component,event,helper, {'sobjectType' : 'User'} ,  'Zone__c', 'Country__c', 'v.userContriesList');
    },
    
    getStores: function (component, event, helper) {
    	helper.getClientStores (component, event, helper);
    },
    
    getStoreUsers: function (component, event, helper) {
    	helper.getUsers (component, event, helper);
    },
    
    
    
    addClientsToCampaignAction : function (component, event, helper) {
    	helper.addClientsToCampaignHelper (component, event, helper);
    	
    },


    changeToStep2: function (component, event, helper) {
    	//component.set("v.spinner", true); 
        component.set("v.step","step2");
    },

    
     changeToStep1: function (component, event, helper) {
        //component.set("v.spinner", true); 
        component.set("v.step","step1");
    },
    
    deleteAllSections: function (component, event, helper) {
    	
        var deleteSection1 = component.get('c.deleteSection1');
        $A.enqueueAction(deleteSection1);
        var deleteSection2 = component.get('c.deleteSection2');
        $A.enqueueAction(deleteSection2);
        var deleteSection3 = component.get('c.deleteSection3');
        $A.enqueueAction(deleteSection3);
        var deleteSection4 = component.get('c.deleteSection4');
        $A.enqueueAction(deleteSection4);
        
        //helper.initRestrictions(component, event, helper);
        

    },
    
    deleteSection1 : function (component, event, helper) {
    	component.set("v.spinner", true);
    	let searchParam = component.get('v.searchParam');
        var childCountries = component.find('childCountries');
    	childCountries.deleteValuesLabel();
        var childNationalities = component.find('childNationalities');
    	childNationalities.deleteValuesLabel();
        var childBirthmonths = component.find('childBirthmonths');
    	childBirthmonths.deleteValuesLabel();
    	searchParam.lastName = '';
    	searchParam.firstName = '';
    	searchParam.zipCode = '';
    	searchParam.city = '';
    	searchParam.countries = '';
    	searchParam.nationalities = '';
    	searchParam.phone = '';
    	searchParam.email = '';
    	searchParam.birthmonths = '';
    	searchParam.birthdate = '';
    	component.set('v.searchParam', searchParam);  
    	//component.set("v.spinner", false);    
    },
    
    deleteSection2 : function (component, event, helper) {
    	component.set("v.spinner", true);
    	let searchParam = component.get('v.searchParam');
    	/*
    	var childStore = component.find('childStore');
    	childStore.deleteValuesLabel();
        */
        var childUser = component.find('childUser');
    	childUser.deleteValuesLabel();
    	
    	var childTopExpectedCategory = component.find('childTopExpectedCategory');
    	childTopExpectedCategory.deleteValuesLabel();
    	
    	var childTopExpectedCategory = component.find('childTopExpectedCategory');
    	childTopExpectedCategory.deleteValuesLabel();
    	
    	var childAccountTypes = component.find('childAccountTypes');
    	childAccountTypes.deleteValuesLabel();
    	
    	var childSegment = component.find('childSegment');
    	childSegment.deleteValuesLabel();
    	
    	var childDiorometer = component.find('childDiorometer');
    	childDiorometer.deleteValuesLabel();
    	
    	var childContactability = component.find('childContactability');
    	childContactability.deleteValuesLabel();
    	
    	searchParam.clientId = '';
    	//searchParam.mainStoreIds = '';
    	searchParam.mainSA = '';
    	searchParam.contactability = [];
    	searchParam.topExpectedCategory = [];
    	searchParam.segments = [];
    	searchParam.accountTypes = [];
    	searchParam.diorometer = '';
    	component.set('v.searchParam', searchParam);   
    	
    	helper.getUsers (component, event, helper);
    	//component.set("v.spinner", false);
    },
    
    deleteSection3 : function (component, event, helper) {
    	component.set("v.spinner", true);
      let searchParam = component.get('v.searchParam');
    	
    	var childFirstOrLastOrAllPurchase = component.find('childFirstOrLastOrAllPurchase');
    	childFirstOrLastOrAllPurchase.deleteValuesLabel();
    	
    	var childPurchasedProductCategoryH1 = component.find('childPurchasedProductCategoryH1');
    	childPurchasedProductCategoryH1.deleteValuesLabel();
    	
    	var childPurchasedProductCategoryH2 = component.find('childPurchasedProductCategoryH2');
    	childPurchasedProductCategoryH2.deleteValuesLabel();
    	
    	var childPurchasedStores = component.find('childPurchasedStores');
    	childPurchasedStores.deleteValuesLabel();
    	
    	var childPurchaseCurrency = component.find('childPurchaseCurrency');
    	childPurchaseCurrency.deleteValuesLabel();
    	
    	var childAtLeastTimes = component.find('childAtLeastTimes');
    	childAtLeastTimes.deleteValuesLabel();
    	
    	var childLifeTimeSpentOperator = component.find('childLifeTimeSpentOperator');        
    	childLifeTimeSpentOperator.deleteValuesLabel();
    	
    	var childYTDSpentOperator = component.find('childYTDSpentOperator');
    	childYTDSpentOperator.deleteValuesLabel();
    	
    	var childTwelveMonthsSpentOperator = component.find('childTwelveMonthsSpentOperator');
    	childTwelveMonthsSpentOperator.deleteValuesLabel();
    	
    	var childAtLeastTimes = component.find('childAtLeastTimes');
    	childAtLeastTimes.deleteValuesLabel();
    	
        searchParam.firstOrLastOrAllPurchase = '';
        searchParam.purchaseDateTo = '';
        searchParam.purchaseDateFrom = '';
    	searchParam.purchasedProductCategoriesH1 = [];
    	searchParam.purchasedProductCategoriesH2 = [];
    	searchParam.purchasedStores = '';
    	searchParam.purchaseCurrency = '';
    	searchParam.lifeTimeSpentOperator = '';
    	searchParam.lifeTimeSpent = '';
    	searchParam.YTDSpentOperator = '';
    	searchParam.YTDSpent = '';
    	searchParam.twelveMonthsSpentOperator = '';      
    	searchParam.twelveMonthsSpent = '';
    	searchParam.atLeastTimes = '';
        searchParam.purchasedSKU =[];
    	
    	component.set('v.searchParam', searchParam);
    	//component.set("v.spinner", false);
    },
    
   
 	deleteSection4 : function (component, event, helper) {
 	
 		component.set("v.spinner", true);
 		let searchParam = component.get('v.searchParam');
      
      var childDidNotPurchaseProductCategories = component.find('childDidNotPurchaseProductCategories');
    	childDidNotPurchaseProductCategories.deleteValuesLabel();
      
      searchParam.didNotPurchaseDateSince = '';
      searchParam.didNotPurchaseProductCategories = [];
      searchParam.didNotPurchasedSKU= [];
      
      component.set('v.searchParam', searchParam);
      //component.set("v.spinner", false);
    },
       
    
 // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
   
    
     
})