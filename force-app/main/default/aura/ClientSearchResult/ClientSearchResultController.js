({
	init: function (cmp, event, helper) {
		let currentUser = cmp.get('v.currentUser');
        cmp.set('v.columns', [
            { label: 'Name',fieldName: 'link', sortable:true,type: 'url', typeAttributes: { label : {fieldName:'Name'},target: '_blank'  } },
            { label: 'Name ('+ currentUser.zone + ')', fieldName: 'zoneName' ,  type: 'text', sortable:true},
            { label: 'SEGMENT', fieldName: 'segment', type: 'text', sortable:true},
            { label: 'Amount To Next Segment (eur)', fieldName: 'AmountToNextSegmentEUR__c', type: 'number', cellAttributes: { alignment: 'left' }, sortable:true},
            { label: 'MAIN S.A', fieldName: 'OwnerName', type: 'text', sortable:true},
            { label: 'MAIN BOUTIQUE', fieldName: 'StoreName', type: 'text', sortable:true},
            { label: '12 MONTHS SPENT (eur)', fieldName: 'TwelveMonthsSpentEUR__c',type: 'currency', cellAttributes: { alignment: 'left' }, sortable:true}
        ]);
        
        helper.getClients(cmp,false);
        helper.getClientsCount(cmp);
        cmp.set('v.loading', true);
//        helper.getClientsIds (cmp);
    }, 
    setSelectedRows : function (cmp,event,helper){
    	
    	let cmpSelectedRows = cmp.find('clientsTable').getSelectedRows();
    	let selectedRowsIds = [];
    	cmpSelectedRows.forEach(elem=>{
    		selectedRowsIds.push(elem.Id); 
    	});
    	cmp.set('v.searchParam.selectedRows', selectedRowsIds);
    	
    	
    },
    onRowSelected : function (cmp,event,helper){
    	let cmpSelectedRows = cmp.find('clientsTable').getSelectedRows();
    	cmp.set('v.nbOfSelectedRows', cmpSelectedRows.length);
    },
    //sortable
    updateColumnSorting: function (cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        // assign the latest attribute with the sorted column fieldName and sorted direction
        cmp.set("v.sortedBy", fieldName); 
        cmp.set("v.sortedDirection", sortDirection);
        helper.sortData(cmp, fieldName, sortDirection); 
    },

    changeToStep1: function (component, event, helper) {
        component.set("v.step","step1");
        component.set('v.loading', false);
    },


    loadMoreClients : function (cmp, event, helper) {
    	let loadingMore = cmp.get('v.loadingMore');
    	
    	if(!loadingMore){
    		cmp.set('v.loadingMore',true);
    		helper.getClients(cmp,true);
    	}	
    }
    
})