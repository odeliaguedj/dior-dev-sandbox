({
	getClients : function(cmp,loadMore) {
        
        
        var action = cmp.get('c.getClients');
        let filter = cmp.get("v.searchParam");
        let clients = cmp.get('v.data');
        
        
        
        filter.lastIdInList = '';
        if(loadMore)
        	filter.lastIdInList = clients[clients.length - 1 ].Id;
        
        action.setParams({
            filter : JSON.stringify(filter)
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                let clients = JSON.parse(response.getReturnValue());
                clients = this.setClientsCrossFields (cmp,clients);
                
                let rows = cmp.get('v.data') || [] ;//existing rows
                let rowsLength = rows.length;
                rows = rows.concat(clients);
                cmp.set('v.data', rows );
                cmp.set('v.showCount', rows.length );
                
                let totalCount = cmp.get('v.totalCount');
                let countShow = rows.length;
                if (countShow == totalCount || clients.length < 50 ){
            		cmp.set('v.enableInfiniteLoading',false);
            	}
                
                if( !cmp.get('v.isSearchContext') ){
	                let selectedRows = cmp.find('clientsTable').getSelectedRows();
	            	
	            	let selectedRowsIds = [];
	            	selectedRows.forEach(elem => {
	                	selectedRowsIds.push(elem.Id);
	                })
	                if (rowsLength == selectedRows.length){
		            	clients.forEach(elem => {
		                	selectedRowsIds.push(elem.Id);
		                })
	                }
	                cmp.set('v.nbOfSelectedRows', selectedRowsIds.length);
	            	cmp.set('v.preselectedRows', selectedRowsIds);
            	}
            	let fieldName = cmp.get("v.sortedBy");
                let sortDirection = cmp.get("v.sortedDirection");
            	this.sortData(cmp, fieldName, sortDirection);
            	cmp.set('v.loading', false);
            
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
            cmp.set('v.loadingMore',false);  
             
        }));
        
        $A.enqueueAction(action);

    },
    
//    getClientsIds : function(cmp) {
//        var action = cmp.get('c.getClientsIds');
//        let filter = cmp.get("v.searchParam");
//        
//        action.setParams({
//            filter : JSON.stringify(filter)
//        });
//       action.setCallback(this, $A.getCallback(function (response) {
//            var state = response.getState();
//            if (state === "SUCCESS") {
//                let clients = JSON.parse(response.getReturnValue());
//    		  
//				} else if (state === "ERROR") {
//                var errors = response.getError();
//                console.error(errors);
//            }
//        }));
//        $A.enqueueAction(action);
//        
//    },
  getClientsCount : function(cmp) {
        var action = cmp.get('c.getClientsCount');
        let filter = cmp.get("v.searchParam");
        let queryCount = cmp.get("v.totalCount");
        
        action.setParams({ 
            filter : JSON.stringify(filter)
        });
        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                let queryCount = JSON.parse(response.getReturnValue());
                cmp.set('v.totalCount', queryCount );
                
           } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
        
    },
    
    setClientsCrossFields : function (cmp,clients) {
        
        let currentUser = cmp.get('v.currentUser');
        
        clients.forEach(client => {
        	
        	client.zoneName = ( client ["FirstName" + currentUser.Zone__c + "__c"] || '') + " " +  ( client[ "LastName" + currentUser.Zone__c + "__c" ] || '');
            client.OwnerName = client.Owner.Name;
            client.link = '/' + client.Id;
            if(client.PreferredStore1__r)
                client.StoreName = client.PreferredStore1__r.Name;
            if(client.Account)
                client.ClientName = client.Account.Name; 
            
        });
        return clients;
    },

    sortData: function (cmp, fieldName, sortDirection) {
        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set("v.data", data);
    },
    
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
    
})