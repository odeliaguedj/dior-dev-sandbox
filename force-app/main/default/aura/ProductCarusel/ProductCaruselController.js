({

    getImages: function (component, event, helper) {

        var action = component.get("c.getImagesData");
        action.setParam("clientId", component.get("v.recordId"));
        action.setCallback(this, function(response){
            console.log(response.getReturnValue());
            if(response.getState() === "SUCCESS"){
                component.set("v.productList",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }

})