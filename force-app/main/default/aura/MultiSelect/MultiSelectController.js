({

    doInit : function (component, event, helper) {
    	let label= '';
    	let options = component.get('v.options');
    	
    	options.forEach(opt => { 
    		if(opt.selected){
    			label += opt.label + '; ';
    		}
    		
    	});
    	
    	component.set('v.inputValue',label);
    
    },

    setValue : function (component,event,helper){
    	let value = '';
    	let label= '';
    	let selectedValue = event.currentTarget.id;
    	let options = component.get('v.options');
    	let inputValue=component.get('v.inputValue');
    	
    	options.forEach(opt => { 
    	
    		
    		if(opt.value == selectedValue && opt.selected){
    			opt.selected = false;
    		}
    		else if(opt.value == selectedValue && !opt.selected){
    			opt.selected = true;
    		}
    		if(opt.selected){
    			value += opt.value + ';';
    			label += opt.label + '; ';
    		}
    		
    	});
    	   
    	component.set('v.value',value);
    	component.set('v.options',options);
    	component.set('v.inputValue',label);
    },
    
    openDropdown : function (component, event, helper){
    	$A.util.toggleClass(component.find('dropdown'),'slds-is-open'); 
    },
    
    initLabel : function (component,event,helper){ 
    	if(component.get('v.value') == ''  )
    		component.set('v.inputValue', '');
    }
    
   
    		
})