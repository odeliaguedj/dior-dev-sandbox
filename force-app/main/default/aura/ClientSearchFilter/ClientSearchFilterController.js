({
    init: function (cmp) {
    
    },
    onRender: function (component,event,helper){
    	 let elems = component.getElements();
    	 Array.from(document.getElementsByTagName('input')).forEach(elem=>{elem.setAttribute ( 'autocomplete','off' )});
     },
    setButtonGroupValue : function (component,event,helper){
    	let value = '';
    	let selectedValue = event.getSource().get("v.value");
    	let options = component.get('v.options');
    	options.forEach(opt => { 
    		if(opt.value == selectedValue && opt.selected){
    			opt.selected = false;
    		}
    		else if(opt.value == selectedValue && !opt.selected){
    			opt.selected = true;
    		}
    		if(opt.selected)
    			value += opt.value + ';';
    	});
    	
    	component.set('v.value',value);
    	component.set('v.options',options);
    },
    
    setButtonGroupOnlyOnceValue : function (component,event,helper){
    	let value = '';
    	let selectedValue = event.getSource().get("v.value");
    	let options = component.get('v.options');
    	options.forEach(opt => { 
    		if(opt.value == selectedValue){
    			opt.selected = true;
    		}
    		else 
    			opt.selected = false;
    		
    		if(opt.selected)
    			value = opt.value ;
    	});
    	
    	component.set('v.value',value);
    	component.set('v.options',options);
    },
    
    deleteValuesLabel : function (component, event, helper) {
		component.set('v.selectedValuesLabel','');         
        let options = component.get('v.options');
        options.forEach(opt => { 	
            opt.selected = false;  			
    	});
        component.set('v.options',options);
        
    }
    
    /*,
    handleChange: function (cmp, event) {
        // This will contain the string of the "value" attribute of the selected option
        var selectedOptionValue = event.getParam("value");
        alert("Option selected with value: '" + selectedOptionValue + "'");
    }*/
})