({
	getCampaign : function(component,event,helper) {
		console.log('getting Campaign....')
        
      
        var action = component.get("c.getCampaign");
    
        action.setParams({
            recordId : component.get("v.recordId")
        });
    
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.campaign", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
    
        $A.enqueueAction(action);

	}
})