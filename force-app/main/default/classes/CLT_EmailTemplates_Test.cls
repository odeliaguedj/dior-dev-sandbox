@isTest
public class CLT_EmailTemplates_Test {
    
    @isTest
    private static void retrieveDeleteReturnNull() {
        final CLT_EmailTemplates clt = new CLT_EmailTemplates();
        system.assertEquals(null, clt.retrieveDelete(new Map<String, String>()));
    }
    
    @isTest
    private static void retrievePostReturnNull() {
        final CLT_EmailTemplates clt = new CLT_EmailTemplates();
        system.assertEquals(null, clt.retrievePost(new Map<String, String>(), new Map<String, Object>()));
    }
    
     @isTest
    public static void test() {
        Map<String, String> params=new Map<String, String>();
        Map<String,Object> body =new Map<String,Object>();
        CLT_EmailTemplates et = new CLT_EmailTemplates();
        Test.startTest();
        Object obj1= et.retrieveDelete(params);
        Object obj2= et.retrievePost(params,body);
        Object obj3= et.retrieveGet(params);
        Object obj4= et.retrieveGet(new Map<String, String>{'runningUser' => '0051t000001wvzmAAA'});

        Test.stopTest();
    }
}