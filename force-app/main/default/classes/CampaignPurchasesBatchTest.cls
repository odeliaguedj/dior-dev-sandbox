@isTest
global class CampaignPurchasesBatchTest 
{
    public static String CRON_EXP = '0 0 3 ? * * *';
    @testSetup static void createRecords() {
		TestUtils.createPersonAccounts();
		TestUtils.createTransactionLines();
        //TestUtils.createTransactionLines();

	}
    public static testmethod void TestCampaignPurchasesBatch(){
            Test.startTest();
            CampaignPurchasesBatch btb = new CampaignPurchasesBatch();
            Id batchId = Database.executeBatch(btb);
            Test.stopTest();
        }
        
        public static testmethod void TestCampaignPurchasesBatch2(){
            Test.startTest();
            CampaignPurchasesBatch2 btb = new CampaignPurchasesBatch2();
            Id batchId = Database.executeBatch(btb);
            Test.stopTest();
        }
        
    public TestMethod static void CampaignPurchasesSchedulerTest() {
        Test.startTest();
         String jobId = System.schedule('CampaignPurchasesSchedulerTest',CRON_EXP , 
            new CampaignPurchasesScheduler());    
        Test.stopTest();
    }
}