/*
 * This class manages the HTTPS requests of the ClientList object web service
 *
 */

global with sharing class CLT_ClientList implements CLT_RetailInterface{
    
    
    public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    
    global Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }

    global Object retrieveGet(Map<String, String> params){
        String UserId=CLT_Utils.checkRunningUser(params);
        ClientListResponse response= getClientLists(UserId);
        return response;
    }

//------------------------------------START GET Client List------------------------------------------------------------------------
    ClientListResponse getClientLists(String UserId){  
        
        User me = CLT_Utils.getMyUser();     
        string storeCode = me.BaseStoreCode__c;


        ClientListResponse response = new ClientListResponse();
        response.ClientLists = new List<ClientListWrapper>();
        response.ClientMembers = new List<ClientListMembersWrapper>();

        Map<String, String> parentCampaignMap = new Map<String, String>();
        Map<Id, ClientList__c> clientListsMap = getClientListsItems(UserId);  
        

        //retrieve General Client Lists with their clients
        for (ClientList__c clientList: clientListsMap.values()){
            Set<String> clientIds = new Set<String>();

            if(clientList.GeneralList__c){
                String query = CLT_Utils_ClientLists.buildDynamicQuery(clientList, me, storeCode);
                System.debug(query);
                Map<String, Account> dynamicClients = new Map<String, Account>((List<Account>)Database.query(query));
                response.ClientMembers.addAll( getGeneralListsMembers ( dynamicClients , clientList.Id ) );
                clientIds.addAll(dynamicClients.keySet());
            } 

            response.ClientLists.add(new ClientListWrapper(clientList, clientIds));
        }
        
        //retrieveCampaigns
        Map<Id, Campaign> campaignsMap  = getCampaigns();  
        for (Campaign campaign: campaignsMap.values()){
            string parentCampaignId = string.isNotEmpty(campaign.parentId) ? campaign.parentId : campaign.Id;
            parentCampaignMap.put(parentCampaignId, campaign.Id);

            response.ClientLists.add(new ClientListWrapper(campaign, new Set<String>()));
        }
        
        //retrieve Campaign Members
        for (CampaignMember campaignMember: getCampaignMembers(me, campaignsMap.keySet(),UserId)){  
            response.ClientMembers.add(new ClientListMembersWrapper(campaignMember));  
        }

        response.CampaignGifts = getCampaignGift(parentCampaignMap);
        response.getDocuments();
        return response;
    }
    
    List<ClientListMembersWrapper> getGeneralListsMembers(Map<String, Account> dynamicClients, String clientListId){
        
        List<ClientListMembersWrapper> members = new List<ClientListMembersWrapper>();
        for(Account client : dynamicClients.values()){
            members.add(new ClientListMembersWrapper ( client.id + clientListId, client.Id, clientListId, ''));
        }
        return members;
    }
    
    List<Account> getClients(Set<String> clientIds){
        String query = CLT_Utils_Query.getClientsQuery(false);
        return Database.query(query + ' AND Id IN :clientIds');
    }

    Map<Id, ClientList__c> getClientListsItems(String UserId){
        //string userId = UserInfo.getUserId();   

        return new Map<ID, ClientList__c>([SELECT Id, Name, SOQLQueryConditions__c, GeneralList__c, Type__c, DiorJourneysType__c,
                                                Position__c
                                            from ClientList__c where GeneralList__c =: true OR ownerId =: UserId]);  
    }

    Map<Id, Campaign> getCampaigns(){
        string storeId = CLT_Utils.getUserStoreId();

        
        Map<ID, Campaign> myMap = new Map<ID, Campaign>([SELECT Id, Name, CampaignName__c, Type, StartDate,EndDate, ParentId, AllowSAtoAddClients__c, Parent.AllowSAtoAddClients__c/*, Position__c*/
                                        FROM Campaign 
                                        where  Store__c = :storeId
                                        AND Status__c = : 'Active']);  
    	return myMap;
    }
    
    List<CampaignGiftWrapper> getCampaignGift(Map<String, String> parentCampaignMap){
        list<CampaignGiftWrapper> campaignGiftWrapper = new list<CampaignGiftWrapper>();  

        for(CampaignGift__c campaignGift : [SELECT Id, Gift__c, Gift__r.Name, Campaign__c
                                            FROM CampaignGift__c 
                                            WHERE Campaign__c IN: parentCampaignMap.keySet() OR Campaign__c IN: parentCampaignMap.values()]){

            string campaignId = parentCampaignMap.containsKey(campaignGift.Campaign__c) ? parentCampaignMap.get(campaignGift.Campaign__c) : campaignGift.Campaign__c;
            campaignGiftWrapper.add(new CampaignGiftWrapper(campaignGift, campaignId));
        }
        
       return campaignGiftWrapper;
    }

    List<CampaignMember> getCampaignMembers(User me, Set<Id> clientListIds,String UserId){
    	//String userId = UserInfo.getUserId();
        return (List<CampaignMember>)Database.query('SELECT Id, Name,  Contact.AccountId, CampaignId, Status, Hidden__c, Purchase__c,AppointmentId__c, AppointmentDateTime__c, UniqueUpsertId__c ' + 
                                                    ' from CampaignMember ' + 
                                                    'where CampaignId IN: clientListIds '+ 
                                                    ' AND Contact.PreferredSA1__c  = :UserID'+
                                                     ' AND Hidden__c = false' + CLT_Utils_Query.getMyClientsMembersCondition(me));
    }

//------------------------------------END   GET Client List------------------------------------------------------------------------


    class ClientListWrapper{
        public String id {get;set;}
        public String parentId {get;set;}
        public String name {get;set;}
        public Double position {get;set;}
        public String type {get;set;}
        public String campaignType {get;set;}
        public Date startDate {get;set;}
        public Date endDate {get;set;}
        public boolean isCampaign {get;set;}
        public boolean isClientEditable {get;set;}
        public String diorJourneysType {get;set;}
       
        ClientListWrapper(ClientList__c clientList, Set<String> clientIds){
            this(clientList);
            //this.clientIds = clientIds;  
            this.isCampaign  = false;
        }
        
        ClientListWrapper(Campaign campaign, Set<String> clientIds){
            this(campaign);
            //this.clientIds = clientIds;  
            this.isCampaign  = true;
        }

        ClientListWrapper(ClientList__c clientList){
            this.id = clientList.Id;
            this.parentId = clientList.Id;
            this.name = clientList.Name;
            this.position = clientList.Position__c;
            this.type = clientList.Type__c;
            this.diorJourneysType = clientList.DiorJourneysType__c; 
            this.isClientEditable = false;
        }
        
        ClientListWrapper(Campaign campaign){
            this.id = campaign.Id;
            this.parentId = campaign.Id;
            if (string.isNotEmpty(campaign.parentId)){
                this.parentId = campaign.parentId;
            }
            
            this.name = campaign.CampaignName__c;
            //this.position = campaign.Position__c;
            if(campaign.Type == 'FDA'){
                this.diorJourneysType = campaign.Type;  
                this.Type = 'DIOR JOURNEYS';
            }
            else {
                this.Type = campaign.Type;
            }
            
            this.startDate = campaign.StartDate;
            this.endDate = campaign.EndDate;  
            this.isClientEditable = campaign.AllowSAtoAddClients__c || campaign.Parent.AllowSAtoAddClients__c;  
        }
    }

    class CampaignGiftWrapper{
        public String id {get;set;}
        public String giftId {get;set;}
        public String giftName {get;set;}
        public String campaign {get;set;}

        CampaignGiftWrapper(CampaignGift__c campaignGift, string campaignId){
            this.id = campaignGift.Id;
            this.giftId = campaignGift.Gift__r.Id;
            this.giftName = campaignGift.Gift__r.Name;
            this.campaign = campaignId;
        } 
    }

    public class ClientListMembersWrapper{
        public String id {get;set;}
        public String contactId {get;set;}
        public String clientId {get;set;}
        public String clientListId {get;set;}
        public String status {get;set;}
        public Boolean hidden {get;set;}
        public Double purchase {get;set;}
        public Datetime appointmentDate {get;set;}
        public String appointmentId {get;set;}
        public String otoUniqueId {get;set;}

        public ClientListMembersWrapper(ClientListMember__c clientListMember){
            this.id = clientListMember.Id;
            this.clientId = clientListMember.Client__c;
            this.clientListId = clientListMember.ClientList__c;
            this.status = clientListMember.Status__c;
            this.purchase = null;
        }
        public ClientListMembersWrapper(String id, String clientId, String clientListId, String status){
            this.id = id;
            this.clientId = clientId;
            this.clientListId = clientListId;
            this.status = status;
            this.purchase = null;
        }
        public ClientListMembersWrapper(CampaignMember campaignMember){
            this.id = campaignMember.Id;
            this.clientId = campaignMember.Contact.AccountId;
            this.clientListId = campaignMember.campaignId;
            this.status = campaignMember.Status;
            this.hidden = campaignMember.Hidden__c;
            this.purchase = campaignMember.Purchase__c;
            this.appointmentDate = campaignMember.AppointmentDateTime__c;  
            this.appointmentId = campaignMember.AppointmentId__c;  
            this.contactId = campaignMember.ContactId;  
            this.clientListId = campaignMember.CampaignId;  
            this.otoUniqueId = campaignMember.UniqueUpsertId__c;
            
        }
    }

    class ClientListResponse{  
        public List<ClientListWrapper> ClientLists {get;set;}
        public List<ClientListMembersWrapper> ClientMembers {get;set;}
        public List<CampaignGiftWrapper> CampaignGifts {get;set;}
        public Map<String, Map<String, Object>> Documents {get;set;}

        Void getDocuments(){ 
            set<String> listIds = new set<String>();

            for(ClientListWrapper clientList : ClientLists){
                listIds.add(clientList.parentId);
            }

            Documents = CLT_Utils.getDocuments(listIds);
        }  
    }

}