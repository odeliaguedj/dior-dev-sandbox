public class CaseHandler {
    
    // This function gets priority field from the parent Case.
	public static void getCasePriority(List<Case> triggerNew, Map<Id, Case> triggerOldMap) {
        Map<String,String> prioritiesMap = getCasePrioritiesMap();
        
        // If it's an insert.
        if(triggerOldMap == null) {
            for(Case itemCase : triggerNew) {
                if(itemCase.Priority=='Undefined') {
                    itemCase.Priority =  prioritiesMap.get(itemCase.ClientSegment__c+itemCase.ContactReasonLevel2__c);
                }
            }
        }
        // If it's an update.
        else {
            for(Case itemCase : triggerNew) {
                if( (itemCase.ContactReasonLevel2__c!=triggerOldMap.get(itemCase.Id).ContactReasonLevel2__c
                    || itemCase.ClientSegment__c!=triggerOldMap.get(itemCase.Id).ClientSegment__c)
                    && itemCase.Priority==triggerOldMap.get(itemCase.Id).Priority
                ) {
                    itemCase.Priority = prioritiesMap.get(itemCase.ClientSegment__c+itemCase.ContactReasonLevel2__c);
                }
            }
        }
    }

    // This function gets all items of the priority case schema : ContactReasonLevel2__c+ClientSegment__c=CasePriority__c
    public static Map<String,String> getCasePrioritiesMap() {
        List<CasePriority__c> casePriorityList = [SELECT ContactReasonLevel2__c, ClientSegment__c, CasePriority__c FROM CasePriority__c];
        Map<String,String> prioritiesMap = new Map<String,String>();
        for(CasePriority__c prio : casePriorityList){
            List<String> prioSegments = prio.ClientSegment__c.split(';');
            for(String segment : prioSegments){
                prioritiesMap.put(segment+prio.ContactReasonLevel2__c, prio.CasePriority__c);
            }
        }
        return prioritiesMap;
    }

    public static void setCaseFieldsOnInsert(List<Case> triggerNew) {
        for(Case itemCase : triggerNew) {
            if(itemCase.CopyParentRecordType__c) {
				itemCase.RecordTypeId = itemCase.ParentRecordTypeId__c;
                itemCase.CopyParentRecordType__c = false;
            }
        }
    }



    // This function retrieves data from the JSON in Product Data field and update the Case with these informations.
    public static void fillFieldsFromProductData(List<Case> triggerNew, Map<Id,Case> triggerOldMap) {
        // If it's an insert.
        if(triggerOldMap == null) {
            for(Case itemCase : triggerNew) {
                if(itemCase.ProductData__c != null) {
                    doMappingFromJSON(itemCase);
                }
            }
        }
        // If it's an update.
        else {
            for(Case itemCase : triggerNew) {
                if(itemCase.ProductData__c != triggerOldMap.get(itemCase.Id).ProductData__c && itemCase.ProductData__c != null) {
                    doMappingFromJSON(itemCase);
                }
            }
        }
    }

     // This functions does all steps for retrieves information from the JSON into Product Data and do mapping with with the corresponding fields.
    public static void doMappingFromJSON(Case c) {
        String JSONFormatted = JSONFormatter(c.ProductData__c);
        Case myCase = (Case)JSON.deserializeStrict(JSONFormatted, Case.class);
        fillFields(c, myCase);
        c.ProductData__c = '';
    }

    // This function allows to format the JSON String to the right format, in order to delete chars from Rich Text Area.
    public static String JSONFormatter(String JSONParam) {
        // To delete all chars before the { char.
        String newJSON = JSONParam.replaceAll('(.+\\{)', '{');
        // To delete all chars after the } char.
        newJSON = newJSON.replaceAll('(\\}[\\s\\S].+)', '}');
        // To format the " char.
        newJSON = newJSON.replaceAll('(&quot;)', '"');
        return newJSON;
    }

    // This function fills the fields into the target object Case.
    public static void fillFields(Case targetCase, Case sourceCase) {
        targetCase.ProductName__c        = sourceCase.ProductName__c;
        targetCase.ProductSKU__c         = sourceCase.ProductSKU__c;
        targetCase.ProductCategoryH1__c  = sourceCase.ProductCategoryH1__c;
        targetCase.ProductCategoryH2__c  = sourceCase.ProductCategoryH2__c;
        targetCase.ProductSize__c        = sourceCase.ProductSize__c;
        targetCase.ProductColour__c      = sourceCase.ProductColour__c;
        targetCase.ProductMarketPrice__c = sourceCase.ProductMarketPrice__c;
        targetCase.CurrencyIsoCode       = sourceCase.CurrencyIsoCode;
        targetCase.Store__c              = sourceCase.Store__c;
    }

}