public class ClientSearchController {
    
    public static string storeCampaignRtId =  Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Campaign (Store)').getRecordTypeId();
    
    public static Set<String> storeIds ;
    public static User currentUser = [select Id, Zone__c,toLabel(Zone__c) zone ,Country__c,toLabel(Country__c) country,
    				 BaseStoreCode__c, (select Id,Store__c, Name From Stores__r) 
    				FROM User Where Id = :UserInfo.getUserId()];
    
    @AuraEnabled
    public static PageDetails getPageDetails(String recordId) {
    	PageDetails details = new PageDetails();
        List<Campaign> currentCampaign = [SELECT CampaignName__c,Id,Name,SA__c,Store__c,Store__r.Name,toLabel(Country__c) country, Country__c,toLabel(Zone__c) zone, Zone__c FROM Campaign WHERE Id=:recordId];
        
		if(currentCampaign.size() > 0 )
	        details.currentCampaign = currentCampaign[0];
        details.currentUser  = currentUser ;
        return details; 
    } 
    
    @AuraEnabled
    public static String getClientStores(String filter, Campaign currentCampaign) {
    	
        SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);   
    	String query = '';
    	system.debug('Tamar clientFilter ' + clientFilter);
    	 
    	query = 'SELECT Id,Name FROM Store__c Where Id != NULL AND Name Not Like %Closed% ';
    	  
    	if(!String.isEmpty(clientFilter.mainStoreIds)){
    		system.debug('clientFilter.mainStoreIds' + clientFilter.mainStoreIds);
    		query += ' AND Id  = \'' + clientFilter.mainStoreIds + '\' ';
    	}
    	String storeQuery = buildStoreFilter(clientFilter,currentCampaign) + ' ORDER BY NAME' ;
    	system.debug('storeQuery  ' + storeQuery);
    	
    	List<Store__c> stores = (List<Store__c>) Database.query(storeQuery);
		String jsonResponse = JSON.serialize(stores);
	    
	    return jsonResponse;
	} 
	
    @AuraEnabled
    public static String getTransactionStores() {
    	List<Store__c> stores = [SELECT Id,Name FROM Store__c order by Name ];
        return JSON.serialize(stores);
    }
    
     @AuraEnabled
    public static String getUsers(String filter) { 
    	
    	SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);
    	String query = '';
    	system.debug('clientFilter ' + clientFilter);
    	
    	query = 'SELECT Id,Name FROM User Where IsActive = true ';
    	if(!String.isEmpty(clientFilter.mainSA)){
    		query += ' AND Id   '  + buildMultiChoiceFilter (clientFilter.mainSA) ;
    	}
		String storeQuery = buildStoreFilter(clientFilter,null);
		if(!String.isEmpty(clientFilter.mainStoreIds)){
    		
    		storeQuery += ' AND Id' + buildMultiChoiceFilter (clientFilter.mainStoreIds);
		}
		Map<Id,Store__c> stores = new Map<Id,Store__c>( (List<Store__c> )Database.query (storeQuery));
		query += ' AND Id in (SELECT User__c FROM  UserStore__c ';
	    if(stores.size() > 0 )
	    	query += ' WHERE Store__c ' + buildMultiChoiceFilterFromSet(stores.keySet() );
	    query += ' ) ';
		
		
		/*
    	if(!String.isEmpty(clientFilter.mainSA)){
    		query += ' AND Id  = \'' + clientFilter.mainSA + '\' ';
    	}
    	else{
    		if(!String.isEmpty(clientFilter.mainStoreIds)){
	    		query += ' AND Id in (SELECT User__c FROM  UserStore__c ';
	    		query += ' WHERE Store__c' + buildMultiChoiceFilter (clientFilter.mainStoreIds) + ' ) ';
    		}
    	}
    	*/
    	
    	query += ' order by Name';
    	system.debug('queryString  ' + query);
    	
    	List<User> users = (List<User>) Database.query(query);
		String jsonResponse = JSON.serialize(users);
	    
	    return jsonResponse;
	}
    
    @AuraEnabled
    public static String getClients(String filter) {
    	system.debug(filter);
    	SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);
    	
    	
        String query = 'SELECT Id,Name, Account.Name, CurrentSegment__c, toLabel(CurrentSegment__c) segment , Account.UCRInternalId__c, AmountToNextSegmentEUR__c, OwnerId, Owner.Name, PreferredStore1__c, PreferredStore1__r.name, TwelveMonthsSpentEUR__c ';
        	   if(currentUser.Zone__c != null ){
	        	   query += ' , FirstName' + currentUser.Zone__c +  '__c ,  ' + 'LastName' + currentUser.Zone__c +  '__c  ';
        	   }
			   query += 'FROM Contact WHERE Optin__c = true ';
        
        
        query += buildQueryFilter(clientFilter);
        
        if (clientFilter.lastIdInList != null && !String.isEmpty (clientFilter.lastIdInList)) 
        	query += ' AND Id > \'' + clientFilter.lastIdInList + '\' ';

        query += ' order by Id ASC limit 50';
        system.debug('Test query getClients '+query);
        List<Contact> clients = (List<Contact>) Database.query(query);
        system.debug('clients  found '+clients.size());
        return JSON.serialize(clients);
    }
    
    
    
    @AuraEnabled
    public static Integer getClientsCount(String filter) {
    	
    	SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);
        
        String query = 'SELECT COUNT ()  FROM Contact WHERE Optin__c = true ';
   		query += buildQueryFilter(clientFilter);
        
        system.debug('\n\n\n\n\n'  +   query   + '\n\n\n\n\n' );
        system.debug('Test query getClientsCount '+query);
        Integer queryCount= Database.countQuery(query);
        return queryCount;
    }
    
    private static String buildQueryFilter (SearchParam clientFilter){
    
    	String queryFilter = '';
    	queryFilter += buildHierarchyFilter(clientFilter);
    	queryFilter += buildClientFilter(clientFilter);
        return queryFilter;
    }
    
    private static String buildHierarchyFilter (SearchParam clientFilter){
		
    	String storeQuery = 'Select Id,Name From Store__c where Id != null ';
    	//User has selected a Zone
		if(!String.isEmpty(currentUser.Zone__c) ||  !String.isEmpty(clientFilter.userZone)){
            storeQuery += ' AND Zone__c =  \'' +  clientFilter.userZone + '\''  ;
           
      
            //User has selected a few Countries
			if(!String.isEmpty(clientFilter.userCountries)){
	            storeQuery += ' AND Country__c  ' +  buildMultiChoiceFilter (clientFilter.userCountries) ;
	    	}

	    	//User has not selected any Countries but is at a Country level
			else if(!String.isEmpty(currentUser.Country__c)){
				storeQuery += ' AND Country__c = \'' + currentUser.Country__c + '\'' ;
				
				if ( !String.isEmpty(currentUser.BaseStoreCode__c) && currentUser.Stores__r.size() > 0){
	    		
		    		storeQuery += ' AND Id in (' ;
		    		for(UserStore__c store : currentUser.Stores__r){
		    			storeQuery += '\'' + store.Store__c +  '\',';
		    		}
		    		storeQuery = storeQuery.removeEnd(',');
		    		storeQuery += ' )' ;
		    	} 
			}
	    	
	    	
    	}
    	
    	if ( !String.isEmpty(clientFilter.mainStoreIds)){
    		storeQuery += ' AND Id  ' +  buildMultiChoiceFilter (clientFilter.mainStoreIds) ;
    	}
    	system.debug( 'storeQuery   ' +  storeQuery);
    	List<Store__c> stores = Database.query(storeQuery );
    	storeIds = new Set<String> ();		
    	for(Store__c s : stores){
     		storeIds.add(s.Id);
  		}
    	String query = ' AND PreferredStore1__c IN :storeIds';
    	return query;
    }
    
    public static String buildStoreFilter (SearchParam clientFilter, Campaign currentCampaign){
    	
    	String storeQuery = 'Select Id,Name From Store__c where Id != null AND (NOT Name like \'%Clo%\')';
    	
    	
    	
    	
    	//User has selected a Zone
		if(!String.isEmpty(currentUser.Zone__c) ||  !String.isEmpty(clientFilter.userZone)){
            storeQuery += ' AND Zone__c =  \'' +  clientFilter.userZone + '\''  ;
            
            //User has selected a few Countries
			if(!String.isEmpty(clientFilter.userCountries)){
	            storeQuery += ' AND Country__c  ' +  buildMultiChoiceFilter (clientFilter.userCountries) ;
	    	}
	    	//User has not selected any Countries but is at a Country level
			else if(!String.isEmpty(currentUser.Country__c)){
				storeQuery += ' AND Country__c = \'' + currentUser.Country__c + '\'' ;
				
				if ( !String.isEmpty(currentUser.BaseStoreCode__c)){
	    		
		    		storeQuery += ' AND Id in (' ;
		    		for(UserStore__c store : currentUser.Stores__r){
		    			storeQuery += '\'' + store.Store__c +  '\',';
		    		}
		    		storeQuery = storeQuery.removeEnd(',');
		    		storeQuery += ' )' ;
		    	} 
			}
	    	
	    	
    	}
    	if(currentCampaign != null && currentCampaign.Store__c != null){
    		storeQuery += ' And Id = \'' + currentCampaign.Store__c + '\'' ;
    	}
    	
    	
    	
    	system.debug(' \n\n\n\n\n ' + storeQuery + '\n\n\n ');
    	return storeQuery;
    }
    
    
    private static String buildClientFilter (SearchParam clientFilter){
    	
    	String query = '';
    	
        if(!String.isEmpty(clientFilter.lastName)){
            query += ' AND ( LastName like  \'%' + clientFilter.lastName + '%\' OR LastNameEU__c like  \'%' + clientFilter.lastName + '%\' OR LastNameUS__c like  \'%' + clientFilter.lastName + '%\' OR LastNameJP__c like  \'%' + clientFilter.lastName + '%\' OR LastNameAsia__c like  \'%' + clientFilter.lastName + '%\' )' ;
        }
        
        if(!String.isEmpty(clientFilter.firstName)){
            query += ' AND ( FirstName like  \'%' + clientFilter.firstName + '%\' OR FirstNameEU__c like  \'%' + clientFilter.firstName + '%\' OR FirstNameUS__c like  \'%' + clientFilter.firstName + '%\' OR FirstNameJP__c like  \'%' + clientFilter.firstName + '%\' OR FirstNameAsia__c like  \'%' + clientFilter.firstName + '%\' )' ;
        }
        
        if(!String.isEmpty(clientFilter.zipCode)){
            query += ' AND ( MailingPostalCode  like \'%' + clientFilter.zipCode + '%\'  OR OtherPostalCode like \'%' + clientFilter.zipCode + '%\')' ;
        }
        
        if(!String.isEmpty(clientFilter.city)){
            query += ' AND ( MailingCity  like \'%' + clientFilter.city + '\'  OR OtherCity like \'%' + clientFilter.city + '\')' ;
        }
        if(!String.isEmpty(clientFilter.countries)){
            query += ' AND ( MailingCountry  ' + buildMultiChoiceFilter (clientFilter.countries) + 'OR OtherCountry' + buildMultiChoiceFilter (clientFilter.countries) + ')';
        }
        
        if(!String.isEmpty(clientFilter.nationalities)){//  ( 'nat1','nat2','nat3' )
            query += ' AND Nationality__c ' + buildMultiChoiceFilter (clientFilter.nationalities);
        }
        
        if(!String.isEmpty(clientFilter.phone))
            query += ' AND (Phone like \'%' + clientFilter.phone + '%\'  OR Phone2__c like \'%' + clientFilter.phone+ '%\'  OR Phone3__c like \'%' + clientFilter.phone + '%\')' ;

        if(!String.isEmpty(clientFilter.email))
            query += ' AND (Email like \'%' + clientFilter.email + '%\'  OR Email2__c like \'%' + clientFilter.email + '%\'  OR Email3__c like \'%' + clientFilter.email + '%\')' ;
        
        if(!String.isEmpty(clientFilter.birthdate))
            query += ' AND Birthdate = ' +  clientFilter.birthdate ;
        
        if(!String.isEmpty(clientFilter.birthmonths))
            query += ' AND Birthmonth__c ' + buildMultiChoiceFilter (clientFilter.birthmonths);
            
        if(!String.isEmpty(clientFilter.clientId))
            query += ' AND Account.UCRInternalId__c like \'%' +  clientFilter.clientId + '%\'' ;
        
        if(!String.isEmpty(clientFilter.mainSA))
            query += ' AND PreferredSA1__c ' + buildMultiChoiceFilter (clientFilter.mainSA);
            
        if(clientFilter.topExpectedCategory != null && clientFilter.topExpectedCategory.size() > 0 ){
            query += ' AND ( FdaCategory1__c  ' + buildMultiChoiceFilterFromList (clientFilter.topExpectedCategory) + 'OR FdaCategory2__c' + buildMultiChoiceFilterFromList (clientFilter.topExpectedCategory) + 'OR FdaCategory3__c' + buildMultiChoiceFilterFromList (clientFilter.topExpectedCategory) + ')';
        }
        /*
        if(clientFilter.accountTypes != null && clientFilter.accountTypes.size() > 0)
            query += ' AND AccountType__c ' + buildMultiChoiceFilterFromList (clientFilter.accountTypes);
          */  
        if(clientFilter.segments != null && clientFilter.segments.size() > 0)
            query += ' AND CurrentSegment__c ' + buildMultiChoiceFilterFromList(clientFilter.segments);
        
        if(!String.isEmpty(clientFilter.diorometer))
            query += ' AND FdaTemperature__c  ' + buildMultiChoiceFilter (clientFilter.diorometer);
        
        if(clientFilter.contactability != null && clientFilter.contactability.size() > 0 ){
			query += ' AND (';
	       	for(String channel :clientFilter.contactability ){
				   
		       	if( channel == 'Phone')
		       		query += ' OptinCalling__c = TRUE';
		       	if( channel == 'Email' && query.contains(' OptinCalling__c = TRUE'))
		       		query += ' OR OptinEmailing__c = TRUE';
				if( channel == 'Email' && query.contains(' OptinCalling__c = TRUE') == false)
		       		query += ' OptinEmailing__c = TRUE';
		       	if( channel == 'Mailing' && query.contains(' OptinCalling__c = TRUE') == false && query.contains(' OptinEmailing__c = TRUE') == false)	
		       		query += ' OptinMailing__c = TRUE';
				if( channel == 'Mailing' && (query.contains(' OptinCalling__c = TRUE') || query.contains(' OptinEmailing__c = TRUE')))
		       		query += ' OR OptinMailing__c = TRUE';
		        if( channel == 'Instant Messaging' && query.contains(' OptinCalling__c = TRUE') == false && query.contains(' OptinEmailing__c = TRUE') == false && query.contains(' OptinMailing__c = TRUE') == false)	
		       		query += ' OptinInstantMessaging__c = TRUE'; 
				if( channel == 'Instant Messaging' && (query.contains(' OptinCalling__c = TRUE') || query.contains(' OptinEmailing__c = TRUE') || query.contains(' OptinMailing__c = TRUE')))
		       		query += ' OR OptinInstantMessaging__c = TRUE'; 
	       	}
			   query += ')';
			  
        }     
        
      	if((!String.isEmpty(clientFilter.purchaseCurrency)) && (!String.isEmpty(clientFilter.lifeTimeSpentOperator)) && (!String.isEmpty(clientFilter.lifeTimeSpent)))
       		query += ' AND LifetimeSpent' + clientFilter.purchaseCurrency + '__c ' + clientFilter.lifeTimeSpentOperator +' ' + clientFilter.lifeTimeSpent;	
        
        if((!String.isEmpty(clientFilter.purchaseCurrency)) && (!String.isEmpty(clientFilter.YTDSpentOperator)) && (!String.isEmpty(clientFilter.YTDSpent)))
       		query += ' AND 	YTDSpent' + clientFilter.purchaseCurrency + '__c ' + clientFilter.YTDSpentOperator +' ' + clientFilter.YTDSpent;	
        
        if((!String.isEmpty(clientFilter.purchaseCurrency)) && (!String.isEmpty(clientFilter.twelveMonthsSpentOperator)) && (!String.isEmpty(clientFilter.twelveMonthsSpent)))
       		query += ' AND TwelveMonthsSpent' + clientFilter.purchaseCurrency + '__c ' + clientFilter.twelveMonthsSpentOperator +' ' + clientFilter.twelveMonthsSpent;
		
		if((!String.isEmpty(clientFilter.purchaseDateFrom)) || (!String.isEmpty(clientFilter.purchaseDateTo)) || (clientFilter.purchasedProductCategoriesH1 != null && clientFilter.purchasedProductCategoriesH1.size()>0 ) || (clientFilter.purchasedProductCategoriesH2 != null && clientFilter.purchasedProductCategoriesH2.size()>0  ) || (!String.isEmpty(clientFilter.purchasedStores))){    
			query += ' AND AccountId IN (SELECT Client__c From TransactionLine__c  WHERE ID != null ';
		
			if(!String.isEmpty(clientFilter.purchaseDateFrom)) 
				query += ' AND SalesDate__c >= ' +  clientFilter.purchaseDateFrom ;
			
			if(!String.isEmpty(clientFilter.purchaseDateTo))
				query += ' AND SalesDate__c <= ' +  clientFilter.purchaseDateTo ;
		
			if(clientFilter.purchasedProductCategoriesH1 != null &&  clientFilter.purchasedProductCategoriesH1.size() > 0)
				query += ' AND  ProductCategoryH1__c ' +  buildMultiChoiceFilterFromList (clientFilter.purchasedProductCategoriesH1);
			
			if(clientFilter.purchasedProductCategoriesH2 != null &&  clientFilter.purchasedProductCategoriesH2.size() > 0 ) 
				query += ' AND  ProductCategoryH2__c ' +  buildMultiChoiceFilterFromList (clientFilter.purchasedProductCategoriesH2);
			
			if(!String.isEmpty(clientFilter.purchasedStores))
				  query += ' AND  Store__c ' +  buildMultiChoiceFilter (clientFilter.purchasedStores);
			
			query +=  ')';
		}
		
		if(!String.isEmpty(clientFilter.atLeastTimes)){
			query += ' AND NbOfPurchases__c >= ' + clientFilter.atLeastTimes  ;     
		}
		if(!String.isEmpty(clientFilter.didNotPurchaseDateSince)){
			query += ' AND LastPurchaseDate__c = ' +  clientFilter.didNotPurchaseDateSince ;     
		}
		if (clientFilter.didNotPurchaseProductCategories!= null && clientFilter.didNotPurchaseProductCategories.size() > 0){
			for(String filt :clientFilter.didNotPurchaseProductCategories){
				if ( filt == 'OMNI_1_MEN')
					query += ' AND (LifetimeSpentMen__c = 0 OR LifetimeSpentMen__c = null) '; 
				if ( filt == 'OMNI_1_DM')
					query += ' AND (LifetimeSpentHome__c = 0 OR LifetimeSpentHome__c = null) '; 
				if ( filt == 'OMNI_1_PCD')
					query += ' AND (LifetimeSpentPCD__c = 0 OR LifetimeSpentPCD__c = null) '; 
				if ( filt == 'OMNI_1_WOM')
					query += ' AND (LifetimeSpentWomen__c = 0 OR LifetimeSpentWomen__c = null) ';
				if ( filt == 'OMNI_1_BAB')
					query += ' AND (LifetimeSpentBaby__c = 0 OR LifetimeSpentBaby__c = null)';
				if ( filt == 'OMNI_1_JEW')
					query += ' AND (LifetimeSpentJewelry__c = 0 OR LifetimeSpentJewelry__c = null) ';
			}
		}
        return query;
    } 
     
/*    @AuraEnabled
    public static List<ClientResponse> getClientsIds(String filter) {
    
    	SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);
    	//List<Contact> clients = [SELECT Id, Account.Name, CurrentSegment__c, UCRInternalId__c,  OwnerId, Owner.Name, PreferredStore1__c, PreferredStore1__r.name, TwelveMonthSpendUSD__c FROM Contact];
        //clientFilter = JSON.serialize(clientFilter);
        
        
        system.debug('clientFilter   ' + clientFilter);
      
        String query = 'SELECT Id, Name, ownerId, owner.Name FROM Contact WHERE ID != null ';
                      
        query += buildClientFilter(clientFilter);
      	system.debug('queryString  ' + query);
        query += ' limit 10000';
        List<Contact> clients = (List<Contact>) Database.query(query);
        
        
        List<ClientResponse> clientIds = new List<ClientResponse>();
        for (Contact con : clients){
            clientIds.add( new ClientResponse(con.Id,con.ownerId,con.owner.Name));
        }
        return clientIds;
    }
*/
    
    private static String buildMultiChoiceFilter (String filter){//nat1;nat2;nat2
    	List<String> filters  = filter.split(';');
    	
        return buildMultiChoiceFilterFromList(filters);
    }
    
    private static String buildMultiChoiceFilterFromSet (Set<Id> filter){//nat1;nat2;nat2
    	List<String> filters  = new List<String>();
    	for(Id i :filter){
    		filters.add((String)i);
    	}
        return buildMultiChoiceFilterFromList(filters);
    }
    
    private static String buildMultiChoiceFilterFromList (List<String> filters ){//nat1;nat2;nat2
    	String queryFilter = '';
    	
    	 queryFilter += ' in ( ' ;
        	
        			for(String filt: filters){
        				queryFilter += '\'' + filt + '\',';
        			}
        	queryFilter = queryFilter.removeEnd(',');
        	queryFilter+= ' )';
        return queryFilter;// in  ( 'nat1','nat2','nat3' )
    }
    
   
   	@AuraEnabled
    public static ClientListResponse addClientsToCampaign(String filter,String campaignId) {
    	system.debug(filter);
    	SearchParam clientFilter = (SearchParam) JSON.deserialize (filter, SearchParam.class);
    	String query = 'SELECT Id, Name,PreferredStore1__c, ownerId, owner.Name FROM Contact WHERE  Optin__c = true ';
                      
        query += buildQueryFilter(clientFilter);
        system.debug('clientFilter.selectedRows  ' + clientFilter.selectedRows);
        if(clientFilter.selectAllClients == false   )
			query += ' AND Id ' + buildMultiChoiceFilterFromList (clientFilter.selectedRows  )  ;// + ' OR Id > \'' + clientFilter.lastIdInList + '\') ';
      	system.debug('queryString  ' + query);
        system.debug('Test  query addClientsToCampaign ' + query);
     
        List<Contact> selectedClients = (List<Contact>) Database.query(query);
        
    	
    	Campaign currentCampaign = [select Id,Name, StartDate,Type, EndDate, ParentId, Store__c,Country__c,Zone__c,TECH_ContactIds__c From Campaign where Id = :campaignId];
    	/*
		if( currentCampaign.Store__c != null ){
    		currentCampaign = [select Id,Name, StartDate,Type, EndDate, ParentId, Store__c,Country__c,Zone__c,TECH_ContactIds__c From Campaign where Id = :currentCampaign.ParentId];
    	}*/
    	//List<ClientResponse> selectedClients = (List<ClientResponse>) JSON.deserialize (clientIds, ClientResponse.class);
    	string separator = '!';
		ClientListResponse response = new ClientListResponse();
		response.numberOfListsCreated = 0;
		response.numberOfListsUpdated = 0;
		response.numberOfMembersCreated = 0;
		response.numberOfMembersCreatedforExistingCL = 0;
		response.numberOfMembersFailures = 0;
		response.numberOfListFailures = 0;
		response.numberOfListUpdateFailures = 0;
		Map<Id,String> ownerIdToClientsMap = new Map<Id,String>();
		Map<Id,String> storeIdToClientsMap = new Map<Id,String>();
        Set<Id> storeToAdd = new Set<Id>();
        Set<Id> storeAlreadyIn = new Set<Id>();
        

        List<Campaign> campaignsToInsert = new List<Campaign>();
        List<CampaignMember> campaignMembersToInsert = new List<CampaignMember>();
        List<Campaign> existingCampaigns = new List<Campaign>();
        Map<Id, Campaign> storeToExistingCampaignMap = new map<Id, Campaign>();
        List<CampaignMember> existingCampaignMembers = new List<CampaignMember>();
		
		
        try{
            /* Construct a list of selected Clients */
            for(Contact client :selectedClients){
                /*create a map of ownerIds with all his associated clients*/
                if(!storeIdToClientsMap.containsKey(client.PreferredStore1__c)){
	                //ownerIdToClientsMap.put(client.OwnerId, '');
	                storeIdToClientsMap.put(client.PreferredStore1__c, '');
                }
                string newEntry = storeIdToClientsMap.get(client.PreferredStore1__c) + client.Id + separator;
                storeIdToClientsMap.put(client.PreferredStore1__c, newEntry);
            }

			if(currentCampaign.Store__c == null){
	            /* Check that the Campaign do not exist for the Store for all Stores*/
	           	existingCampaigns = [SELECT id, name,Type, OwnerId , StartDate, EndDate, TECH_ContactIds__c, Store__c
				                    FROM Campaign
				                    //WHERE OwnerId IN :ownerIdToClientsMap.keySet()
				                    WHERE Store__c IN :storeIdToClientsMap.keySet()
				                    AND ParentId =: currentCampaign.Id];
			}
			else {
				existingCampaigns.add(currentCampaign);
			}


	      	for (Campaign c : existingCampaigns) {
				//ownerToCampaignMap.put (c.ownerid, c);
				storeToExistingCampaignMap .put (c.Store__c, c);
		    }

			for (Id storeId : storeIdToClientsMap.keySet()) {
				if (!storeToExistingCampaignMap.containsKey(storeId)) {
					storeToAdd.add(storeId);
				}
				else {
	           		storeAlreadyIn.add(storeId);
	         	}
	       	}
			
			system.debug('### storeToAdd.size():' + storeToAdd.size());
			system.debug('### storeToAdd :' + storeToAdd);
			system.debug('### storeAlreadyIn.size():' + storeAlreadyIn.size());
			system.debug('### storeAlreadyIn :' + storeAlreadyIn);

            /* Creating Campaigns */
			if(!storeToAdd.isEmpty()){
				Map<Id,Store__c> storesMap = new Map<Id,Store__c>([select Id, Name From Store__c where Id in :storeToAdd]);
			    for(Id storeId:storeToAdd){
			    	
			    	string clients = storeIdToClientsMap.get(storeId);
			    	String storeName = storesMap.get(storeId).Name;
                  	response.numberOfMembersCreated += clients != null? clients.split('[!]').size():0;
                    //response.errorMessage = 'numberOfMembersCreated   ' + clients ;
                    campaignsToInsert.add( 
                    		new Campaign(	Name = currentCampaign.Name + ' - ' + storeName,
                    						StartDate = currentCampaign.StartDate,
                    						EndDate = currentCampaign.EndDate ,
                    						Type = currentCampaign.Type ,
											Zone__c = currentCampaign.Zone__c,
					                        //Description__c = listDescription,
					                        //Active__c = false,
					                        TECH_LaunchAddMembersBatch__c = true,
					                        //TECH_DoNotLaunchWorkflow__c = true,
					                        //OwnerId = ownerId,
					                        //SA__c = ownerId,
					                        Store__c = storeId,
					                        Country__c = currentCampaign.Country__c,
					                        TECH_ContactIds__c = clients,
					                        //TECH_CreatedFromTCLScreen__c = true,
					                        ParentId = currentCampaign.Id,
					                        RecordTypeId = storeCampaignRtId
                        				)
                    		);
                }

                if(!campaignsToInsert.isEmpty()){
                    /* Create clients lists assigned to the specific SA */
                    Database.Saveresult[] lSR = Database.insert(campaignsToInsert, false);
                    /* Check Results */
                    for (integer i=0;i<lSR.size();i++){
                        Database.SaveResult oSR = lSR[i];

                        if(oSR.isSuccess()){
                          response.numberOfListsCreated ++;
                        }
                        else {
                          response.numberOfListFailures ++;
                          response.errorMessage += osR.errors;
                        }
                    }
                    /* ApexPages.AddMessage(new ApexPages.message(ApexPages.severity.INFO,System.Label.IC_Client_List_Created.replaceAll('###CLNAME###', listName).replaceAll('###SANAME###', sSAName))); */
                }
            }
            /* TAKE CARE OF THIS. */

            /* ICI */
            if(!storeAlreadyIn.isEmpty()) {
				list<Campaign> campaignsToUpdate = new list<Campaign>();
				/* lClientsListsMembersToInsert.clear(); */
				system.debug('### DANS IF :');
                for(ID storeId : storeAlreadyIn){
                	Set<String> existingClientsIds = new Set<String>();
                	List<CampaignMember> existingMembers = [select Id,ContactId From CampaignMember where Campaign.Store__c = :storeId and Campaign.ParentId = :currentCampaign.Id];
                	for(CampaignMember cm :existingMembers){
						existingClientsIds.add(cm.ContactId);
					}
					String contactIds = storeIdToClientsMap.get(storeId);
					
					string clientsToAdd ;
			      	
			      	for(String clientId :contactIds.split('[!]')){
			      		
			      		if( clientId != '' && !existingClientsIds.contains(clientId)){
			      			if(clientsToAdd == null) clientsToAdd = ''; 
			      			clientsToAdd += clientId + separator;
			      		}
			      	}
			      	
					//if(contactIds == null) contactIds = '';
					response.numberOfMembersCreatedforExistingCL += clientsToAdd != null? clientsToAdd.split('[!]').size():0;
					Campaign camp = storeToExistingCampaignMap .get(storeId);
					
					if(camp.TECH_ContactIds__c  == null )
					  camp.TECH_ContactIds__c  = clientsToAdd;
					else
					  camp.TECH_ContactIds__c += (separator + clientsToAdd);
					system.debug('### camp.TECH_ContactIds__c' + camp.TECH_ContactIds__c);
					system.debug('### camp.Id' + camp.Id);
					camp.TECH_LaunchAddMembersBatch__c = true;
					
					campaignsToUpdate.add(camp);
					//response.errorMessage += 'clientsToAdd   ' + clientsToAdd ;
					//response.errorMessage += ' \n numberOfMembersCreatedforExistingCL   ' + response.numberOfMembersCreatedforExistingCL ;
                }
              	if(!campaignsToUpdate.isEmpty()){
					system.debug('### DANS SECOND INSERT');
					system.debug('### campaignMembersToInsert' + campaignMembersToInsert);
					  /* Create clients lists members */
					//update cliListToUpdate;
					Database.Saveresult[] lSR = Database.update(campaignsToUpdate, false);
					    /* Check Results */
					for (integer i=0;i<lSR.size();i++){
					    Database.SaveResult oSR = lSR[i];
					
					    if(oSR.isSuccess()){
					      response.numberOfListsUpdated ++;
					    }
					    else {
					      response.numberOfListUpdateFailures ++;
					      response.errorMessage += osR.errors;
					    }
					}
              	}
            }
            Database.executeBatch(new CreateCampaignMembersBatch());
        }
        catch(Exception e){
        	response.errorMessage = e.getMessage();
        }
        return response;
    	
    }   
    
    
    public class SearchParam{
    	@AuraEnabled public String userCountries;
    	@AuraEnabled public String userZone;
        @AuraEnabled public String lastName; 
    	@AuraEnabled public String firstName;
        @AuraEnabled public String zipCode;
        @AuraEnabled public String city;
        @AuraEnabled public String countries;
        @AuraEnabled public String nationalities;
        @AuraEnabled public String phone;
        @AuraEnabled public String email;
        @AuraEnabled public String birthdate;
        @AuraEnabled public String birthmonths;
        @AuraEnabled public String clientId;
        @AuraEnabled public List<String> segments;
        @AuraEnabled public String mainStoreIds;
        @AuraEnabled public String mainSA;
        @AuraEnabled public List<String> contactability;
        @AuraEnabled public List<String> accountTypes; 
        @AuraEnabled public String diorometer;
        @AuraEnabled public List<String> topExpectedCategory;
        @AuraEnabled public String firstOrLastOrAllPurchase;
        @AuraEnabled public String purchaseDateFrom;
    	@AuraEnabled public String purchaseDateTo;
        @AuraEnabled public List<String> purchasedProductCategoriesH1;
        @AuraEnabled public List<String> purchasedProductCategoriesH2;
        @AuraEnabled public String purchasedStores;
        @AuraEnabled public String purchaseCurrency;
        @AuraEnabled public String lifeTimeSpentOperator;
        @AuraEnabled public String lifeTimeSpent;
        @AuraEnabled public String YTDSpentOperator;
        @AuraEnabled public String YTDSpent;
        @AuraEnabled public String twelveMonthsSpentOperator;
        @AuraEnabled public String twelveMonthsSpent;
        @AuraEnabled public String atLeastTimes;
        @AuraEnabled public List <String> purchasedSKU;
        @AuraEnabled public String didNotPurchaseDateSince;
        @AuraEnabled public List<String> didNotPurchaseProductCategories;
        @AuraEnabled public List <String> didNotPurchasedSKU;
        @AuraEnabled public String lastIdInList;
        @AuraEnabled public List <String> selectedRows;
        @AuraEnabled public Boolean selectAllClients;
       
   }
   
   public class ClientResponse {
    public String ownerId;
    public String ownerName;
    public String contactId;


    public ClientResponse(){}
    public ClientResponse(String cId,String oId, String oName){
      ownerId = oId;
      ownerName = oName;
      contactId = cId;
    }
  }
   
	public class PageDetails {
		
   		@AuraEnabled public Campaign currentCampaign;
   		@AuraEnabled public User currentUser;
   	}
   
	public class ClientListResponse {
	    @AuraEnabled public Integer numberOfListsCreated;
	    @AuraEnabled public Integer numberOfListsUpdated;
	    @AuraEnabled public Integer numberOfMembersCreated;
	    @AuraEnabled public Integer numberOfMembersCreatedforExistingCL;
	    @AuraEnabled public Integer numberOfListFailures;
	    @AuraEnabled public Integer numberOfListUpdateFailures;
	    @AuraEnabled public Integer numberOfMembersFailures;
	    @AuraEnabled public String errorMessage;
	  }
    
    @AuraEnabled
    public static String getPickListVal(String obj, String fld, String fldEx) {
        
      return JSON.serialize(Utils.getPickListVal(obj,  fld,  fldEx));
    }
    
    @AuraEnabled
    public static String getDependentPickListVal(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        
      return JSON.serialize(Utils.getDependentPickListVal( objDetail,  contrfieldApiName, depfieldApiName));
    }

}