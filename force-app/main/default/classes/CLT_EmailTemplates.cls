public with sharing class CLT_EmailTemplates implements CLT_RetailInterface {
    
  	public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }

    public Object retrieveGet(Map<String, String> params){
        return new Map<String, Object> { 
            'emailTemplates' => getEmailTemplates(params)
        };
    }

    public List<Object> getEmailTemplates(Map<String, String> params){
        String UserLanguage=CLT_Utils.checkRunningUser(params);
        
        List<Map<String, Object>> wrapperList = new List<Map<String, Object>>();

        Map<String, CLT_FieldsMapping__mdt> fieldsMapping = CLT_Utils_Mapping.getMappingObjectDict('EmailTemplate', 'EmailTemplate', 'SFFieldPath__c');

        String langCondition = ' Folder.DeveloperName LIKE \'%'+UserLanguage+'%\' ';
        String query =  'FROM EmailTemplate WHERE ('+ langCondition +') AND IsActive = TRUE ORDER BY Name asc ';
        system.debug('CLT_Utils_Mapping.getQueryString(fieldsMapping, query,true) '  + CLT_Utils_Mapping.getQueryString(fieldsMapping, query,true));
        for(SObject item : Database.query(CLT_Utils_Mapping.getQueryString(fieldsMapping, query,false))){
            Map<String, Object> itemWrapper = CLT_Utils_Mapping.getItemWrapper(fieldsMapping, item);

            string developerName = String.valueOf(item.getSObject('Folder').get('DeveloperName')).toUpperCase();
            string htmlBody = String.valueOf(itemWrapper.get('htmlValue'));
            Boolean isEmail =  developerName.contains('EMAIL');
            
            itemWrapper.put('language', UserLanguage);
            itemWrapper.put('isEmail', isEmail);
            
            if(isEmail && String.isNotEmpty(htmlBody)){
                itemWrapper.put('body', htmlBody);
            }

            wrapperList.add(itemWrapper);      
        }

        return wrapperList;
    }

}