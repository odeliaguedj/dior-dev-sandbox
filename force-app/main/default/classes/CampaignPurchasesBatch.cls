global class CampaignPurchasesBatch  implements Database.Batchable<sObject>,Database.stateful{
	global  String Query;
	global  Date salesDateFilter;
	global DateTime todayDate; 
   	global Integer interval;

   	global Database.QueryLocator start(Database.BatchableContext BC){ 
		
		if(interval == null) {
			interval = -1;
		}
		todayDate = DateTime.now().addDays(interval); 
      	system.debug(' \n\n\n\n  salesDateFilter in start 1'  + salesDateFilter + '\n\n\n\n ');
      	List<TransactionLine__c> allTransactions;
      	if (salesDateFilter == null ){
      		allTransactions = [SELECT Id,SalesDate__c  
      							FROM TransactionLine__c 
      							WHERE Campaign__c =null 
      							AND Client__c != null 
      							AND SalesDate__c != null 
      							AND SalesDate__c < :Date.Today().addDays(30) 
      							AND createddate >= :todayDate //dernieres 24h
      							ORDER BY SalesDate__c ASC LIMIT 1];
			if (allTransactions.size() > 0){
				system.debug(allTransactions.size());
      			salesDateFilter = allTransactions[0].SalesDate__c; 
			}
      	}
      	
      	query = 'SELECT Id, Campaign__c, Client__c ,SalesDate__c, SalesAmountIncludingTaxEUR__c , SA__c '+ 
    	         'FROM TransactionLine__c WHERE Campaign__c =null AND Client__c != null AND SalesDate__c = :salesDateFilter AND createddate > :todayDate' ;  //ajouter le filtre de la date de creation
      	system.debug(' \n\n\n\n  salesDateFilter in start 2'  + salesDateFilter + '\n\n\n\n ');
      	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<TransactionLine__c> transactionLines){
   		
   		//List<Transaction__c> campaignSales = new List<Transaction__c>();
   		List<TransactionLine__c> campaignItems = new List<TransactionLine__c>();
   		Map<Id,Event> eventsToUpdate = new Map<Id, Event>();
   		Date minDate = salesDateFilter.addDays(0);
   		Date maxDate = salesDateFilter.addDays(0);
   		system.debug(' \n\n\n\n  salesDateFilter in execute'  + salesDateFilter + '\n\n\n\n ');
   		Set<String> clientIds = new Set<String> ();
   		for (TransactionLine__c trans :transactionLines){
   			clientIds.add(trans.Client__c);
   		}
   		
   		
   		Map<Id,Account> clientsMap =  new Map<Id,Account>([SELECT Id, PreferredSa1__pc,  (	SELECT Id, Status__c, ActivityDate, Campaign__c,OwnerId, PurchaseEUR__c
   																		FROM Events 
					   													WHERE 	(Show__c = 'Yes' AND Status__c ='Completed')
					   													AND ActivityDate >= :minDate
					   													AND ActivityDate <= :maxDate
																		AND CampaignMemberId__c != null
																		AND Campaign__c != null ) 
					   									FROM Account where Id in: clientIds]);
			for(TransactionLine__c trans: transactionLines){
				if(trans.Client__c != null && clientsMap.get(trans.Client__c) != null){
					Account client = clientsMap.get(trans.Client__c);
					if(client.Events.size() > 0 ){
						for(Event e :client.Events){
							if(trans.SA__c == e.OwnerId){			
								//Update Transaction
								//trans.Campaign__c = campMem.CampaignId;
								//campaignSales.add(trans);
								
								//Update Transaction Lines
								//for(TransactionLine__c line :trans.Transaction_Lines__r){
								trans.Campaign__c = e.Campaign__c;
								if(!campaignItems.contains(trans)){
									campaignItems.add(trans);
								}
								
								//}
						
								//update Campaign Member Total Sales
								//Need to be fixed: if batch is executed twice, it will count sales twice.
								
								//e.PurchaseEUR__c = e.PurchaseEUR__c == null ? 0 : e.PurchaseEUR__c;
								//e.PurchaseEUR__c = trans.SalesAmountIncludingTaxEUR__c;

								//Decimal totalPurchase = campMem.Purchase__c;
						
								if(eventsToUpdate.get(e.Id) == null){
									if(e.PurchaseEUR__c == null) e.PurchaseEUR__c = 0;
									e.PurchaseEUR__c += trans.SalesAmountIncludingTaxEUR__c;
									eventsToUpdate.put(e.Id,e);	
								}
								else {
									eventsToUpdate.get(e.Id).PurchaseEUR__c += trans.SalesAmountIncludingTaxEUR__c;
								}

								break;
							}
							
						}
				}
			
			//if(campaignSales.size()> 0)
				//update campaignSales;
			
			}
		}  	

		if(campaignItems.size()> 0)
				update campaignItems;
				
			if(eventsToUpdate.size()> 0)
				update eventsToUpdate.values();	
    }

	global void finish(Database.BatchableContext BC){
		system.debug(' \n\n\n\n  salesDateFilter in finish'  + salesDateFilter + '\n\n\n\n ');
		List<TransactionLine__c> transactions = [SELECT Id,SalesDate__c FROM TransactionLine__c //ajoute le filtre de date de creation
								WHERE Campaign__c =null AND Client__c != null and SalesDate__c > :salesDateFilter AND createddate > :todayDate order by SalesDate__c ASC LIMIT 1];
		if (transactions.size() > 0){
  			salesDateFilter = transactions[0].SalesDate__c;
   			CampaignPurchasesBatch a = new CampaignPurchasesBatch(); 
   			a.salesDateFilter = salesDateFilter;
			a.interval = interval;
			Database.executeBatch(a);
		}
   	}
}