//created by Rachel Braverman

@isTest
public class CLT_ClientListMember_Test {
    
    @isTest
    private static void retrieveDeleteReturnNull() {
        final CLT_ClientListMember clt = new CLT_ClientListMember();
        System.assertEquals(null, clt.retrieveDelete(new Map<String, String>()));
    }
    
    @isTest
    private static void retrievePostReturnNull() {
        final CLT_ClientListMember clt = new CLT_ClientListMember();
        System.assertEquals(null, clt.retrievePost(new Map<String, String>(), new Map<String, Object>()));
    }

    @isTest
    private static void retrieveGetReturnNull() {
        final CLT_ClientListMember clt = new CLT_ClientListMember();
        System.assertEquals(null, clt.retrieveGet(new Map<String, String>()));
    }
}