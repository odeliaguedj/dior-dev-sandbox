@IsTest
private class CLT_ClientList_Test {
	@isTest static void test_method_one() {

		Account account = new Account(Name = 'account');
		insert account;

		ClientList__c clientList = new ClientList__c(GeneralList__c = true, SOQLQueryConditions__c = 'AND DiorJourneys__pc = \'Top Clients\'');
		insert clientList;

		ClientListMember__c member = new ClientListMember__c(ClientList__c = clientList.Id,Client__c = account.Id);
		insert member;

		Campaign campaign = new Campaign(Name = 'campaign');
		insert campaign;

		Attention__c attention = new Attention__c();
		insert attention;

		CampaignGift__c campaignGift = new CampaignGift__c(Campaign__c = campaign.Id, Gift__c = attention.Id);
		insert campaignGift;

		CLT_ClientList clientDetails = new  CLT_ClientList();
		Test.startTest();
        
        User u = [SELECT id, BaseStoreCode__c FROM User Where BaseStoreCode__c != null LIMIT 1];

        String s = u.BaseStoreCode__c;
        Store__c s_c = new Store__c(StoreCode__c = s);
        insert s_c;
             
        Campaign cmp = new Campaign(name = 'cmp', Store__c = s_c.Id, DaysAfterCampaign__c = 0, EndDate = System.today(), StartDate = System.today());
        insert cmp;
        System.debug('cmpIDddddddddddddd ' + cmp.Id);
        
        System.runAs(u) {
        clientDetails.retrieveGet(new Map<String, String>{'Type' => clientList.Id});
        clientDetails.retrieveGet(new Map<String, String>{'runningUser' => '0051t000001wvzmAAA'});
		clientDetails.retrieveDelete(new Map<String, String>());
		clientDetails.retrievePost(new Map<String, String>(),new Map<String,Object>());
        }
		Test.stopTest();
	}
    
    @isTest
    private static void test2() {
        Test.startTest();
        CLT_ClientList.ClientListMembersWrapper c = new CLT_ClientList.ClientListMembersWrapper('a', 'b', 'c', 'd');
        CLT_ClientList.ClientListMembersWrapper c2 = new CLT_ClientList.ClientListMembersWrapper(new ClientListMember__c());
        CLT_ClientList.ClientListMembersWrapper c3 = new CLT_ClientList.ClientListMembersWrapper(new CampaignMember());
        c.status = 'Active';
        String id1 = c.contactId;
        String id2 = c.clientId;
        String id3 = c.clientListId;
        Boolean d = c.hidden;
        Double dp = c.purchase;
        Datetime dates = c.appointmentDate; 
    	Test.stopTest();
    }
}