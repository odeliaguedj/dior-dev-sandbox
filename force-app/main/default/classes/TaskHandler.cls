public with sharing class TaskHandler {
	
	public static string outreachTskRtId =  Schema.SObjectType.Task.getRecordTypeInfosByName().get('Outreach').getRecordTypeId();
	public static string actionTskRtId =  Schema.SObjectType.Task.getRecordTypeInfosByName().get('Action').getRecordTypeId();
	
    public static void setTaskFields(List<Task> newTasks){
    	for(Task tsk :newTasks){
			
			//set Task Record Type on Insert
			if(trigger.isInsert){
				//default Record Type = Action
				tsk.RecordTypeId = actionTskRtId;
				//if Outreach Type is not null set RecordType to Outreach
				if( String.isEmpty(tsk.OutreachType__c ) == false) {
					tsk.RecordTypeId = outreachTskRtId;
				}
			}
			
			//Set a few fields on Insert/Update
			if(tsk.Type == null)
				tsk.Type = 'Other';
			if (tsk.Status__c == 'Completed' )
				tsk.Status = 'Completed';
			else
				tsk.Status = 'Open';
			/*	
			if(tsk.ReminderDateTime__c != null && tsk.ReminderDateTime__c  > DateTime.now()  ){
				tsk.IsReminderSet = true;
				tsk.ReminderDateTime = tsk.ReminderDateTime__c;
			}*/
				
		}
    }
    
    public static List<Task> filterTasks(List<Task> newTasks){
        List<Task> filteredTasks = new List<Task>();
        for(Task tsk :  newTasks){
            // for Outreaches related to Account
            // excluding Mass outreaches
            if(tsk.Type != '' && !tsk.MassOutreach__c && tsk.AccountId != null && tsk.OutreachType__c != null)
            	filteredTasks.add(tsk);
        }
        return filteredTasks; 
    }
    
	/**
     * Method for trigger Task_AfterInsert, Task_AfterUpdate 
     * This trigger is fired only when==============================
     */
     public static void setClientLastContactDate(List<Task> newTasks){
        
        newTasks = filterTasks(newTasks);
        Map<Id, Account> mapAcc = new Map<Id, Account>();

        for(Task t: newTasks){
            
            Account acc = new Account (Id=t.AccountId);
            if(!mapAcc.containsKey(t.AccountId)){
                if(t.ActivityDate > t.Account.LastContactDate__pc || t.Account.LastContactDate__pc==null){
                	acc.LastContactDate__pc = t.ActivityDate;
                    mapAcc.put(t.accountId,acc);
                }
            }
            else{
                if(t.ActivityDate > mapAcc.get(t.AccountId).LastContactDate__pc){
                	acc.LastContactDate__pc = t.ActivityDate;
                    mapAcc.put(t.accountId,acc);
                }
            } 
        }

        List<Id> lstAccId = new List<Id>(mapAcc.keySet());
        update mapAcc.values();
     }   
}