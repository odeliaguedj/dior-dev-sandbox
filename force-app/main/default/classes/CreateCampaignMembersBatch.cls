global class CreateCampaignMembersBatch implements Database.Batchable<sObject>{
    global database.querylocator start(Database.BatchableContext CD){
        string query  = 'select id, TECH_ContactIds__c from Campaign where TECH_LaunchAddMembersBatch__c = true';
        return Database.getQueryLocator(query);   
    } 
    
    global void execute(Database.BatchableContext CD, list<Campaign> cliLists){
        map<string, set<string>> listToMemberMap = new map<string, set<string>>();
        list<CampaignMember> memberList = new list<CampaignMember>();

        // Extracting the CLM query from the query locator to prevent INVALID QUERY LOCATOR errors
        List<CampaignMember> clientListMembers = [SELECT id, ContactId, CampaignId FROM CampaignMember WHERE CampaignId IN :cliLists];

        try{
            for(Campaign camp : cliLists){
                set<string> membersId = new set<string>();
                for(CampaignMember cliMem : clientListMembers){
                    if (cliMem.CampaignId == camp.id){
                         membersId.add(cliMem.ContactId);
                    }
                }
                
                listToMemberMap.put(camp.Id, membersId);
            }
            
            for(Campaign camp : cliLists){
                if(camp.TECH_ContactIds__c != null){
                    list<string> clientIds = camp.TECH_ContactIds__c.split('!'); 
                    for(string clientId : clientIds){
                        set<string> existingMembers = listToMemberMap.get(camp.id);
                        if(String.isNotEmpty(clientId) && !existingMembers.contains(clientId)){
                            existingMembers.add(clientId);
                            memberList.add(new CampaignMember(CampaignId = camp.id, ContactId = clientId));
                        }
                    } 
                }  
                camp.TECH_ContactIds__c = '';
                //camp.TECH_DoNotLaunchWorkflow__c = false;
                camp.TECH_LaunchAddMembersBatch__c = false;
                //camp.Active__c = true;
            }
            
            update cliLists;
            insert memberList;
            
    
        }catch (DmlException e){
            // Process exception here
            System.debug('#### '+e.getTypeName()+' Exception:'+e.getMessage()+' '+e.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext CD){}

}