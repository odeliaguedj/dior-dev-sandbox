//Created by Rachel Braverman 15/05/2019

@isTest
public class CreateCampaignMembersBatch_Test {

    @testSetup
    private static void setUp() {
		final List<Campaign> cmps = new List<Campaign>();   

        for (Integer i = 0; i < 10; i++) {
        	cmps.add(new Campaign(name='Campaign ' + i, TECH_LaunchAddMembersBatch__c = true));
        }
        Campaign cmp = new Campaign(name='Campaign_', TECH_LaunchAddMembersBatch__c = true, TECH_ContactIds__c = '!');
		cmps.add(cmp);
        insert cmps; 
       
        Contact contact = new Contact(LastName = 'LastName');
        insert contact;

        CampaignMember campaignMember = new CampaignMember(CampaignId=cmp.Id, CurrencyIsoCode='CAD', ContactId =  contact.Id, Status = 'Completed', AppointmentDateTime__c = System.now(), UniqueUpsertId__c = cmp.Id);
        insert campaignMember;        
    }
    
    @isTest
    private static void test() {
        final CreateCampaignMembersBatch batch = new CreateCampaignMembersBatch();
        Test.startTest();
        Database.executeBatch(batch);
        Test.stopTest();
    }
    
    @isTest
    private static void throwDmlException() {
        final CreateCampaignMembersBatch batch = new CreateCampaignMembersBatch();
        Test.startTest();
        Database.executeBatch(batch);  
        Test.stopTest();
    }

}