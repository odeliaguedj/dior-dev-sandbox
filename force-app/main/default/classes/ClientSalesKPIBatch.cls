global class ClientSalesKPIBatch  implements Database.Batchable<sObject>,Database.stateful{
	global  String Query;
	global  Date salesDateFilter;

   	

   	global Database.QueryLocator start(Database.BatchableContext BC){ 
      	system.debug(' \n\n\n\n  salesDateFilter in start 1'  + salesDateFilter + '\n\n\n\n ');
      	//salesDateFilter = Date.today();
      	List<TransactionLine__c> allTransactions;
      	if (salesDateFilter == null ){
      		allTransactions = [SELECT Id, SalesDate__c 
      							FROM TransactionLine__c 
      							WHERE  Client__c != null 
      							AND SalesDate__c != null 
      							AND createddate >= :Date.Today().addDays(-1)
      							ORDER BY SalesDate__c ASC LIMIT 1];
			if (allTransactions.size() > 0){
				System.debug(allTransactions[0].SalesDate__c);
      			salesDateFilter = allTransactions[0].SalesDate__c; 
			}
      	}
      	query = 'SELECT Id , Client__c, CurrencyIsoCode FROM TransactionLine__c WHERE  Client__c != null and SalesDate__c = :SalesdateFilter limit 2000000 ';//AND createddate > :salesDateFilter
      	system.debug(' \n\n\n\n  salesDateFilter in start 2'  + salesDateFilter + '\n\n\n\n ');
      	return Database.getQueryLocator(query); 
   	}

   	global void execute(Database.BatchableContext BC, List<TransactionLine__c> scope){
   		
   		
   		system.debug(' \n\n\n\n  salesDateFilter in execute'  + salesDateFilter + '\n\n\n\n ');
   		Set<String> clientIds = new Set<String> ();
   		for (TransactionLine__c trans :scope){
   			clientIds.add(trans.Client__c); 
   		}
    	/*
    	Map<Id,Account> accounts = new Map<Id,Account>();
    	for(Account acc:[Select Id, NbOfPurchases__pc, LifetimeSpentWomen__pc, LifetimeSpentPCD__pc, LifetimeSpentMen__pc, LifetimeSpentJewelry__pc, LifetimeSpentHome__pc, LifetimeSpentBaby__pc, 
    									(Select SalesAmountIncludingTaxEUR__c, ProductCategoryH1__c,CurrencyIsoCode From TransactionLines__r) 
    									From Account Where Id in: clientIds])
    		accounts.put(acc.Id,acc);
    	
    	
        
        for(Account acc: accounts.values()){
        	double Baby = 0;   
	        double Men = 0;   
	        double Home = 0; 
	        double PCD = 0;
	        double Women = 0;
	        double Jewellery = 0;
           for (TransactionLine__c trans :acc.TransactionLines__r){
	           	if(trans.ProductCategoryH1__c != null && trans.SalesAmountIncludingTaxEUR__c != null){
	           
	            	if(trans.ProductCategoryH1__c == 'OMNI_1_BAB' ){
	            		Baby += trans.SalesAmountIncludingTaxEUR__c;
	            	}
	            	else if(trans.ProductCategoryH1__c == 'OMNI_1_MEN' ){
	            		Men += trans.SalesAmountIncludingTaxEUR__c;
	            	}
	            	else if(trans.ProductCategoryH1__c == 'OMNI_1_DM' ){
	            		Home += trans.SalesAmountIncludingTaxEUR__c;
	            	}
	            	else if(trans.ProductCategoryH1__c == 'OMNI_1_PCD' ){
	            		PCD += trans.SalesAmountIncludingTaxEUR__c;
	            	}
	            	else if(trans.ProductCategoryH1__c == 'OMNI_1_WOM' ){
	            		Women += trans.SalesAmountIncludingTaxEUR__c;
	            	}
	            	else if(trans.ProductCategoryH1__c == 'OMNI_1_JEW' ){
	            		Jewellery += trans.SalesAmountIncludingTaxEUR__c; 
	            	}
           		}	
           }
           
           acc.LifetimeSpentWomen__pc = Women;
           acc.LifetimeSpentPCD__pc = PCD;
           acc.LifetimeSpentMen__pc = Men;
           acc.LifetimeSpentHome__pc = Home;
           acc.LifetimeSpentBaby__pc = Baby;
           acc.LifetimeSpentJewelry__pc = Jewellery;	
           acc.NbOfPurchases__pc = acc.TransactionLines__r.size();
    	}
    	*/
    	
    	AggregateResult[] groupedResults = [ SELECT Client__c,  COUNT(Id) nbOfTransactions , SUM(SalesAmountIncludingTaxEUR__c) sumAmnt, ProductCategoryH1__c   
    											FROM TransactionLine__c 
    											Where Client__c in: clientIds 
    											AND ProductCategoryH1__c != 'Other' 
    											GROUP BY Client__c , ProductCategoryH1__c ];
    	Map<String,Account> accountsToUpdate = new Map<String,Account>();										
    	for (AggregateResult ar : groupedResults){
    		Account client = new Account();
    		client.Id = (Id)ar.get('Client__c');
		    if(ar.get('ProductCategoryH1__c') == 'OMNI_1_BAB')
		    	client.LifetimeSpentBaby__pc = (Decimal)ar.get('sumAmnt');
		    else if(ar.get('ProductCategoryH1__c') == 'OMNI_1_MEN')
		    	client.LifetimeSpentMen__pc = (Decimal)ar.get('sumAmnt');
		    else if(ar.get('ProductCategoryH1__c') == 'OMNI_1_DM')
		    	client.LifetimeSpentHome__pc = (Decimal)ar.get('sumAmnt');
		    else if(ar.get('ProductCategoryH1__c') == 'OMNI_1_PCD')
		    	client.LifetimeSpentPCD__pc = (Decimal)ar.get('sumAmnt');	
		    else if(ar.get('ProductCategoryH1__c') == 'OMNI_1_WOM')
		    	client.LifetimeSpentWomen__pc = (Decimal)ar.get('sumAmnt');
		    else if(ar.get('ProductCategoryH1__c') == 'OMNI_1_JEW')
		    	client.LifetimeSpentJewelry__pc = (Decimal)ar.get('sumAmnt');	
		    client.NbOfPurchases__pc = (Decimal) ar.get('nbOfTransactions');
		    accountsToUpdate.put(client.Id,client);
    	}
    	update accountsToUpdate.values();
    	//Database.update (accountsToUpdate.values(),false);
        
    }

	global void finish(Database.BatchableContext BC){
		system.debug(' \n\n\n\n  salesDateFilter in finish'  + salesDateFilter + '\n\n\n\n ');
		
		List<TransactionLine__c> transactions = [SELECT Id,SalesDate__c FROM TransactionLine__c //ajoute le filtre de date de creation
								WHERE Client__c != null and SalesDate__c > :salesDateFilter  order by SalesDate__c ASC LIMIT 1];
		if (transactions.size() > 0){
  			salesDateFilter = transactions[0].SalesDate__c; 
   			ClientSalesKPIBatch a = new ClientSalesKPIBatch(); 
   			a.salesDateFilter = salesDateFilter;
			Database.executeBatch(a);
		}
   	}
}