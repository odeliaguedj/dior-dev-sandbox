/*
 * Created on 8/6/2019.
 */
public class CLT_Carts implements CLT_RetailInterface{

	public Object retrievePost(Map<String, String> params, Map<String, Object> body) {

		return upsertItems(body);
	}

	private Map<String, Object> upsertItems(Map<String, Object> body) {

		String objectName = (String) body.get('objectName');
		Map<String, Object> cart = (Map<String, Object>) body.get('cart');
		List<Object> cartItems = (List<Object>) cart.get('items');

		Set<String> cartIds = CLT_Utils_Mapping.saveRecords(objectName, 'Cart',new List<Object>{cart});
		Set<String> cartItemIds = CLT_Utils_Mapping.saveRecords('CartItem__c', 'CartItem',cartItems);

		Map<String, Object> results = new Map<String, Object>();


		results.put('cart', retrieveRecords(objectName, 'Cart', cartIds));
		results.put('cartItems', retrieveRecords('CartItem__c', 'CartItem', cartItemIds));

		return results;

	}

	public Object retrieveGet(Map<String, String> params) {
		return null;
	}

	public Object retrieveDelete(Map<String, String> params) {
		return null;
	}

	public static List<Map<String, Object>> retrieveRecords(String objectName, String objectTitle, Set<String> recordsIds) {
		return CLT_Utils_Mapping.getStandardWrapperList(objectName, objectTitle,
						'FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(recordsIds) +
						' ORDER BY CreatedDate DESC');
	}
}