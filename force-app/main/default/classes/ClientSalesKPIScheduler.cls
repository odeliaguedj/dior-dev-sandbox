global class ClientSalesKPIScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        ClientSalesKPIBatch b = new ClientSalesKPIBatch();
        database.executebatch(b);
    }
}