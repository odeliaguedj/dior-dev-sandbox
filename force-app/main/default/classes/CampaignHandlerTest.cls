@isTest
private class CampaignHandlerTest {

    static testMethod void updateCampaignFieldsTest() {   
        User us = [SELECT Id, BaseStoreCode__c FROM User WHERE BaseStoreCode__c != null AND Id = :UserInfo.getUserId()];
		
        Store__c store1 = new Store__c (Name = 'Test Store', StoreCode__c  =  us.BaseStoreCode__c);
        insert store1;
		
		/*CAMPAIGN-PARENT*/
		Campaign parentCampaign = new Campaign(Name='Test Campaign Parent');  
        insert parentCampaign;
        
        /*CAMPAIGN*/
		Campaign childCampaign = new Campaign(Name='Test campaign Child1', ParentId= parentCampaign.Id, Store__c = store1.Id);
        insert childCampaign;
	
		/*CONTACT*/
		Contact cont1= new Contact(LastName = 'jean');
        insert cont1;

        Contact cont2= new Contact(LastName = 'jean');
        insert cont2;
							
		/*CAMPAIGN MEMBERS*/
        Event e1 = new Event(CampaignMemberId__c ='test 1', DurationInMinutes=120, ActivityDateTime= Date.today(), PurchaseEUR__c = 40);
        Event e2 = new Event(CampaignMemberId__c ='test 1', DurationInMinutes = 240, ActivityDateTime=Date.today());

        insert e1;
        insert e2; 

		CampaignMember cm1= new CampaignMember (CampaignId= childCampaign.Id, Status = 'Show Yes', ContactId = cont1.Id, AppointmentIds__c = e1.ID +','+ e2.ID, UniqueUpsertId__c ='test 1');
        insert cm1;

        CampaignMember cm2= new CampaignMember (CampaignId= childCampaign.Id, Status = ' No Show', ContactId = cont2.Id, AppointmentIds__c = e1.ID +','+ e2.ID, UniqueUpsertId__c ='test 1');
        insert cm2;
                       
        childCampaign.Name = 'Test Campaign Child2';
        
        System.runAs(us) {
            update childCampaign;
        }
        cm1.Status = 'No Show';
        update cm1;

        
    }
    
	
}