//Dior Transaction

global with sharing class  Transaction_CLT_CartList  implements CLT_RetailInterface{
    
	 public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    
    global Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }
    
      global Object retrieveGet(Map<String, String> params){
          String objectName = 'Cart__c';
          Set<String> itemIds = new Set<String>();
          String StoreCode=params.get('StoreCode');
          String StoreId=[SELECT Id FROM Store__c WHERE storeCode__c=:params.get('StoreCode')].Id;
          List<Cart__c> cartList=getCartLists(StoreId);
          for(Cart__c item: cartList) {
			itemIds.add(item.Id);
		}

          if(itemIds.size() > 0) {
			return CLT_Utils_Mapping.getStandardWrapperList('Cart__c', 'Cart',
							' FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(itemIds) + ' ORDER BY CreatedDate DESC');
          }
          return null;
          
    }
    
    List<Cart__c> getCartLists(String StoreId){  
      	return [SELECT Client__c,Amount__c, Store__c, Status__c, ActiveOwnerId__c, ItemCount__c FROM Cart__c WHERE Store__c=:StoreId AND Status__c='ON_HOLD*'];     
     }
    
    
}