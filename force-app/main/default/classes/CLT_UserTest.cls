//Tamar s
@isTest
public  class CLT_UserTest {
    @isTest
    public static void  Get_UserTest(){
        Store__c store1 = new Store__c (Name='HUGO', CreatedById = UserInfo.getUserId(),StoreCode__c = '123');
        insert store1;

        Profile p = [select id from profile where name='Standard User'];

        User user1 = new User (Username  = 'Test@1234567890test.com',FirstName = 'Test',LastName = 'Test',Email = 'john@acme.com',Alias = 'Test',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',LanguageLocaleKey = 'en_US', LocaleSidKey=  'en_US', ProfileId = p.Id , BaseStoreCode__c = store1.StoreCode__c);
        insert user1;

        Map<String, String> params = new Map<String, String>{'storeId'=> store1.StoreCode__c};
        Map<String,Object> body = new Map<String,Object>() ;

        Test.startTest();
        
        CLT_User clt_User = new CLT_User();
       
        System.assertEquals(null, clt_User.retrieveDelete(params));
        System.assertEquals(null, clt_User.retrievePost(params, body));
        
        Object MyObject = clt_User.retrieveGet(params);

        //System.debug(LoggingLevel.ERROR, ' Test '+ MyObject);
       
       // List<Map<String, Object>> TestList= clt_User.retrieveGet(params);
       // User MyUser = TestList[0].get('storeId');
       // System.assertEquals(MyUser.BaseStoreCode__c,'123');
        	Test.stopTest();
    }
}