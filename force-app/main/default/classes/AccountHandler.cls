public with sharing class AccountHandler {

	public static void updateClientFields (List<Account> triggerNew, Map<Id,Account> oldMap, Boolean isFromTrigger){
		for(Account client :triggerNew){
			
	
			
			if(client.Birthday__pc != null && client.Birthmonth__pc != null && client.Birthyear__pc != null  ){
				client.PersonBirthdate =   Date.newInstance ( Integer.valueOf(client.Birthyear__pc ) , Integer.valueOf( client.Birthmonth__pc),Integer.valueOf(client.Birthday__pc));
            
			}
			if(client.PreferredSA1__pc != null ) 
				client.OwnerId  = client.PreferredSA1__pc;
			
			if(isFromTrigger){	
				if(client.DiorJourneys__pc == 'WALK_IN_(Retail)' &&  client.WalkInCreationDate__pc == null && (Trigger.isInsert  || oldMap.get(client.Id).DiorJourneys__pc != 'WALK_IN_(Retail)' ))
					client.WalkInCreationDate__pc = system.today();
				
				/*if (Trigger.isUpdate){
					if(client.DiorJourneys__pc != 'WALK_IN_(Retail)' &&  client.WalkInConversionDate__pc == null && (oldMap.get(client.Id).DiorJourneys__pc == 'WALK_IN_(Retail)' ))
					client.WalkInConversionDate__pc = system.today();
				}*/
			}
			
			if (client.WalkInCreationDate__pc != null && client.WalkInConversionDate__pc != null){
				client.DaysFromWalkInRegistration__pc = client.WalkInCreationDate__pc.daysBetween ( client.WalkInConversionDate__pc);
			}		
		}
		//DECEASED
	}

	public static void syncronizeClients(List<Account> clients, Boolean isInsert) {
		List<Data_Hub_Integration_Custom_Field__mdt> fieldMaps = [SELECT DataHubField__c, SFField__c, SaveFromDataHub__c, SendOnCreate__c, SendOnUpdate__c FROM Data_Hub_Integration_Custom_Field__mdt where Object__c = 'Account'];

		list<Map<String, Object>> clientItems = new list<map<string, Object>>();
		List<Id> clientIds = new List<Id>();

		for (Account client :clients){
			Map<String, Object> clientObject = getClientObject(client, fieldMaps, isInsert);
			clientItems.add(clientObject);
			clientIds.add(client.Id);
		}

/*
        if(!Test.isRunningTest()){ 
        		HTTP_ADD_Contact.sendClientToAAD(clientIds, JSON.serialize(clientItems));
        }
    */
	}

	private static Map<String, Object> getClientObject(Account client, List<Data_Hub_Integration_Custom_Field__mdt> fieldMapping, Boolean isInsert) {
		Map<String, Object> ret = new Map<String, Object>();

		for(Data_Hub_Integration_Custom_Field__mdt field : fieldMapping){
			if (isInsert && field.SendOnCreate__c || field.SendOnUpdate__c){
		 		ret.put(field.DataHubField__c, client.get(field.SFField__c));
			}
		}
		return ret;
	}

	/* =================*/
	

}