//Created by Rachel Braverman 15/05/2019

@isTest
public class Timer_Test {
    
    @isTest
    private static void test() {
        String name = 'Test';
        
        Test.startTest();
        Timer.start(name);
        Timer.getTime(name);
        Timer.stop(name);       
        System.assertEquals(null, Timer.stop(null));
        Test.stopTest();
    }

}