/*
* This class manages the HTTPS requests of the DiorStarNews__c object web service
*/

public class CLT_News implements CLT_RetailInterface{

 	//on post
  public Object retrievePost(Map<String, String> params, Map<String, Object> body) {
    return null;
  }
   //on delete request
  public Map<String,Object> retrieveDelete(Map<String, String> params) {
      return null;
  }
	//on get request -return map of news and documents
  public map<string, object> retrieveGet(Map<String, String> params) {
      return getNews();
  }

  public map<string, object> getNews(){
      List<map<string, object>> news = new List<map<string, object>>();
      Map<String, Object> result = new Map<String, Object>();

      Date today = Date.today();
      User myself = CLT_Utils.getMyUser();
      String myStoreCode = myself.BaseStoreCode__c;
      String myZone = myself.Zone__c;

      if (String.isEmpty(myStoreCode) || String.isEmpty(myZone)){
        return result;
      }

      list<map<string, object>> myZoneNews = CLT_Utils_Mapping.getStandardWrapperList('DiorStarNews__c', 'DiorStarNews', 
          'FROM DiorStarNews__c WHERE IsActive__c = true AND Zone__c INCLUDES (\'' + myZone + '\') AND StoresRelated__c = 0 AND StartDate__c <= TODAY AND EndDate__c >= TODAY');

      list<map<string, object>> myStoreNews = CLT_Utils_Mapping.getStandardWrapperList('DiorStarNews__c', 'DiorStarNews', 
          'FROM DiorStarNews__c WHERE IsActive__c = true AND Zone__c INCLUDES (\'' + myZone + '\') AND id in (select News__c from StoreNews__c where Store__r.StoreCode__c =\'' + myStoreCode + '\') AND StartDate__c <= TODAY AND EndDate__c >= TODAY');

      
      news.addAll(myZoneNews);
      news.addAll(myStoreNews);

      result.put('news', news);
          result.put('documents', CLT_Utils.getDocuments(news));

      return result;
  }
}