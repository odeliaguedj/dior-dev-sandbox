global with sharing class ProductsCatalogAppCtrl {
    public ProductsCatalogAppCtrl() {

    }

    @RemoteAction
    global static List<Product_Catalog_Country_Index__mdt> getProductCatalogCountry(String a){
        return [Select DeveloperName, MasterLabel, Country__c, Index__c, Language__c, RegionCode__c, CountryCode__c
                From Product_Catalog_Country_Index__mdt];
    }

    @RemoteAction
    global static ProductCatalog__c getProductCatalogData(String a){
        return [Select DivisionsImages__c, APIDiorcomStockURL__c, AlgoliaApplicationID__c, 
                    AlgoliaAPIKeyCreatedDate__c, AlgoliaAPIKey__c, AlgoliaAdminAPIKey__c
                From ProductCatalog__c];
    }

    @RemoteAction
    global static Map<String, String> getDvisionsImages(String a){
        String divisionsImages = getProductCatalogData('').DivisionsImages__c;
        Map<String, Object> divisionsImagesMap = (Map<String, Object>)System.JSON.deserializeUntyped(divisionsImages);
        Map<String, String> divisionsImagesList = new Map<String, String>();
        for(String division : divisionsImagesMap.keySet()){
            divisionsImagesList.put(division,
              EncodingUtil.base64Encode(getDivisionImage((String)divisionsImagesMap.get(division))) 
            );
        }
        return divisionsImagesList;
    }

    global static Blob getDivisionImage(String imageId){
        return [Select VersionData 
                From ContentVersion 
                Where ContentDocumentId =: imageId
                And IsLatest = true ].VersionData;
    }

    @RemoteAction
    global static String getAccessToken(String a){
        return HTTP_Integration_Utils.getAccessToken().AccessToken__c;
    }

    @RemoteAction
    global static String getImage(String json){
        Map<String, Object> O = (Map<String, Object>)System.JSON.deserializeUntyped(json);
        String accessToken = (String)O.get('accessToken');
        String productCode = (String)O.get('productCode');
        return ProductCarouselServerCtrl.getImage(accessToken, productCode);
    }

    @RemoteAction
    global static List<Store__c> getStore(String storesCodesJson){
        List<String> storesCodes = (List<String>)System.JSON.deserialize(storesCodesJson, List<String>.class);
        return new List<Store__c>([Select Name, StoreCode__c, XpertStoreCode__c, PriceList__c From Store__c Where XpertStoreCode__c IN: storesCodes ]);
    }

}