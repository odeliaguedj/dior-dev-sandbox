public with sharing class Utils {
    
    public static final string bypassTriggers; //List of bypassed triggers
    
    public static final User user;
    
    static {
        user = [Select BypassTriggers__c, profile.name from User where Id=:UserInfo.getUserId() limit 1];
        bypassTriggers = ';'+ user.BypassTriggers__c + ';'; 
    }
    
    
         
    /**
    * Method used by all triggers
    * @param name: name of the trigger
    * @return boolean
    */
    public static boolean canTrigger(string Name){
        return (bypassTriggers.indexof(';' + Name + ';') == -1);

    }
    
    /**  Returns List of picklist Values  **/
    
    public static List<Option> getPickListVal(String obj, String fld, String fldEx) {
        
      System.debug ('*** Start to get Picklist Values on ' + obj + '/'+ fld);
      List<Option> options = new List<Option>();            
      // Get the object type of the SObject.
      SobjectType objType = Schema.getGlobalDescribe().get(obj);
      // Describe the SObject using its object type.
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();       
      // Get a map of fields for the SObject
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
      // Get the list of picklist values for this field.
      list<Schema.PicklistEntry> values =
         fieldMap.get(fld).getDescribe().getPickListValues();
      // Add these values to the selectoption list.
      for (Schema.PicklistEntry a : values)
      { 
        if (a.getValue()!=fldEx){
         options.add( new Option (a.getValue(), a.getLabel()));
        } 
      }
      System.debug ('***  options size=' + options.size());
      System.debug ('*** End to get Picklist Values ');
      return options;
    }

    public class Option {
      @AuraEnabled public string value;
      @AuraEnabled public string label;

      public Option (string value, string label){
        this.value = value ;
        this.label = label;
      }

    }
    
    @AuraEnabled 
    public static Map<String, List<Option>> getDependentPickListVal(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<Option>> objResults = new Map<String,List<Option>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String value = ple.getValue();
            objResults.put(value, new List<Option>());
            controllingValues.add(value);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add( new Option( plew.value,plew.label));
                }
            }
        }
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }


	public static Id getStoreId(String storeCode) {
        List<Store__c> stores = [SELECT Id FROM Store__c WHERE StoreCode__c = : storeCode];

        if (stores.isEmpty()){
            return null;
        }

        return stores.get(0).Id;
    }
}