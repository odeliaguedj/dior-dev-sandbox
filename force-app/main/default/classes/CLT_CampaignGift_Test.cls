//created by Leah Schachter 14/05/19
@isTest
public with sharing class CLT_CampaignGift_Test {
    @TestSetup
    static void makeData()
    {
        Task tsk=new Task();
        insert tsk;

    }

    @isTest
    public static void test() {
        List<Task> tsk=[SELECT id FROM Task LIMIT 1];
        Map<String, String> params=new Map<String, String>(); //empty map
        Map<String,Object> body1= new Map<String,Object> { 'objectName'=> 'Task','id' => tsk.get(0).id,'items'=> JSON.deserializeUntyped('[{},{}]')};

        List<Object> myList = new List<Object>{new Map<String, Object>{'isAllDayEvent' => true,'durationInMinutes'=>80, 'activityDate' => System.today()}};
        Map<String,Object> body2=new Map<String,Object> { 'objectName'=> 'Event','id' => task.Id,'items'=>myList};
        CLT_CampaignGift cmpgnGift =new CLT_CampaignGift();
       
        Map<String,Object> body3= new Map<String,Object> { 'objectName'=> 'lala','id' => tsk.get(0).id,'lala'=> JSON.deserializeUntyped('[{},{}]')};

        Map<String,Object> body4= new Map<String,Object> { 'runningUser'=> '0050Q000002SM4eQAG'};

        Test.startTest();
        Object obj1= cmpgnGift.retrieveDelete(params);
        Object obj2= cmpgnGift.retrievePost(params,body1);
        Object obj3= cmpgnGift.retrievePost(params,body2);
        Object obj4= cmpgnGift.retrievePost(params,body3);
        
        Object obj5= cmpgnGift.retrieveGet(params);
        Object obj6= cmpgnGift.retrievePost(params,body4);
        Test.stopTest();

    }
}