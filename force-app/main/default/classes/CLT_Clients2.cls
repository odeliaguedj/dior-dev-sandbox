/*
 * Created by israel on 6/11/2019.
 */
public class CLT_Clients2 implements clt_retailInterface {

	//on Delete request
	public Object retrieveDelete(Map<String, String> params){
		return null;
	}
	//on Post request
	public Object retrievePost(Map<String, String> params, Map<String,Object> body){

		return null;
	}

	//get Clients
	public Object retrieveGet(Map<String, String> params){
		//return new Map<String, Object>();
		return new Map<String, Object> {
		'clients' => getClients(params)
		};
	}
	// get clients that match the params parameter value
	public List<Object> getClients(Map<String, String> params){
		String clientFields = CLT_Utils_Mapping.getFieldsListString('Account','Client');

		String query = 'FIND \'' + getFieldsString(params) + '\' IN ALL FIELDS RETURNING Account (' +clientFields +' WHERE ' + getCondition(params) + ' AND CurrentSegment__pc != null  AND CurrentSegment__pc != \'NO_SEGMENT\' ) LIMIT 100';
		System.debug(query);
		List<List<Sobject>> result = search.query(query);

		if(!result.isEmpty()){
			return result.get(0);
//			 return CLT_Utils_Mapping.getStandardWrapperListFromObjectList('Account', 'Client', result.get(0));
		}
		return null;

	}


	private String getCondition(Map<String, String> params){

		Map<String, List<String>> fieldsNamesMap = getFieldNamesMap();
		List<String> values = new List<String>();

		for (String key : params.keySet()){

			String condition;
			List<String> subCondition = new List<String>();
			String value = params.get(key).trim();

			if(String.isEmpty(value))	continue;

			for (String fieldName : fieldsNamesMap.get(key.trim())){
				switch on key.toLowerCase() {
					when 'firstname', 'lastname', 'passportnumber', 'phone', 'personemail', 'id' {
						condition = fieldName + ' LIKE \'%' + value + '%\'';
					}
					when else {
						condition = fieldName + ' = \'' + value + '\'';
					}
				}
				subCondition.add(condition);
			}
			values.add( '( ' + String.join(subCondition, ' OR ') +' ) ');
		}
		return String.join(values, ' AND ');
	}

	private String getFieldsString(Map<String, String> params){

		Set<String> values = new Set<String>{'nationality__pc', 'billingcountry'};
		List<String> ret = new List<String>();

		for (String key : params.keySet()){
			if (!values.contains(key.toLowerCase())){
				ret.add(params.get(key) + '*');
			}
		}
		return String.join(ret, ' AND ');
	}


	public static Map<String, List<String>> getFieldNamesMap() {
		Map<String, List<String>> fieldsNamesMap = new Map<String, List<String>>();
		fieldsNamesMap.put('FirstName', new List<String>{'FirstName', 'FirstNameEU__pc', 'FirstNameUS__pc', 'FirstNameJP__pc', 'FirstNameAsia__pc'});
		fieldsNamesMap.put('LastName', new List<String>{'LastName', 'LastNameEU__pc', 'LastNameUS__pc', 'LastNameJP__pc', 'LastNameAsia__pc'});
		fieldsNamesMap.put('Phone', new List<String>{'Phone', 'Phone2__pc', 'Phone3__pc'});
		fieldsNamesMap.put('PersonEmail', new List<String>{'PersonEmail', 'Email2__pc', 'Email3__pc'});
		fieldsNamesMap.put('Id', new List<String>{'UCRInternalId__c'});
		fieldsNamesMap.put('Nationality__pc', new List<String>{'Nationality__pc'});
		fieldsNamesMap.put('PassportNumber', new List<String>{'PassportNumber__pc'});
		fieldsNamesMap.put('BillingCountry', new List<String>{'PersonMailingCountry'});
		return fieldsNamesMap;
	}
}