/*
this class mange the http request of activities service
*/

public with sharing class CLT_Activities implements clt_retailInterface{
    
   

    //delete activity
    public Object retrieveDelete(Map<String, String> params){
        string objName = params.get('objectName');
        
        sobject activity = new Event();
        if(objName == 'Task'){
            activity = new Task();
        }
        activity.Id = params.get('id');
        
        delete activity; 
        return true;
    }
    
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        String UserId=CLT_Utils.checkRunningUser(body);
        return upsertItems(body,UserId);
    }

    public Object retrieveGet(Map<String, String> params){
        system.debug('ACTIVITIES GET PARAMS '+params);
        list<map<string, object>> activities = new list<map<string, object>>();
       	String UserId=CLT_Utils.checkRunningUser(params);
        system.debug('ACTIVITIES UserId '+UserId);
        activities.addAll(getTasks(UserId));
        activities.addAll(getEvents(UserId));
        
        return activities;
    }

    public list<map<string, object>> getTasks(String UserId){
        return CLT_Utils_Mapping.getStandardWrapperList('Task', 'Task', 'FROM Task WHERE OwnerId = \'' + UserId + '\' AND OutreachType__c = NULL');
    }
    
    public list<map<string, object>> getEvents(String UserId){
        return CLT_Utils_Mapping.getStandardWrapperList('Event', 'Event', 'FROM Event WHERE OwnerId = \'' + UserId + '\'');
    }

    public Map<String,Object> upsertItems(Map<String,Object> body,String UserId) {
        String objectName = (String) body.get('objectName');
        List<Object> items = (List<Object>) body.get('items');
        String userStoreId = CLT_Utils.getUserStoreId();
        //Map<Id,Account> clientsMap = new Map<Id,Account>();
        //Map<String,Event> uniqueCampaignEvents = new Map<String,Event> ();
        //Set<String> clientsIds = new Set<String>();
        Set<String> uniqueOtoIds = new Set<String>();

        List<CampaignMember> campMembersToUpsert = new List<CampaignMember>();

        Set<String> itemIds = saveRecords(items, objectName, objectName,UserId);
        if(objectName == 'Task') {
//            itemIds = saveRecords(items, 'Task', 'Task');
        }
        else if(objectName == 'Event') {
//            itemIds = saveRecords(items, 'Event', 'Event');
//            List<Map<String, Object>> mapEvents = CLT_Utils_Mapping.getMapObjectsByWrapperItems('Event', 'Event', items);
//            List<Event> events = (List<Event>) JSON.deserialize(JSON.serialize(mapEvents), List<Event>.class);
//            for(Event item: events) {
//                item.Store__c =  CLT_Utils.getUserStoreId();
//                //clientsIds.add( item.WhatId );
//                if (item.OwnerId == null){
//                    item.OwnerId = UserInfo.getUserId();
//                }
//
//            }
//            upsert events;
//
//            for(Event item: events) {
//                itemIds.add(item.Id);
//            }
            List<Event> otoEvents  = [select Id,CampaignMemberId__c From Event where Id in :itemIds];
            for(Event ev :otoEvents){
            	if(ev.CampaignMemberId__c != null)
            		uniqueOtoIds.add(ev.CampaignMemberId__c);
            }
            /*
            clientsMap = new Map<Id,Account> ([select Id,PersonContactId from Account where Id in :clientsIDs]);
            
            for(Event item :events){
                system.debug(item);
                if( item.Campaign__c != null && clientsMap.get(item.WhatId) != null ){
                    String contactId = clientsMap.get(item.WhatId).PersonContactId;
                    system.debug(contactId);
                    CampaignMember cm = new CampaignMember( CampaignId = item.Campaign__c , 
                                                            //ContactId = contactID,
                                                                UniqueUpsertId__c = String.valueOf(item.Campaign__c ) +  contactId, 
                                                                Status = item.Status__c == 'Completed' ? 'Show Yes' : 'Confirmed',
                                                                AppointmentDateTime__c = item.StartDateTime );
                    
                    campMembersToUpsert.add(cm);
                    uniqueCampaignEvents.put(cm.UniqueUpsertId__c,item);
                    item.CampaignMemberId__c = cm.UniqueUpsertId__c;
                    if(cm.Status ==  'Show Yes' ){
                    	item.Show__c =  'Yes' ;
                    }
                    else if (cm.Status ==  'No Show' ){
                    	item.Show__c =  'No' ;
                    }

                }
            }
            //update events with OTO id
            update events;
            //}
            List<CampaignMember> existingMembers = [select Id,UniqueUpsertId__c From CampaignMember where UniqueUpsertId__c in :uniqueCampaignEvents.keySet()];
            Map<String,CampaignMember> existingMembersMap = new  Map<String,CampaignMember>  ();
            for(CampaignMember cm :existingMembers){
                existingMembersMap.put(cm.UniqueUpsertId__c , cm);
            }
            for(CampaignMember cm :campMembersToUpsert){
                if (existingMembersMap.get(cm.UniqueUpsertId__c) == null){
                    
                    String contactId  = clientsMap.get(uniqueCampaignEvents.get(cm.UniqueUpsertId__c).WhatId).PersonContactId;
                    cm.ContactId = contactId;
                }
                    
            }
            system.debug(campMembersToUpsert);
            if(campMembersToUpsert.size() > 0 )
                upsert campMembersToUpsert UniqueUpsertId__c;

                */
        }
        
        
        Map<String,Object> results = new Map<String,Object>();
        if(itemIds.size() > 0) {
            results.put('activities', CLT_Utils_Mapping.getStandardWrapperList(objectName, objectName, 
                'FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(itemIds) + ' ORDER BY ActivityDate, CreatedDate DESC'));
            if(uniqueOtoIds.size() > 0)
                results.put('campaignMembers', CLT_Utils_Mapping.getStandardWrapperList('CampaignMember', 'CampaignMember',
                            ' FROM CampaignMember WHERE UniqueUpsertId__c IN ' + CLT_Utils.convertToString(uniqueOtoIds) + ' AND Hidden__c = false ORDER BY CreatedDate DESC'));
            return results;
        }
        return null;
    }

    public static Set<String> saveRecords(List<Object> items, String objectName, String objectTitle,String UserId) {
        Set<String> itemIds = new Set<String>();
        String userStoreId = CLT_Utils.getUserStoreId();


        List<Map<String, Object>> mapTasks = CLT_Utils_Mapping.getMapObjectsByWrapperItems(objectName, objectTitle, items);
        List<sObject> objects = (List<sObject>) JSON.deserialize(JSON.serialize(mapTasks), Type.forName('List<'+ objectName +'>'));

        for (sObject obj : objects) {
            obj.put('Store__c', userStoreId);
            if (obj.get('OwnerId') == null) {
                obj.put('OwnerId', UserId);
            }

            if(obj.get('Description')!= null){
                    String description = (String)obj.get('Description');
                    if(description.length()>=32000)
                    {
                        obj.put('Description', description.substring(0,31999)); 
                    }
            }
           
        }
        upsert objects;
        for (sObject item : objects) {
            itemIds.add(item.Id);
        }

        return itemIds;
    }
}