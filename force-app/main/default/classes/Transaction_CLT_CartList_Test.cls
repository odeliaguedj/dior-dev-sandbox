//DiorTransaction

@IsTest
private class Transaction_CLT_CartList_Test {
    @isTest static void test_method_one() {
    Store__c store=new Store__c(StoreCode__c='00');
    insert store;
    Cart__c cart1=new Cart__c(Client__c='0010Q00000TDobbQAD',Store__c=store.Id,Status__c='ON_HOLD*',ActiveOwnerId__c='0050Q000002TwrAQAS');
    insert cart1; 
    Cart__c cart2=new Cart__c(Client__c='0010Q00000TDobbQAD',Store__c=store.Id,Status__c='ON_HOLD*',ActiveOwnerId__c='0050Q000002Twr0QAC');
    insert cart2;
    
    Transaction_CLT_CartList cartList = new  Transaction_CLT_CartList();
        
        
	Test.startTest();
    List<Map<String, Object>> getResult =(List<Map<String, Object>>)cartList.retrieveGet(new Map<String, String>{'StoreCode'=>store.StoreCode__c});
    system.assertEquals(getResult.size(),2);
    Object deleteResult=cartList.retrieveDelete(new Map<String, String>());
    system.assertEquals(deleteResult,null);
    Object postResult=cartList.retrievePost(new Map<String, String>(),new Map<String,Object>());
    system.assertEquals(postResult,null);
	Test.stopTest();
    }  
}