@isTest
private class AccountHandlerTest {

	static testMethod void updateClientFields() {
        
        /*CLIENT*/
		//Id clientTest = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
         
        Account client1 = new Account();
		client1.LastName = 'hugo';
        client1.Salutation='1';
        client1.FirstName='TEST';
		client1.Birthday__pc = '21';
		client1.Birthmonth__pc = '4';
		client1.Birthyear__pc = '1994';
	    client1.PreferredSA1__pc = UserInfo.getUserId();
	    client1.DiorJourneys__pc = 'Walk In';
	    client1.RecordtypeId = TestUtils.paRtId;
		insert client1;
		
		Account client2 = new Account();
		client2.LastName = 'serge';
		client2.Birthday__pc = '21';
		client2.Birthmonth__pc = '4';
		client2.Birthyear__pc = '1994';
	    client2.PreferredSA1__pc = UserInfo.getUserId();
	    client2.DiorJourneys__pc = 'Welcome';
	    client2.RecordtypeId = TestUtils.paRtId;
        client2.Salutation='1';
        client2.FirstName='TEST2';
		insert client2;
		
	    client1.DiorJourneys__pc = 'Welcome';
		update client1;
    }
    
    @isTest
    private static void syncronizeClientsTest() {
        
        List<Account> clients = new List<Account>();
        Account a = new Account(name = 'name');
        clients.add(a);
        insert clients;
        
        Test.startTest();
        AccountHandler.syncronizeClients(clients, true);
        Test.stopTest();
    }    
}