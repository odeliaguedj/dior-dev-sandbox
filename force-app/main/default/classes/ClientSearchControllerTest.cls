//created by Leah Schachter 21/05/19
@isTest
private class ClientSearchControllerTest {
	
	
	@testSetup static void createRecords() {
		
		TestUtils.createPersonAccounts();
        //TestUtils.createTransactionLines();
        
		
	
	}
	
	private static ClientSearchController.SearchParam getFilter (){
		ClientSearchController.SearchParam clientFilter = new ClientSearchController.SearchParam();
		clientFilter.userCountries = '';
    	clientFilter.userZone = 'EU';
        clientFilter.lastName= 'test';
    	clientFilter.firstName = 'test';
        clientFilter.zipCode = '34';
        clientFilter.city = 'Paris';
        clientFilter.countries = 'FRA';
        clientFilter.nationalities = 'FRA';
        clientFilter.phone = '09';
        clientFilter.email = 'test@test.com';
        clientFilter.birthdate = '';
        clientFilter.birthmonths = '1;2;3';
        clientFilter.clientId = '';
        clientFilter.segments = new List<String>{'Prospect'};
        clientFilter.mainStoreIds = '';
        clientFilter.mainSA = UserInfo.getUserId() ;
        clientFilter.contactability = new List<String>{'Phone','Email','Mailing','InstantMessaging'};
        clientFilter.accountTypes = new List<String>();
        clientFilter.diorometer = 'Hot';
        clientFilter.topExpectedCategory = new List<String> {'1','2'};
        clientFilter.firstOrLastOrAllPurchase = '';
        clientFilter.purchaseDateFrom = '2019-01-01';
    	clientFilter.purchaseDateTo = '2019-01-01';
        clientFilter.purchasedProductCategoriesH1 = new List<String>();
        clientFilter.purchasedProductCategoriesH2 = new List<String>();
        clientFilter.purchasedStores = '';
        clientFilter.purchaseCurrency ='EUR';
        clientFilter.lifeTimeSpentOperator   = '>';
        clientFilter.lifeTimeSpent  = '100';
        clientFilter.YTDSpentOperator  = '>';
        clientFilter.YTDSpent  = '100';
        clientFilter.twelveMonthsSpentOperator  = '>';
        clientFilter.twelveMonthsSpent  = '100';
        clientFilter.atLeastTimes  = '2';
        clientFilter.purchasedSKU = new List<String>();
        clientFilter.didNotPurchaseDateSince  = '';
        clientFilter.didNotPurchaseProductCategories = new List<String>{'OMNI_1_MEN','OMNI_1_DM','OMNI_1_PCD','OMNI_1_WOM','OMNI_1_BAB','OMNI_1_JEW'};
        clientFilter.didNotPurchasedSKU = new List<String>();
        clientFilter.lastIdInList  = '';
        clientFilter.selectedRows = new List<String>();
        clientFilter.selectAllClients = true;
        return  clientFilter;
	}
	
	
    
    static testMethod void testgetPageDetailsAndGetClientStores() {  

    	Campaign camp = new Campaign(Name = 'test');
    	insert camp;
    	//cover with empty filter 
    	ClientSearchController.getPageDetails(camp.Id);
    	ClientSearchController.SearchParam clientFilter = new ClientSearchController.SearchParam();
    	String filter1 = JSON.serialize(clientFilter);
    	ClientSearchController.getClientStores(filter1,camp);

		//cover with full filter
		String filter2= '{"zipCode":"aaa","YTDSpentOperator":"aaa" ,"YTDSpent":"aaa","userZone":"aaa","userCountries":"aaa","twelveMonthsSpentOperator":"aaa","twelveMonthsSpent":"aaa","topExpectedCategory":null,"selectedRows":["1","2"],"segments":null,"purchasedStores":null,"purchasedSKU":null,"purchasedProductCategoriesH2":null,"purchasedProductCategoriesH1":null,"purchaseDateTo":null,"mainStoreIds" :null,"contactability":["Phone","Email","Mailing","InstantMessaging"],"lastName" : "aaa","firstName" : "aaa","city" : "aaa","countries" : "aaa","nationalities" : "aaa","phone" : "aaa","email" : "aaa","birthdate" : "aaa","birthmonths" : "aaa","clientId" : "aaa","mainSA" : "aaa","accountTypes" : ["aaa"],"segments" : ["aaa"],"diorometer" : "aaa","mainStoreIds": "aaa","lastIdInList" : "aaa","purchaseDateFrom" :"aaa","purchaseDateTo" :"aaa","purchasedProductCategoriesH1":["aaa"],"purchasedProductCategoriesH2":["aaa"],"atLeastTimes" : "aaa"}';
		ClientSearchController.getClientStores(filter2,camp);

    	
    }

	static testMethod void coverGetTransactionStores() {   
    	ClientSearchController.getTransactionStores();	
    }

		static testMethod void coverGetUsers() {   
		
		//filter = '{"userCountries" : "userCountries_a", "userZone" : "userZone_a", "lastName" : "lastName_a","firstName" : "firstName_a","zipCode" : "zipCode_a","city" : "city_a","countries" : "countries_a","nationalities" : "nationalities_a","phone" : "phone_a","email" : "email_a" ,"birthdate" : "birthdate_a","birthmonths" : "birthmonths_a","clientId" : "clientId_a","mainStoreIds" : "mainStoreIds_a","mainSA" : "mainSA_a","diorometer" : "diorometer_a","topExpectedCategory" : "topExpectedCategory_a","firstOrLastOrAllPurchase" : "firstOrLastOrAllPurchase_a","purchaseDateFrom" : "purchaseDateFrom_a","purchaseDateTo" : "purchaseDateTo_a","purchasedStores" : "purchasedStores_a","purchaseCurrency" : "purchaseCurrency_a","lifeTimeSpentOperator" : "lifeTimeSpentOperator_a","lifeTimeSpent" : "lifeTimeSpent_a","YTDSpentOperator" : "YTDSpentOperator_a","YTDSpent" : "YTDSpent_a","twelveMonthsSpentOperator" : "twelveMonthsSpentOperator_a","twelveMonthsSpent" : "twelveMonthsSpent_a","atLeastTimes" : "atLeastTimes_a","didNotPurchaseDateSince" : "didNotPurchaseDateSince_a","didNotPurchaseProductCategories" : "didNotPurchaseProductCategories_a","lastIdInList" : "lastIdInList_a"}';
    	ClientSearchController.SearchParam clientFilter = getFilter();
    	String filter = JSON.serialize(clientFilter);
    	ClientSearchController.getUsers(filter);	
    }

	static testMethod void coverGetClients() {   
    	ClientSearchController.SearchParam clientFilter = getFilter();
    	String filter = JSON.serialize(clientFilter);
    	ClientSearchController.getClients(filter);	
    	
    	Store__c store = [select Id From Store__c limit 1];
    	clientFilter.mainStoreIds = store.Id + ';';
    	clientFilter.userCountries = 'FRA';
    	clientFilter.selectAllClients = true;
    	filter = JSON.serialize(clientFilter);
    	ClientSearchController.getClients(filter);	
    	
    }

	static testMethod void coverGetClientsCount() {  
		ClientSearchController.SearchParam clientFilter = getFilter();
    	String filter = JSON.serialize(clientFilter); 
    	ClientSearchController.getClientsCount(filter);

    }

    static testMethod void coverAddClientsToCampaign() {   
		Campaign Parentcamp=new Campaign(Name = 'parent');
		insert Parentcamp;
		Store__c store=new Store__c(StoreCode__c='code');
		insert store;
    	Campaign camp = new Campaign(Name = 'testCamp',StartDate=system.today(),Type='OTO',EndDate=system.today().addMonths(1),ParentId=Parentcamp.id,Store__c=store.id,Country__c='FRA',Zone__c='EU',TECH_ContactIds__c='hi hi');
    	insert camp;
    	ClientSearchController.SearchParam clientFilter = getFilter();
    	clientFilter.userCountries = '';
    	clientFilter.userZone = 'EU';
        clientFilter.lastName= '';
    	clientFilter.firstName = '';
        clientFilter.zipCode = '';
        clientFilter.city = '';
        clientFilter.countries = 'FRA';
        clientFilter.nationalities = 'FRA';
        clientFilter.phone = '';
        clientFilter.email = '';
        clientFilter.birthdate = '';
        clientFilter.birthmonths = '';
        clientFilter.clientId = '';
        clientFilter.segments = new List<String>();
        clientFilter.mainStoreIds = '';
        clientFilter.mainSA = UserInfo.getUserId() ;
        clientFilter.contactability = new List<String>();
        clientFilter.accountTypes = new List<String>();
        clientFilter.diorometer = '';
        clientFilter.topExpectedCategory = new List<String>();
        clientFilter.firstOrLastOrAllPurchase = '';
        clientFilter.purchaseDateFrom = '';
    	clientFilter.purchaseDateTo = '';
        clientFilter.purchasedProductCategoriesH1 = new List<String>();
        clientFilter.purchasedProductCategoriesH2 = new List<String>();
        clientFilter.purchasedStores = '';
        clientFilter.purchaseCurrency ='EUR';
        clientFilter.lifeTimeSpentOperator   = '';
        clientFilter.lifeTimeSpent  = '';
        clientFilter.YTDSpentOperator  = '';
        clientFilter.YTDSpent  = '';
        clientFilter.twelveMonthsSpentOperator  = '';
        clientFilter.twelveMonthsSpent  = '';
        clientFilter.atLeastTimes  = '';
        clientFilter.purchasedSKU = new List<String>();
        clientFilter.didNotPurchaseDateSince  = '';
        clientFilter.didNotPurchaseProductCategories = new List<String>();
        clientFilter.didNotPurchasedSKU = new List<String>();
        clientFilter.lastIdInList  = '';
        clientFilter.selectedRows = new List<String>();
        clientFilter.selectAllClients = true;
    	String filter = JSON.serialize(clientFilter);
    	ClientSearchController.addClientsToCampaign(filter,String.valueOf(camp.id));
    	//ClientSearchController.addClientsToCampaign(filter,String.valueOf(camp.id));
    	
    }


	static testMethod void coverPageDetails() {  
	ClientSearchController.PageDetails pd =new ClientSearchController.PageDetails();
    }

	static testMethod void coverClientResponse() {  
	ClientSearchController.ClientResponse cr1 =new ClientSearchController.ClientResponse();
	ClientSearchController.ClientResponse cr2 =new ClientSearchController.ClientResponse('cid','oid','oname');
    }

	
	static testMethod void coverClientListResponse() {  
	ClientSearchController.ClientListResponse clr =new ClientSearchController.ClientListResponse();
    }

	static testMethod void coverGetPickListVal() {  
	Event ev=new Event(CurrencyIsoCode='USD',DurationInMinutes=4,ActivityDateTime=System.today());
	insert ev;
	System.debug('my event: '+ev);
	String objId=String.valueOf(ev.Id);
	//there is an error in getPickListVal:
	//ClientSearchController.getPickListVal(objId,'PreferredChannel','fldex');
    }

	static testMethod void coverGetDependentPickListVal() {  
	Store__c store=new Store__c(StoreCode__c='code');
	insert store;
	ClientSearchController.getDependentPickListVal((sObject)store,'contrfieldApiName','depfieldApiName');
    }


}