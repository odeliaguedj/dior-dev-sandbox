public with sharing class UserStoreHandler {
	
	public static List<UserStore__c> filterRecords(List<UserStore__c> userStores, Map<Id,UserStore__c> oldMap){
        List<UserStore__c> filteredRecords = new List<UserStore__c>();
        for(UserStore__c us :userStores){
			if(us.CurrentStore__c && (Trigger.isInsert  || !oldMap.get(us.Id).CurrentStore__c ))
				filteredRecords.add(us);
		}
        return filteredRecords; 
    }
    
    
    //when a user is marked as current store
    //1.update the relevant user with the stoer information (Zone, Country, Internal code, POS Code)
    //2. Uncheck Current Store for all other stores for this user.
    public static void updateUserStores (List<UserStore__c> userStores, Map<Id,UserStore__c> oldMap){	
    	
    	List<UserStore__c> userStoresToUpdate = new List<UserStore__c>();
    	Set<String> uniqueIds  = new Set<String>();
    	List<UserStore__c> userStoresToProcess = filterRecords(userStores,oldMap);
		Set<String> userIds = new Set<String>();
		Set<String> userStoreIds = new Set<String>();
		
		for(UserStore__c us :userStoresToProcess){
			userIds.add(us.User__c);
			userStoreIds.add(us.Id);
		}
    	
		Map<Id,User> users = new Map<Id,User> ([SELECT  Id, BaseStoreCode__c ,(select Id,User__c,CurrentStore__c,Store__c FROM Stores__r 
																														WHERE CurrentStore__c = true 
																														AND Id not in :userStoreIds) 
												FROM User WHERE Id IN :userIds  ]);
    	
    	
    	for (UserStore__c us : userStoresToProcess){
			User tempUser = users.get(us.User__c);
			tempUser.POSStoreCode__c = us.POSStoreCode__c;
			tempUser.BaseStoreCode__c = us.StoreCode__c;
			tempUser.Country__c = us.StoreCountry__c;
			tempUser.Zone__c = us.StoreZone__c; 
			for(UserStore__c item :tempUser.Stores__r){
				item.CurrentStore__c = false;
				if(!uniqueIds.contains(item.Id)){
					uniqueIds.add(item.Id);
					userStoresToUpdate.add(item);
				}
			}
		}
    	
    	update users.values();
    	update userStoresToUpdate;
    	
    }
  
}