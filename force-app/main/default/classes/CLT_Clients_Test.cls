@isTest
private class CLT_Clients_Test {
    
    @isTest static void test_method_one() {
        Map<String, String> params = new Map<String, String>();
        params.put('FirstName', 'FirstName');
        params.put('LastName', 'LastName');
        params.put('PassportNumber', 'PassportNumber'); 
        
      List<Object> myList = (List<Object>)JSON.deserializeUntyped(
'[ { "client": { "phone": "454545", "turnover": 0, "optInCalling": true, "firstName": "Laura", "title": "Mrs", "nationalityLabel": "ALB", "lastPurchaseAmount": 0, "title_LBL": "Mrs.", "totalItemsBought": 0, "nationalityLabel_LBL": "Albania", "lastName": "Stone" }, "signature": "iVBORw0KGgoAAAAN" } ]');
           
        Map<String,Object> body= new Map<String,Object> { 'objectName'=> 'Account','items'=> myList};
        CLT_Clients cltClients = new CLT_Clients();
        
        Test.startTest();
        cltClients.retrieveDelete(params);
        cltClients.retrieveGet(params);
        cltClients.retrievePost(params, body);
        Test.stopTest();
    }
    
}