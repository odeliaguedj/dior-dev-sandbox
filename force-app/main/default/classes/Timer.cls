public class Timer {

	static Map<String, Datetime> times = new Map<String, Datetime>();

	public static Datetime start(String name){
		Datetime now = System.now();
		times.put(name, now);
		System.debug('======================');
		System.debug('=                    =');
		System.debug('start '+ name );
		System.debug('=      '+ now +'      =');
		System.debug('======================');

		return now;
	}
	public static Datetime getTime(String name){
		return times.get(name);
	}

	public static Long stop(String name){
		if (!times.containsKey(name) || times.get(name) == null){
			return null;
		}
		Long duration = System.now().getTime() - times.get(name).getTime();

		System.debug('======================');
		System.debug('=                    =');
		System.debug('end '+ name);
		System.debug('=      '+duration+'              =');
		System.debug('======================');
		return duration ;
	}
}