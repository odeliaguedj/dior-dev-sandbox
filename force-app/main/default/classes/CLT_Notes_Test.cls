@isTest
public class CLT_Notes_Test {    
    @isTest
    private static void retrieveGetReturnNull() {
        final CLT_RetailInterface clt = new CLT_Notes();
        system.assertEquals(null, clt.retrieveGet(new Map<String, String>()));
    }
                            
    @isTest
    private static void retrieveDeleteReturnTrue() {
        final CLT_RetailInterface clt = new CLT_Notes();
        Map<String, String> params = new Map<String, String>();
        
        Account a = new Account(name = 'account');
        insert a;
        sobject note = new Note__c(Client__c = a.Id);
        insert note;
        params.put('id', note.Id);
       
        Test.startTest();
        system.assertEquals(true, clt.retrieveDelete(params));
    	Test.stopTest();
    }          
    
    @isTest
    private static void retrievePostReturnNull() {
        final CLT_RetailInterface clt = new CLT_Notes();
        Task t = new Task();
        insert t;
        List<Task> tsk = [SELECT id FROM Task LIMIT 1];
        Account account = new Account(Name = 'account');
		insert account;

        List<Map<String, Object>> myList = new List<Map<String, Object>>();
        myList.add(new Map<String, Object>{'clientId' => account.Id});
        Map<String,Object> body1 = new Map<String,Object> { 'objectName'=> 'Note__c','id' => tsk.get(0).id,'items'=> myList};
        Test.startTest();
        clt.retrievePost(new Map<String, String>(), body1);
        Test.stopTest();
    }
}