@isTest
public with sharing class BirthdayTasksBatchTest
{
    public static String CRON_EXP = '0 0 3 ? * * *';
    @testSetup static void createRecords() {
		
		TestUtils.createPersonAccounts();
        //TestUtils.createTransactionLines();

	}
        public static testmethod void  TestBirthdayTasksBatch(){
        Test.startTest();
        BirthdayTasksBatch btb = new BirthdayTasksBatch();
        Id batchId = Database.executeBatch(btb);
        Test.stopTest();
    }
  
    public TestMethod static void BirthdayTasksSchedulerTest() {
        Test.startTest();
         String jobId = System.schedule('ScheduledApexTest',CRON_EXP , 
            new BirthdayTasksScheduler());    
        Test.stopTest();
    }
}