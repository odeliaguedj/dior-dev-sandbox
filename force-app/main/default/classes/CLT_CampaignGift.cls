public with sharing class CLT_CampaignGift implements clt_retailInterface{
    
    
    
    public Object retrieveDelete(Map<String, String> params){//
        return null;    
    }
    
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){//
        String UserId=CLT_Utils.checkRunningUser(body);
        
       return upsertItems(body,UserId);
    }

    public Object retrieveGet(Map<String, String> params){//
        return null;
    }

    public List<Object> upsertItems(Map<String,Object> body,String UserId) {
        String objectName = (String) body.get('objectName');
        List<Object> items = (List<Object>) body.get('items');
        Set<String> itemIds = new Set<String>();
        String userStoreId = CLT_Utils.getUserStoreId();

        if(objectName == 'Task') {
            List<Map<String, Object>> mapTasks = CLT_Utils_Mapping.getMapObjectsByWrapperItems('Task', 'Task', items);
            List<Task> tasks = (List<Task>) JSON.deserialize(JSON.serialize(mapTasks), List<Task>.class);
            for(Task item: tasks) {
                item.Store__c = userStoreId;
                if (item.OwnerId == null){
                    item.OwnerId = UserId;
                }
            }
            upsert tasks;
            for(Task item: tasks) {
                itemIds.add(item.Id);  
            }
        } 
        else if(objectName == 'Event') {
            List<Map<String, Object>> mapEvents = CLT_Utils_Mapping.getMapObjectsByWrapperItems('Event', 'Event', items); 
            List<Event> events = (List<Event>) JSON.deserialize(JSON.serialize(mapEvents), List<Event>.class);
            for(Event item: events) {
                item.Store__c = userStoreId;
                if (item.OwnerId == null){
                    item.OwnerId = UserId;
                }
            }
            upsert events;
            for(Event item: events) {
                itemIds.add(item.Id);
            }
        }
        
        if(itemIds.size() > 0) {
            return CLT_Utils_Mapping.getStandardWrapperList(objectName, objectName, 
                'FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(itemIds) + ' ORDER BY ActivityDate, CreatedDate DESC');
        }
        return null;
    }
}