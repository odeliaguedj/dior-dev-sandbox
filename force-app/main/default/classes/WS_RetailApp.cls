/*
* This class is responsible for managing all HTTP requests
* and forwarding them to the appropriate service
*/
@RestResource(urlMapping='/retail/*')
global with sharing class WS_RetailApp{

    //mange post requests 
    @HttpPost
    global static Void postItem() {
        RestRequest request = RestContext.request;
		System.debug('*****Request requestBody.toString****\n\n\n' + request.requestBody.toString()+ '\n\n\n\n');
        System.debug('*****Request Params****\n\n\n' + request.params + '\n\n\n\n');
 		
        //get the service name
        String objectService = getObjectService(request);
        Map<String,Object> requestBody = getRequestBody(request);
        
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type','application/json');
        system.debug('objectService ' + objectService);
        if(objectService != null){
            //find the warpper service name from metadata table
            List<dependency_injection_class__mdt> classNameList = [ SELECT Class_Name__c
                                                                    FROM dependency_injection_class__mdt 
                                                                    WHERE DeveloperName =: objectService];
            system.debug('classNameList ' + classNameList);
            if(classNameList.size() > 0){  
                 try{
                    clt_retailInterface retailWS = (clt_retailInterface)Type.forName(classNameList[0].Class_Name__c).newInstance();
                    system.debug('retailWS ' + retailWS);
                    Object responseData = retailWS.retrievePost(request.params, requestBody);     
                    response.statusCode = 200;                    
                    response.responseBody = Blob.valueOf(JSON.serialize(new Map<String, Object> {
                        'text' => 'Success',
                        'data' => responseData
                    }));
                }
                catch(Exception ex){
                System.debug(ex.getStackTraceString());
                    response.statusCode = 400;
                    response.responseBody = Blob.valueOf(JSON.serialize(ex.getMessage() + '\n\n' + ex.getStackTraceString()));
                }
            }
        }  
    }
	
    //mange get requests 
    @HttpGet
    global static Void getItem(){
        RestRequest request = RestContext.request;
        //get the service name
        String objectService = getObjectService(request);
        System.debug('*****Request Get****\n\n\n' + request.params + '\n\n\n\n');
        RestResponse response = RestContext.response;
        response.addHeader('Content-Type','application/json');


        if(objectService != null){
            //find the warpper service name from metadata table
            List<dependency_injection_class__mdt> classNameList = [ SELECT Class_Name__c
                                                                    FROM dependency_injection_class__mdt 
                                                                    WHERE DeveloperName =: objectService ];
            if(classNameList.size() > 0){
                try{
                    clt_retailInterface retailWS = (clt_retailInterface)Type.forName(classNameList[0].Class_Name__c).newInstance();
                    Object objectList = retailWS.retrieveGet(request.params);  
                    
                    response.statusCode = 200;
                    response.responseBody = Blob.valueOf(JSON.serialize(objectList));
                }
                catch(Exception ex){
                     System.debug(ex.getStackTraceString());
                    response.statusCode = 400;
                    response.responseBody = Blob.valueOf(JSON.serialize(ex.getMessage() + '\n\n' + ex.getLineNumber()  ));
                }
            }
        }
    }
    
    //mange delete requests 
    @HttpDelete
    global static Void deleteItem(){
        RestRequest request = RestContext.request;
        //get the service name
        String objectService = getObjectService(request);

        RestResponse response = RestContext.response;
        response.addHeader('Content-Type','application/json');


        if(objectService != null){
            //find the warpper service name from metadata table
            List<dependency_injection_class__mdt> classNameList = [ SELECT Class_Name__c
                                                                    FROM dependency_injection_class__mdt 
                                                                    WHERE DeveloperName =: objectService ];
            if(classNameList.size() > 0){
                try{
                    clt_retailInterface retailWS = (clt_retailInterface)Type.forName(classNameList[0].Class_Name__c).newInstance();
                    Object objectList = retailWS.retrieveDelete(request.params);  
                    
                    response.statusCode = 200;
                    response.responseBody = Blob.valueOf(JSON.serialize(objectList));
                }
                catch(Exception ex){
                    response.statusCode = 400;
                    response.responseBody = Blob.valueOf(JSON.serialize(ex.getMessage() + '\n\n' + ex.getLineNumber()));
                }
            }
        }
    }
   // get map of from requestBody key&value
    global static Map<String,Object> getRequestBody(RestRequest request){
        Map<String,Object> body = new Map<String,Object>(); 
        if (request.requestBody != null) {
            system.debug(request.requestBody.toString());
            
            body = (Map<String,Object>)JSON.deserializeUntyped(request.requestBody.toString());
        }
        

        return body;
    }
	//get the service class name from url
    global static string getObjectService(RestRequest request){
        System.debug('request:'+request);
        return request.requestURI.substring(request.requestURI.lastIndexOf('/') + 1);
    }
}