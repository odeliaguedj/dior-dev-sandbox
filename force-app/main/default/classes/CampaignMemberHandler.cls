public with sharing class CampaignMemberHandler {
    
    public static boolean campaignMemberTriggerContext = false;
    
    public static void setCampaignMembersFields(List<CampaignMember> triggerNew, Map<Id,CampaignMember> oldMap){
    	
    	for(CampaignMember cm :triggerNew){
	    	cm.UniqueUpsertId__c = String.valueOf(cm.CampaignId) + String.valueOf(cm.ContactId);
	    	
	    	if ( cm.Status == 'Declined' ||cm.Status == 'Cannot Be Reached'   || (cm.Status == 'Confirmed' && !EventHandler.eventTriggerContext) ){
	    		cm.AppointmentDateTime__c = null;
	    		cm.AppointmentId__c = null;
	    	}
	    	
	    	if ( cm.Status == 'Show Yes' ||cm.Status == 'No Show'    ){
	    		cm.AppointmentId__c = null;
	    	}
	   
	    }
    }
    
    
    public static void updateOTOAppointments (List<CampaignMember> members, Map<Id,CampaignMember> oldMap){ 
    	
    	Set<String> otoIds = new Set<String>();
    	List<CampaignMember> otosToProcess  = new List<CampaignMember>();
    	
    	for(CampaignMember cm :members){
    		if( (cm.Status != oldMap.get(cm.Id).Status) 
    				&& (cm.Status == 'Show Yes' || cm.Status == 'No Show' || cm.Status == 'Declined' ||cm.Status == 'Cannot Be Reached' )){
    			otoIds.add ( cm.UniqueUpsertId__c );
    			otosToProcess.add(cm);
    		}
    	}
    	if(otoIds.size() > 0 ){
	    	List<Event>  otos = [ select Id,CampaignMemberId__c From Event where ( Status__c != 'Completed' or Show__c = '' ) and CampaignMemberId__c in :otoIds  ];
	    	if(otos.size () > 0){
		    	Map<String,Event> otoMap = new Map<String,Event>();
		    	for(Event oto :otos){
		    		otoMap.put(oto.CampaignMemberId__c,oto);
		    	}
		    	List<Event>  otoToUpdate = new List<Event>();
	    		List<Event>  otoToDelete = new List<Event>();
		    	
		    	for(CampaignMember cm :otosToProcess){
		    		if( cm.Status  == 'Show Yes' || cm.Status == 'No Show' && otoMap.get(cm.UniqueUpsertId__c) != null  ){
		    			Event ev = otoMap.get(cm.UniqueUpsertId__c) ;
		    			system.debug(ev);
		    			system.debug(otoMap);
		    			ev.Show__c = cm.Status ==  'Show Yes' ? 'Yes' : 'No';
		    			ev.Status__c = 'Completed';
		    			otoToUpdate.add ( ev);
		    		}
		    		else if ( cm.Status == 'Declined' ||cm.Status == 'Cannot Be Reached' ){
		    			otoToDelete.add(otoMap.get(cm.UniqueUpsertId__c));
		    		}
		    	}
		    	
		    	if(otoToUpdate.size() > 0 ){
					campaignMemberTriggerContext = true;
					update otoToUpdate;
		    	}
				if(otoToDelete.size() > 0 ){
					campaignMemberTriggerContext = true;
					delete otoToDelete;
				}
	    	}
			
    	}
    }


}