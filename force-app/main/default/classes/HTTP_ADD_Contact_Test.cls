@isTest
public class HTTP_ADD_Contact_Test 
{
    @testsetup static void setup(){
        Account acc  = new Account(LastName = 'Test',Phone='1234567890',PersonBirthdate=System.today(),Language__pc='');
        insert acc;
        insert new ClientAddress__c(Client__c=acc.id);
        APIManagerConstants__c integData =  new APIManagerConstants__c(ApimSubscriptionKey__c='2fd64f1105e44514829f37e1d2db0679',SetupOwnerId = UserInfo.getUserId(),ApimTrace__c = 'Ocp-Apim-Trace', ClientID__c = acc.Id ,Name = 'Test1',AccessToken__c= 'accessToken',ExpireDate__c =  Datetime.newInstance(2012, 03,03),RefreshToken__c = 'refreshToken',loginURL__c='https://dev-api-central.christiandior.com/login');//,ClientID__c = ,ClientSecret__c
        
        insert integData;
	}
	 @isTest
    public static void HTTP_ADD_Contact_Test() 
    {
        Test.startTest();
        HTTP_ADD_Contact http = new HTTP_ADD_Contact();
        String response ='test',  request = 'test', status = 'ok',customerId ='1',  storeCode ='test',  signature = 'test';
        List<Id> clientIds = new List<Id>();
        Map<String, Object> resObj = new Map<String, Object>();
        string data ='test';
        HTTP_ADD_Contact.getMessage(resObj);
        HTTP_ADD_Contact.getSyncData(response,request,status); 
        HTTP_ADD_Contact.sendSignature(customerId, storeCode, signature);
        String allAccountFields=String.join(new List<String>(Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().keyset()),',');
        String allClientAddressFields=String.join(new List<String>(Schema.getGlobalDescribe().get('ClientAddress__c').getDescribe().fields.getMap().keyset()),',');
        List<Account> clients=Database.query('Select '+allAccountFields+' From Account');
        List<ClientAddress__c> addressList=Database.query('Select '+allClientAddressFields+' From ClientAddress__c');
        HTTP_ADD_Contact.sendClients(clients, addressList, 'add', [select id,POSStoreCode__c, StaffCode__c from user limit 1]);
        //HTTP_ADD_Contact.sendClientToAAD(clientIds, data);
        Test.stopTest();
    }
}