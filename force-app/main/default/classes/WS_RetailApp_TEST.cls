//createD by Leah Schachter 22/05/19

@isTest
public with sharing class WS_RetailApp_TEST {

	static testMethod void coverPostItem() { 
        RestRequest request=new RestRequest();
        RestResponse res = new RestResponse();
        request.requestURI=Url.getSalesforceBaseUrl()+'/services/apexrest/retail/news';
        request.httpMethod='POST';
        request.requestBody=Blob.valueOf('{"Name":"test"}');
        RestContext.request=request; 
        RestContext.response=res;
	    WS_RetailApp.postItem();
    }

    	static testMethod void coverGetItem() {
        RestRequest request=new RestRequest();
        RestResponse res = new RestResponse();
        request.requestURI=Url.getSalesforceBaseUrl()+'/services/apexrest/retail/News';
        request.httpMethod='GET';  
        request.requestBody=Blob.valueOf('[]');
       	RestContext.request=request;
        RestContext.response=res;
	    WS_RetailApp.getItem();
    }

        static testMethod void coverDeleteItem() { 
        RestRequest request=new RestRequest();
        RestResponse res = new RestResponse();
        request.requestURI=Url.getSalesforceBaseUrl()+'/services/apexrest/retail/News';
        request.httpMethod='DELETE';
        request.requestBody=Blob.valueOf('[]');
        RestContext.request=request;
        RestContext.response=res;
	    WS_RetailApp.deleteItem();
    }
}