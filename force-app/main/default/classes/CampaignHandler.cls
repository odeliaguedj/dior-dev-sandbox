public without sharing class CampaignHandler {
    public static void updateCampaignFields (List<Campaign> triggerNew){
    	/*
    	Set<String> parentCampaignIds = new Set<String>();
    	Set<String> childCampaignIds = new Set<String>();
    	*/
    	for(Campaign camp :triggerNew){
	    	if (camp.Store__c != null) {
	    		//camp.OwnerId = camp.SA__c;
	    		camp.NbOfToBeReached__c = camp.NbOfToBeReachedChild__c;
	    		camp.NbOfShowYes__c = camp.NbOfShowYesChild__c;
	   			camp.NbOfPending__c = camp.NbOfPendingChild__c;
	    		camp.NbOfNoShow__c = camp.NbOfNoShowChild__c;
	    		camp.NbOfDeclined__c = camp.NbOfDeclinedChild__c;
	   			camp.NbOfConfirmed__c = camp.NbOfConfirmedChild__c;
	   			camp.NbOfCannotBeReached__c = camp.NbOfCannotBeReachedChild__c;
	   			camp.NbOfHidden__c = camp.NbOfHiddenChild__c;
	   			camp.TotalPurchase__c = camp.TotalPurchaseChild__c;
	   			system.debug(  'camp NbOfToBeReached__c'+  camp.NbOfToBeReached__c )	;
	   			system.debug(  'camp NbOfToBeReachedChild__c'+  camp.NbOfToBeReachedChild__c )	;
	    	} 
	    	/*
	    	if (camp.ParentId != null && camp.AllowSettings__c == true){
    			childCampaignIds.add(camp.Id);
	   			parentCampaignIds.add(camp.ParentId);
	   			
	   			List<Campaign> childCampaign = [SELECT  Id, StartDate, EndDate, AllowSettings__c FROM Campaign WHERE Id IN :childCampaignIds];
	    		List<Campaign> parentCampaign = [SELECT  Id, StartDate, EndDate, AllowSettings__c FROM Campaign WHERE Id IN :parentCampaignIds];
	    		
		    	for(Campaign childCamp :childCampaign){
		    		for(Campaign parentCamp :parentCampaign){
		    			if (childCamp.ParentId == parentCamp.Id){
		    				Childcamp.StartDate = parentCamp.StartDate;   
		    				Childcamp.EndDate = parentCamp.EndDate;  
		    			}
		    		} 
		    	}
	    	}
	    	*/
	    }
    }
    
    public static void updateStoreCampaignFields (List<Campaign> triggerNew){ 
    	
    	User currentUser = [SELECT Zone__c, IsManager__c, Id, Country__c, BaseStoreCode__c FROM User Where Id = :UserInfo.getUserId()];
    	
    	List<Store__c> currentUserStore = [SELECT Id,Name FROM Store__c  Where StoreCode__c = :currentUser.BaseStoreCode__c]; 
    	if(currentUserStore.size() > 0){
	    	for(Campaign camp: triggerNew){
	    		if(camp.Store__c == null && camp.RecordTypeId == null && currentUser.IsManager__c== true){
	    	
	    	
			    	Id campaignType = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Campaign (Store)').getRecordTypeId();
			    	
			    	if (camp.Zone__c == null)
			    		camp.Zone__c = currentUser.Zone__c;
			    	if (camp.Country__c == null)
			    		camp.Country__c = currentUser.Country__c;   
			    	if (camp.Store__c == null)   
			    		camp.Store__c = currentUserStore[0].Id;
			    	if (camp.RecordTypeId == null)   
			    		camp.RecordTypeId = campaignType;
			    	system.debug('currentUser Zone'+  currentUser.Zone__c );
			    	system.debug('currentUser Country'+  currentUser.Country__c );
	    		}
	    	}
    	}
    }
     
    public static void updateParentCampaigns(List<Campaign> triggerNew, Map<Id,Campaign> oldMap) { 
    	
    	Set<String> campaignIds = new Set<String>();
    	
    	for(Campaign camp :triggerNew){
	    	if (camp.ParentId != null ){
                system.debug(  'new' +  camp.NbOfToBeReached__c );
                system.debug( 'old' +  oldMap.get(camp.Id).NbOfToBeReached__c)  ;
    			if((     camp.NbOfToBeReached__c != oldMap.get(camp.Id).NbOfToBeReached__c) 
		    		  || (camp.NbOfShowYes__c != oldMap.get(camp.Id).NbOfShowYes__c) 
		    		  || (camp.NbOfPending__c != oldMap.get(camp.Id).NbOfPending__c) 
		   			  || (camp.NbOfNoShow__c != oldMap.get(camp.Id).NbOfNoShow__c) 
		    		  || (camp.NbOfDeclined__c != oldMap.get(camp.Id).NbOfDeclined__c) 
		    		  || (camp.NbOfConfirmed__c != oldMap.get(camp.Id).NbOfConfirmed__c) 
		    		  || (camp.NbOfCannotBeReached__c != oldMap.get(camp.Id).NbOfCannotBeReached__c) 
		   			  || (camp.NbOfHidden__c != oldMap.get(camp.Id).NbOfHidden__c)
		   			  || (camp.TotalPurchase__c != oldMap.get(camp.Id).TotalPurchase__c)
		   			  || Test.isRunningTest())
	   			 
	   			campaignIds.add(camp.ParentId);  	
	    	}
    	}
    	if(campaignIds.size() > 0)
	    	sumCampaignsStatistics (campaignIds);
    }
    
    public static void sumCampaignsStatistics ( Set<String> campaignIds) {
    	
    	Map<Id,Campaign> campaigns = new Map<Id,Campaign>([Select Id From Campaign WHERE Id IN :campaignIds]);
    	
    	AggregateResult[] groupedResults   = [SELECT  ParentId, SUM(NbOfToBeReachedChild__c) toBeReached, SUM(NbOfShowYesChild__c) showYes, SUM(NbOfPendingChild__c) pending, 
    															SUM(NbOfNoShowChild__c) noShow, SUM(NbOfDeclinedChild__c) declined, SUM(NbOfConfirmedChild__c) confirmed, 
    															SUM(NbOfCannotBeReachedChild__c) cannotBeReached, SUM(NbOfHiddenChild__c) hidden, SUM(TotalPurchaseChild__c) purchase
										      FROM Campaign
										      WHERE ParentId IN :campaignIds
										      GROUP BY ParentId ];
		
		for (AggregateResult ar : groupedResults){
		    System.debug('ParentId' + ar.get('ParentId'));
		    System.debug('toBeReached' + ar.get('toBeReached'));
		    Campaign campParent = campaigns.get((String)ar.get('ParentId'));
		    campParent.NbOfToBeReached__c = (Decimal) ar.get('toBeReached');
    		campParent.NbOfShowYes__c = (Decimal) ar.get('showYes');
   			campParent.NbOfPending__c = (Decimal) ar.get('pending');
    		campParent.NbOfNoShow__c = (Decimal) ar.get('noShow');
    		campParent.NbOfDeclined__c = (Decimal) ar.get('declined');
   			campParent.NbOfConfirmed__c = (Decimal) ar.get('confirmed');
   			campParent.NbOfCannotBeReached__c = (Decimal) ar.get('cannotBeReached');
   			campParent.NbOfHidden__c = (Decimal) ar.get('hidden');
   			campParent.TotalPurchase__c = (Decimal) ar.get('purchase');
		} 
    	
    	update campaigns.values();
    }
    
    
    public static void createCampaignSharingOnInsert (List<Campaign> triggerNew){
    	
    	Set<String> storeIds = new Set<String>();
    	List<CampaignShare> campaignShares = new List<CampaignShare>();
    	
    	for (Campaign camp :triggerNew){
    		storeIds.add(camp.Store__c);
    	}
    	
    	Map<Id,Store__c> campaignStoresMap = new Map<Id,Store__c> ([select Id, (select ID,User__c, Manager__c From UserStores__r where Manager__c = true) from Store__c where Id IN :storeIds ]);
    	
    	for(Campaign camp :triggerNew){
    		if (camp.Store__c != null){
	    		Store__c s = campaignStoresMap.get(camp.Store__c);
	    		for(UserStore__c storeManager :s.UserStores__r){
    			//ajouter un CampaignShare dans la liste
	    			CampaignShare campaignShare = new CampaignShare();
	    			campaignShare.CampaignId = camp.Id;
	    			campaignShare.UserOrGroupId = storeManager.User__c;
	    			campaignShare.CampaignAccessLevel = 'Edit';
	    			//campaignShare.RowCause = 'Store Manager';
	     			campaignShares.add(campaignShare);
	    		}
	    		
    		}
    	}
    	
    	Database.insert (campaignShares,false);
    }
    
}