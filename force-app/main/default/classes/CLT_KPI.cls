/*
this class mange the http request of kpi service
*/

public class CLT_KPI implements CLT_RetailInterface{
	
    //global var
	Date today = System.today();
	Date today_A_Year_Ago = System.today().addYears(-1);

	public Object retrievePost(Map<String, String> params, Map<String, Object> body) {
		return null;
	}

	public Map<String,Object> retrieveGet(Map<String, String> params) {
		String userIds =  params.get('userIds');

		List<String> ids = Pattern.matches('\\[.*\\]',userIds) ?
		userIds.substringAfter('["')
				.substringBefore('"]')
				.split(',') : null;
       
        System.debug('ids:'+ids);
//		List<String> ids = userIds != null ? (List<String>)System.JSON.deserialize(userIds, List<String>.class): null;

        Boolean myStore = params.get('myStore') != null ? Boolean.valueOf(params.get('myStore')) : false;
		String myStoreCode = '';

		if (myStore){
			User myself = CLT_Utils.getMyUser();
			myStoreCode = myself.BaseStoreCode__c;
//			ids = getStoresUserIds(myStoreCode);
		}
		String preiod = params.get('period') != null ? params.get('period') : 'y';

		Boolean getUpt = params.get('getUpt') != null ? Boolean.valueOf(params.get('getUpt')) : false;

		String operation =  params.get('operation');
		//if(operation == null)
		return getKPIData(ids, getStartDate(preiod), myStoreCode,  getUpt);
		/*else if (operation == 'transaction')
			return getKPIData(ids, getStartDate(preiod), myStoreCode,  getUpt);
		else if (operation == 'clienteling')
			return getKPIData(ids, getStartDate(preiod), myStoreCode,  getUpt);
		else if (operation == 'transaction')
			return getKPIData(ids, getStartDate(preiod), myStoreCode,  getUpt);
		 */
	}

	private List<String> getStoresUserIds(String storeCode) {
		List<String> ret = new List<String>();
		for(User user :[SELECT Id FROM  User WHERE BaseStoreCode__c = :storeCode]){
			ret.add(user.Id);
		}
		return ret;
	}

	private Map<String, Object> getKPIData(List<String> ids, Date startDate, String myStoreCode, Boolean getUpt) {

		Map<String, Object> ret = new Map<String, Object>();
		
		List<AggregateResult> transactionResult = getTransationResult(ids, startDate);
		List<AggregateResult> tasksResult = getTasksResult(ids, startDate);
		List<AggregateResult> clientsResult = getClientsResult(ids, startDate);
		List<AggregateResult> walkInConvertedResult = getWalkInConvertedResult(ids, startDate);
		List<AggregateResult> SAResult = getSA_Result(ids, startDate);
//		List<AggregateResult> campaignResult = getCampaignResult(startDate);
		List<AggregateResult> campaignCompletedResult = getCampaignCompletedResult(startDate,ids);

		ret.put('segments', getSegmentsObject(transactionResult, tasksResult, clientsResult));
		ret.put('thisYear',
				getYearObject(tasksResult, walkInConvertedResult, campaignCompletedResult, SAResult, startDate, ids));
		ret.put('captureRate', getCaptureRate(ids, clientsResult));
		ret.put('repeatRate',  getRepeatRate(ids));
		ret.put('UPT',  getUPT(SAResult));

		System.debug(JSON.serialize(ret));
		return ret;

	}
 	
    //calc UPT
	private Decimal getUPT(List<AggregateResult> SAResult) {
		Decimal productsCount = SAResult.get(0).get('productsCount') != null ? (Decimal)SAResult.get(0).get('productsCount') : 0;
		Decimal transactionCount = (Decimal)SAResult.get(0).get('transactionCount') != null ? (Decimal)SAResult.get(0).get('transactionCount') : 0;
		Decimal upt = transactionCount != 0 ? productsCount.divide(transactionCount, 2) : 0;

		return upt;
	}
	//calc CaptureRate
	private Decimal getCaptureRate(List<String> usersIds, List<AggregateResult> clientsResult) {
		List<AggregateResult> result = [SELECT COUNT(Id) clients
										FROM Account
										WHERE PreferredSA1__pc IN :usersIds AND Contactable__pc = true];

		Decimal totalClients = 0;
		for (AggregateResult item : clientsResult){
			if (item.get('segmentGrp') == 1){
				totalClients = (Decimal)item.get('clientsCount');
			}
		}


		return totalClients != 0 ?(((Decimal)result.get(0).get('clients')).divide(totalClients,2)) * 100: 0;
	}
	//calc RepeatRate
	private Decimal getRepeatRate(List<String> usersIds) {
		List<AggregateResult> results = [SELECT SUM(RepeatRate12MRolling__c) repeatRate FROM User WHERE Id IN :usersIds];
		if (results.isEmpty() || results.get(0).get('repeatRate') == null){
			return 0;
		}
		return (Decimal) results.get(0).get('repeatRate');
	}

	//build thisYear object
	private Map<String, Object> getYearObject(List<AggregateResult> tasksResult,
											  List<AggregateResult> walkInConvertedResult,
											  List<AggregateResult> campaignCompletedResult,
											  List<AggregateResult> saResult,
											  Date startDate,
											  List<String> userIds) {
		Map<String, Object> ret = new Map<String, Object>();

		ret.put('contacted', getContactedData(tasksResult, startDate, userIds));
		ret.put('walkin', getWalkinData(walkInConvertedResult, startDate, userIds));
		ret.put('OTO', getOTOData(campaignCompletedResult));
		ret.put('total', getTotalSalesData(saResult));
		return ret;
	}
	
    //build total sales object
	private Map<String,Object> getTotalSalesData(List<AggregateResult> saResult) {
		Map<String, Object> ret = new Map<String, Object>();
		AggregateResult result = !saResult.isEmpty() ? saResult.get(0) : null;
		System.debug(result);
		ret.put('clients', result.get('clientsCount'));
		ret.put('purchased', result.get('totalAmount'));
		return ret;
	}

	private Map<String,Object> getOTOData(List<AggregateResult> campaignCompletedResult) {
		Map<String, Object> ret = new Map<String, Object>();
		AggregateResult result = !campaignCompletedResult.isEmpty() ? campaignCompletedResult.get(0) : null;
		if (result != null){
			ret.put('sales', result.get('purchaseOTO'));
			ret.put('completed', result.get('completOTO'));
		}
		return ret;
	}

	private Map<String,Object> getWalkinData(List<AggregateResult> walkInResult, Date startDate, List<String> userIds) {
		Map<String, Object> ret = new Map<String, Object>();
		Decimal registered = 0;

		for (AggregateResult result : walkInResult){
//			if ((Boolean)result.get('conversionWithinRange')) {
				ret.put('converted', result.get('clientCount'));
//			}
//			else{
//				registered += (Decimal)result.get('clientCount');
//			}
		}

		ret.put('registered', getWalkInRegisteredResult(userIds, startDate));
		ret.put('sales', getConvertedWalkingGeneratedSales(startDate, userIds));
		return ret;

	}

	private Map<String,Object> getContactedData(List<AggregateResult> tasksResult,
												Date startDate,
												List<String> userIds) {
		Map<String, Object> ret = new Map<String, Object>();

		for (AggregateResult result : tasksResult){
			if (result.get('year') == null && result.get('segmentGrp') == 1){
				ret.put('clients', result.get('ContactedClientsCount'));
			}
		}
		ret.put('sales', getContactedClientsGeneratedSales(startDate, userIds));
		return ret;
	}
	
    //build Segment object
	private List<Object>  getSegmentsObject(List<AggregateResult> transactionResult, List<AggregateResult> tasksResult, List<AggregateResult> clientsResult) {
		Map<String,Object> ret = new Map<String, Object>();
		Decimal totalSales = getTotalSales(transactionResult);
		Decimal totalContacted = getTotalContacted(tasksResult);

		for (AggregateResult result : tasksResult){
			Map<String,Object> obj = getObject(ret, result);
			if (obj != null){
				obj.put('contacted', result.get('contactedClientsCount'));
				Decimal value = totalContacted != 0 ? ((Decimal)result.get('contactedClientsCount')).divide(totalContacted, 2): 0;
				obj.put('contactedPercentage', value);
			}
		}

		for (AggregateResult result : transactionResult){
			Map<String,Object> obj = getObject(ret, result);
			if (obj != null) {
				obj.put('sales', result.get('sales'));
				Decimal value = totalSales != 0 ? ((Decimal)result.get('sales')).divide(totalSales, 2): 0;
				obj.put('salesPercentage', value);
			}
		}

		for (AggregateResult result : clientsResult){
			Map<String,Object> obj = getObject(ret, result);
			if (obj != null) {
				obj.put('clientsCount', result.get('clientsCount'));
			}
		}
		System.debug(ret);
		return convertSegmentsToList(ret);
	}

	private Decimal getTotalContacted(List<AggregateResult> tasksResult) {
		for (AggregateResult result : tasksResult){
			if (result.get('year') == null && result.get('segmentGrp') == 1 ){
				return (Decimal) result.get('contactedClientsCount');
			}
		}
		return 0;

	}

	private Decimal getTotalSales(List<AggregateResult> transactionResult ){
		for (AggregateResult result : transactionResult){
			if (result.get('year') == null && result.get('segmentGrp') == 1 ){
				return (Decimal) result.get('sales');
			}
		}
		return 0;
	}

	private List<Object> convertSegmentsToList(Map<String, Object> ret) {

		List<Object> objectList = new List<Object>();
		for (String key : ret.keySet()){
			if (ret.get(key) instanceof Map<String, Object>){
				Map<String, Object> newObj = new Map<String, Object>();
				newObj.putAll((Map<String,Object>)ret.get(key));
				newObj.put('name', key);
				objectList.add(newObj);
			}
		}
		return objectList;
	}

	private Set<Id> getContactedClient(Date startDate, List<String> userIds) {
		Set<Id> ret = new Set<Id>();

		/*for (Task task : [SELECT AccountId
							FROM Task
							WHERE OutreachType__c != NULL AND Activity_Date__c != NULL  AND Account.PreferredSA1__pc IN :userIds
								AND ( (Activity_Date__c >= :startDate AND Activity_Date__c <= :today))]){
			ret.add(task.AccountId);
		}*/
		for (Account acc  : [SELECT Id
							FROM Account
							WHERE LastContactDate__pc != NULL  AND PreferredSA1__pc IN :userIds
								AND ( (LastContactDate__pc >= :startDate AND LastContactDate__pc <= :today))]){
			ret.add(acc.Id);
		}
		return ret;
	}

	private Decimal getContactedClientsGeneratedSales(Date startDate, List<String> userIds) {
		System.debug(userIds);
		List<AggregateResult> result = [SELECT SUM(SalesAmountIncludingTax__c) sales
										FROM TransactionLine__c
										WHERE Client__c IN :getContactedClient(startDate, userIds) AND SalesDate__c != NULL  AND SA__C IN :userIds
											AND(/*(OrderDate__c >= :startDate.addYears(-1) AND OrderDate__c <= :today_A_Year_Ago)
												OR*/(SalesDate__c >= :startDate AND SalesDate__c <= :today))];
		System.debug(result);
		if (!result.isEmpty() && result.get(0).get('sales') != null){
			return (Decimal)result.get(0).get('sales');
		}
		return 0;
	}



	private List<AggregateResult> getTasksResult(List<String> userIds, Date startDate) {
		List<AggregateResult> tasksAgg = [SELECT COUNT_DISTINCT (AccountId) contactedClientsCount, CALENDAR_YEAR(Activity_Date__c) year, Account.CurrentSegment__pc segment, GROUPING(Account.CurrentSegment__pc) segmentGrp
											FROM Task
											WHERE OutreachType__c != NULL AND Activity_Date__c != NULL AND Account.PreferredSA1__pc IN :userIds
												AND (/*(Activity_Date__c >= :startDate.addYears(-1) AND Activity_Date__c <= :today_A_Year_Ago)
													OR*/ (Activity_Date__c >= :startDate AND Activity_Date__c <= :today))
											GROUP BY ROLLUP( CALENDAR_YEAR(Activity_Date__c), Account.CurrentSegment__pc)];
		return tasksAgg;
	}

	private List<AggregateResult> getSA_Result(List<String> userIds, Date startDate) {
		List<AggregateResult> ret = [SELECT SUM(NbOfClients__c) clientsCount, SUM(TotalSalesEUR__c) totalAmount, SUM(NumOfTransactions__c) transactionCount,
											SUM(NumOfProducts__c) productsCount
									FROM SATurnover__c
									WHERE Date__c >= :startDate AND Date__c <= TODAY AND SA__c IN :userIds];
		System.debug(userIds);
		return ret;
	}


	private List<AggregateResult> getTransationResult(List<String> userIds, Date startDate) {
		System.debug(startDate);
        
		List<AggregateResult> transactions;
		if (userIds.size()> 1){
			List<User> users = [SELECT BaseStoreCode__c FROM User WHERE Id = :userIds.get(0)];
			String storeId = [SELECT Id FROM Store__c WHERE StoreCode__c = :users.get(0).BaseStoreCode__c].Id ;
			transactions = [SELECT CALENDAR_YEAR(SalesDate__c) year, Client__r.CurrentSegment__pc segment, SUM(SalesAmountIncludingTax__c) sales, GROUPING(Client__r.CurrentSegment__pc) segmentGrp
			FROM TransactionLine__c
			WHERE SalesDate__c != NULL AND Store__c = : storeId AND SA__c IN : userIds
			AND(/*(OrderDate__c >= :startDate.addYears(-1) AND OrderDate__c <= :today_A_Year_Ago)
														OR(OrderDate__c >= :startDate AND OrderDate__c <= :today))
														OR*/(SalesDate__c >= :startDate AND SalesDate__c <= :today))
			GROUP BY ROLLUP(CALENDAR_YEAR(SalesDate__c), Client__r.CurrentSegment__pc)];

		}
		else{
			transactions = [SELECT CALENDAR_YEAR(SalesDate__c) year, Client__r.CurrentSegment__pc segment, SUM(SalesAmountIncludingTax__c) sales, GROUPING(Client__r.CurrentSegment__pc) segmentGrp
			FROM TransactionLine__c
			 WHERE SalesDate__c != NULL AND SA__c IN :userIds
			 AND Client__r.PreferredSA1__pc IN :userIds
			 AND (/*(OrderDate__c >= :startDate.addYears(-1) AND OrderDate__c <= :today_A_Year_Ago)
			 											//OR(OrderDate__c >= :startDate AND OrderDate__c <= :today))
			 											//OR*/(SalesDate__c >= :startDate AND SalesDate__c <= :today))
			 GROUP BY ROLLUP(CALENDAR_YEAR(SalesDate__c), Client__r.CurrentSegment__pc)];
			System.debug('transactions:'+transactions);


		}
		return transactions;
	}


	private List<AggregateResult> getClientsResult(List<String> userIds, Date startDate) {
		List<AggregateResult> ret = [SELECT COUNT(Id) clientsCount, CurrentSegment__pc segment, GROUPING(CurrentSegment__pc) segmentGrp
												FROM Account
												WHERE PreferredSA1__pc IN :userIds
												AND CurrentSegment__pc != null 
												AND CurrentSegment__pc != 'NO_SEGMENT' 
												GROUP BY ROLLUP(CurrentSegment__pc)];
		return ret;

	}

    //need to be changed to retrieve Events instead of Campaign Members
    //one campaign Member can generate multiple OTOs
	/*private List<AggregateResult> getCampaignCompletedResultOld( Date startDate,List<String> ids){
		List<AggregateResult> ret = [SELECT Count(Id) completOTO, SUM(Purchase__c) purchaseOTO, CALENDAR_YEAR(AppointmentDate__c) year
									FROM CampaignMember
									WHERE Campaign.Type in ('OTO','Event' ) 
									AND Status = 'Show Yes'
									AND AppointmentDate__c > :startDate
									
										
											AND Contact.PreferredSA1__c in :ids
									GROUP BY  CALENDAR_YEAR(AppointmentDate__c)];
		return ret;
	}
	*/
	
	private List<AggregateResult> getCampaignCompletedResult( Date startDate,List<String> ids){
		List<AggregateResult> ret = [SELECT Count(Id) completOTO, SUM(PurchaseEUR__c) purchaseOTO, CALENDAR_YEAR(Activity_Date__c) year
									FROM Event
									WHERE Type in ('OTO','Event' ) 
									AND Show__c = 'Yes'
									AND Activity_Date__c > :startDate
									
										//AND ((Campaign.StartDate >= :startDate.addYears(-1) AND Campaign.StartDate <= :today_A_Year_Ago)
											//OR(Campaign.StartDate >= :startDate AND Campaign.StartDate <= :today))
											AND OwnerID in :ids
									/*AND AppointmentDateTime__c =''*/
									GROUP BY  CALENDAR_YEAR(Activity_Date__c)];
		return ret;
	}


	private List<AggregateResult> getWalkInConvertedResult(List<String> userIds, Date startDate) {
		List<AggregateResult> ret = [SELECT COUNT(Id) clientCount
												FROM Account
												WHERE ConversionWithinRange__pc = true 
												AND WalkIn__pc = true
												AND PreferredSA1__pc IN :userIds
			 										AND(/*(FirstPurchaseDate__pc >= :startDate.addYears(-1) AND FirstPurchaseDate__pc <= :today_A_Year_Ago)
														OR*/(FirstPurchaseDate__pc >= :startDate AND FirstPurchaseDate__pc <= :today)) ];
		return ret;

	}


	private Decimal getWalkInRegisteredResult(List<String> userIds, Date startDate) {
		List<AggregateResult> result = [SELECT COUNT(Id) clientCount
									FROM Account
									WHERE  PreferredSA1__pc IN :userIds
									AND WalkIn__pc = true
										AND(FirstSourceCreationDate__c >= :startDate AND FirstSourceCreationDate__c <= :today) ];

		if (!result.isEmpty() && result.get(0).get('clientCount') != null){
			return (Decimal)result.get(0).get('clientCount');
		}
		return 0;
	}



	private Decimal getConvertedWalkingGeneratedSales(Date startDate, List<String> userIds) {
		List<AggregateResult> result = [SELECT SUM(SalesAmountIncludingTax__c) sales
										FROM TransactionLine__c
										WHERE Client__r.ConversionWithinRange__pc = true
											AND Client__r.FirstPurchaseDate__pc >= :startDate 
											AND Client__r.FirstPurchaseDate__pc <= :today
											AND Client__r.PreferredSA1__pc IN :userIds
											AND Client__r.WalkIn__pc = true
											AND SalesDate__c != NULL 
											AND FirstPurchase__c = true
											AND SA__C IN :userIds
											AND(/*(OrderDate__c >= :startDate.addYears(-1) AND OrderDate__c <= :today_A_Year_Ago)
																				OR*/(SalesDate__c >= :startDate AND SalesDate__c <= :today))];

		System.debug(result);
		if (!result.isEmpty() && result.get(0).get('sales') != null){
			return (Decimal)result.get(0).get('sales');
		}
		return 0;
	}






	private Date getStartDate(String period) {
		switch on period.toLowerCase(){
			when 'w'{
				return System.today().toStartOfWeek();
			}
			when 'm'{
				return System.today().toStartOfMonth();
			}
			when 'y'{
				return System.today().addDays(-System.today().dayOfYear() + 1);
			}
		}
		return null;
	}


	public Object retrieveDelete(Map<String, String> params) {
		return null;
	}

	private Map<String, Object> getObject(Map<String, Object> ret, AggregateResult result) {
		if (result.get('segmentGrp') == 1) {
			return null;
		}
		String key = result.get('segment') != null ? (String)result.get('segment') : 'others';
		if (!ret.containsKey(key)) {
			ret.put(key, new Map<String, Object>());
			}
		return (Map<String, Object>)ret.get(key);
	}
}