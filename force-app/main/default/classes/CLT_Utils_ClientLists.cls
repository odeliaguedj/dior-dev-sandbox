/*
build Dynamic Query string for ClientList__c object
*/
global with sharing class CLT_Utils_ClientLists {
    global static String buildDynamicQuery(ClientList__c clientList, User me, string storeCode){

        return 'SELECT Id from Account WHERE id != \'\' ' + clientList.SOQLQueryConditions__c + CLT_Utils_Query.getMyClientsCondition(me) + ' LIMIT 400';
    }
}