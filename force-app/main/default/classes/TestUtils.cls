public class TestUtils {
	public static final Id paRtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    
    public static User createUser(String uName) {
		Profile sysAdmin = getSysAdminUserProfile();
		User u = new User(
				UserName = '' + UserInfo.getOrganizationId() + System.now().millisecond() + 'test@test111k.net', // Avoiding duplicates
				Email ='test@test111.net',
				LastName = 'test',
				ProfileId = sysAdmin.Id,
				LocaleSidKey = 'en_US',
				LanguageLocaleKey = 'en_US',
				TimeZoneSidKey = 'GMT',
				EmailEncodingKey = 'UTF-8',
				Alias = 'user4t_',
				BaseStoreCode__c = 'TE1');

		return u;
	}

	public static Profile getSysAdminUserProfile() {
		return [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
	}
    
    
    public static  void createPersonAccounts() {
		
		Id currentUserId = UserInfo.getUserId();
		Store__c store = new Store__c(Name = 'Test Store', StoreCode__c = 'TESTCODE',Zone__c = 'EU', Country__c = 'FRA');
		insert store;
		UserStore__c us = new UserStore__c (User__c = currentUserId, Store__c = store.Id, CurrentStore__c = true);
		insert us;
		
		List<Account> newAccounts = new List<Account>();
		for(Integer i=0;i<200;i++){
			newAccounts.add(new Account (FirstName = 'Test FN',
										LastName = 'Test LN',
										RecordTypeId = paRtId ,
										Optin__pc = true,
										Nationality__pc = 'FRA',
										PersonMailingCountry = 'FRA',
										Birthday__pc = String.valueOf(Date.today().addDays(14).Day()),
										Birthmonth__pc =String.valueOf(Date.today().addDays(14).month()),
										Birthyear__pc= String.valueOf(Date.today().addDays(14).year()),
										CurrentSegment__pc = 'Elite',
										PreferredStore1__pc = store.Id,
										PreferredStore2__pc = store.Id,
										PreferredStore3__pc = store.Id,
										PreferredSa1__pc = currentUserId));
		}        
		
		insert newAccounts;
    }
    
    public static  void createTransactionLines() {
        Account client = [SELECT Id FROM Account Limit 1];

        List<TransactionLine__c> newTransaction = new List <TransactionLine__c>();
		newTransaction.add(new TransactionLine__c(Client__c = client.Id, SalesDate__c = Date.today()));
		insert newTransaction;
		List<TransactionLine__c> newTransactionLines = new List <TransactionLine__c>();
        for(Integer i=0;i<200;i++){
            newTransactionLines.add(new TransactionLine__c (Client__c = client.Id, SalesDate__c = Date.today()));
		}        
        
		insert newTransactionLines;
	}
}