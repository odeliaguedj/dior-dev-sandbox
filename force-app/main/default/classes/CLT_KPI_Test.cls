@isTest


public class CLT_KPI_Test {

    @TestSetup
    public static void makeData(){
    User u = [SELECT id,BaseStoreCode__c FROM User where BaseStoreCode__c<>null LIMIT 1 ];
    Store__c s=new Store__c();
    s.StoreCode__c=u.BaseStoreCode__c;
    insert s;
    TransactionLine__c c=new TransactionLine__c();
    insert c;
    TransactionLine__c t=new TransactionLine__c();
    //t.Transaction__c=c.id;
    t.SA__c=u.id;
    t.Store__c=s.Id;
    t.SalesAmountIncludingTax__c=200;
    t.SalesDate__c=System.today();
    insert t;
    }
    @isTest
    private static void tests1() {
        CLT_KPI c = new CLT_KPI();
        c.retrievePost(new Map<String, String>(), new Map<String, Object>());
        c.retrieveDelete(new Map<String, String> ());
    }
    
    @isTest
    private static void tests0() {
        CLT_KPI c = new CLT_KPI();
        Map<String, String> myMap = new Map<String, String>();
         User u = [SELECT id,BaseStoreCode__c FROM User where BaseStoreCode__c<>null LIMIT 1 ];
        System.runAs(u){
        	myMap.put('userIds' , '["'+u.Id+'"]');
            myMap.put('myStore', 'true');
        	c.retrieveGet(myMap);
        }
    }
    
    @isTest
    private static void tests() {
        CLT_KPI c = new CLT_KPI();
        Map<String, String> myMap = new Map<String, String>();
        User u = TestUtils.createUser('Test KPI');
        insert u;
        List<User> U_list=[SELECT id FROM User where BaseStoreCode__c<>null LIMIT 2];
        System.runAs(u){
        	myMap.put('userIds' , '["'+U_list[0].Id+','+U_list[1].Id+'"]');
            myMap.put('myStore', 'true');
            myMap.put('period', 'j');
        	c.retrieveGet(myMap);
        }
    }

    
    @isTest
    private static void tests2() {
        CLT_KPI c = new CLT_KPI();
        Map<String, String> myMap = new Map<String, String>();
         User u = [SELECT id,BaseStoreCode__c FROM User where BaseStoreCode__c<>null LIMIT 1 ];
        System.runAs(u){
        	myMap.put('userIds' ,'["'+u.Id+'"]');
            myMap.put('myStore', 'true');
            myMap.put('period', 'm');
        	c.retrieveGet(myMap);
        }
    }
    
    @isTest
    private static void tests6() {
        CLT_KPI c = new CLT_KPI();
        Map<String, String> myMap = new Map<String, String>();
        User u = [SELECT id,BaseStoreCode__c FROM User where BaseStoreCode__c<>null LIMIT 1 ];
        System.runAs(u){
        	myMap.put('userIds' , '["'+u.Id+'"]');
            myMap.put('myStore', 'true');
            myMap.put('period', 'w');
        	c.retrieveGet(myMap);
        }
    }
    

   }