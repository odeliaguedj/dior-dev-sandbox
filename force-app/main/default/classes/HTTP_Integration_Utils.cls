public with sharing class HTTP_Integration_Utils {

	private static String token;

	public static APIManagerConstants__c getToken( String tokenType){

		APIManagerConstants__c integData = APIManagerConstants__c.getInstance();
        System.debug('WHAT integData:'+integData);
		if (token != null){
			integData.AccessToken__c = token;
			return integData;
		}

	
		String body = getBodyRequest(integData, tokenType);
		try{
			HTTP_UTILS response =
					new HTTP_UTILS()
							.post(integData.loginURL__c)
							.header('Ocp-Apim-Subscription-Key', integData.ApimSubscriptionKey__c )
							.header('Ocp-Apim-Trace', integData.ApimTrace__c )
							.timeout(60000)
							.body(body)
							.call();
			System.debug('response::'+response);
			if (Test.isRunningTest()){
				return integData;
			}

			if(response.statusCode() != 200){
				if (tokenType == 'refreshToken' && response.statusCode() == 400){
					return getToken('accessToken');
				}
				throw new HTTP_UTILS.Custom_Exception(response.status());
			}
			integer expireInSeconds = Integer.valueOf(response.getParameters().get('expires_in')) - 5;
			token = response.getParameters().get('access_token');
			integData.AccessToken__c = response.getParameters().get('access_token');
			integData.RefreshToken__c = response.getParameters().get('refresh_token');
			integData.ExpireDate__c = Datetime.now().addSeconds(expireInSeconds);

			return integData;
		}
		catch(Exception ex){
			System.debug(ex.getMessage());
			System.debug(ex.getStackTraceString());
			throw new HTTP_UTILS.Custom_Exception(ex.getMessage());
		}
	}

	public static string getBodyRequest(APIManagerConstants__c integData, String tokenType){
		String body= '';

		if (tokenType == 'refreshToken'){
			String refreshToken = integData.RefreshToken__c;
            
			body = 'grant_type=refresh_token' +
					'&Refresh_token=' + EncodingUtil.urlEncode(refreshToken, 'UTF-8');
		} else if (tokenType == 'accessToken'){
            
			body = 'grant_type=client_credentials' +
					'&client_secret=' + EncodingUtil.urlEncode(integData.ClientSecret__c, 'UTF-8') +
					'&client_id=' + EncodingUtil.urlEncode(integData.ClientID__c, 'UTF-8') +
					'&resource=' + EncodingUtil.urlEncode(integData.Resource__c, 'UTF-8') ;

		}
		System.debug(body);
		return body;

	}

	public static APIManagerConstants__c getAccessToken(){
		APIManagerConstants__c integData = APIManagerConstants__c.getInstance();
		if (integData == null || String.isEmpty(integData.AccessToken__c)){
			integData = getToken('accessToken');
//			insert integData;
//			
		} 
        
		else if (integData.ExpireDate__c != null && integData.ExpireDate__c < Datetime.now()){
			integData = getToken('refreshToken');
//			insert integData;
		}
        
		return integData;
	}

}