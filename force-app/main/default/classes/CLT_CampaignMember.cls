/*
This class manages the HTTPS requests of the CampaignMember object web service
*/

public with sharing class CLT_CampaignMember implements CLT_RetailInterface {
    public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
         return upsertItems(body);
    }

    public Object retrieveGet(Map<String, String> params){
        return null;
    }

    public static list<map<string, object>> upsertItems(Map<String,Object> body) {
        String objectName = (String) body.get('objectName');
        List<Object> items = (List<Object>) body.get('items');
        Set<String> itemIds = new Set<String>();
        String userStoreId = CLT_Utils.getUserStoreId();

        List<Map<String, Object>> mapCampaignMembers = CLT_Utils_Mapping.getMapObjectsByWrapperItems('CampaignMember', 'CampaignMember', items);
        System.debug(mapCampaignMembers);
        List<CampaignMember> campaignMembers = (List<CampaignMember>) JSON.deserialize(JSON.serialize(mapCampaignMembers), List<CampaignMember>.class);
        
        for(CampaignMember cm :campaignMembers){
        	cm.UniqueUpsertId__c = String.valueOf(cm.CampaignId) + String.valueOf(cm.ContactId); 
        	if( !cm.Hidden__c ) cm.Hidden__c = false;                                              
        }
        
		System.debug(campaignMembers);
        upsert campaignMembers UniqueUpsertId__c;  
        
        //upsert campaignMembers;  

        for(CampaignMember item: campaignMembers) {
            itemIds.add(item.Id);
        }

        if(itemIds.size() > 0) {
            return CLT_Utils_Mapping.getStandardWrapperList('CampaignMember', 'CampaignMember',
                            ' FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(itemIds) + ' AND Hidden__c = false ORDER BY CreatedDate DESC');
        }    
        return null;    
    }
}