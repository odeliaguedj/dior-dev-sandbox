@isTest
public class CampaignMemberTrigger_Test {
    @isTest 
    private static void testInsert() {
        Campaign cmp = new Campaign(name='Campaign_', TECH_LaunchAddMembersBatch__c = true, TECH_ContactIds__c = '!');
        insert cmp; 
        
        Contact contact = new Contact(LastName = 'LastName');
        insert contact;

        CampaignMember campaignMember = new CampaignMember(CampaignId=cmp.Id, CurrencyIsoCode='CAD', ContactId =  contact.Id, Status = 'Cannot Be Reached', AppointmentDateTime__c = System.now());
        insert campaignMember;
    }    
    
	@isTest 
    private static void testUpdate() {
        Campaign cmp = new Campaign(name='Campaign_', TECH_LaunchAddMembersBatch__c = true, TECH_ContactIds__c = '!');
        insert cmp;
        
        Contact contact = new Contact(LastName = 'LastName');
        insert contact;
        
        CampaignMember campaignMember = new CampaignMember(CampaignId=cmp.Id, CurrencyIsoCode='CAD', ContactId =  contact.Id, Status = 'No Show', AppointmentDateTime__c = System.now());
        insert campaignMember;
        
        Event appointment = new Event(StartDateTime = DateTime.now(), EndDateTime = DateTime.now());
        insert appointment;
        
        campaignMember.Status ='Show Yes';
        update campaignMember;
    }        
}