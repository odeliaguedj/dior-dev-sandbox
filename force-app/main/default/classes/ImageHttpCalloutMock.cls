@isTest
global class ImageHttpCalloutMock implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        /*  String url ='https://uat-api-central.christiandior.com/diamant-public/product/stylecolor/0691t00000Ae99R/asset/0691t00000Ae99R.jpg?rendition=Low_Resolution';
        HttpResponse response = new HTTP_UTILS().get(url)
				.header('Content-Type', 'application/json')
				.header('Ocp-Apim-Subscription-Key', '0cff11f3784840148f3dc94b70340457')
				.header('Ocp-Apim-Trace', 'true')
				.header('Authorization', 'BearerOMNI_1_WOM')
				.call()
 				.getResponse();
        response.setStatusCode(200);*/
        
      HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setHeader('Content-Type', 'application/json');
        response.setBody('success');
        return response;

    }
}