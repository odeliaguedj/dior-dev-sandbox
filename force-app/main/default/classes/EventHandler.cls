public with sharing class EventHandler {
	
	public static boolean eventTriggerContext = false;
	
	
	public static Map<Id,Account> clientsMap;
	
	public static Map<Id,Account> getClientsMap(List<Event> triggerNew){
		Set<String> clientsIds = new Set<String>();
		
		if(clientsMap == null){
			for(Event item: triggerNew) {
	            clientsIds.add( item.WhatId );
	        }
			clientsMap = new Map<Id,Account> ([select Id,PersonContactId, Name  from Account where Id in :clientsIDs]);
		}
		
		return clientsMap;
	}
	
	//set a few field updates on Insert Update
	public static void setEventFields(List<Event> triggerNew){
		
	    clientsMap = getClientsMap( triggerNew);
		
		for(Event item :triggerNew){
			//For OTO appointments:
			//1. Overwrite subject 
			//2. set unique OTO ID
			//3. set show Yes if Completed
			if( (item.Type == 'OTO' || item.Type == 'Event')  && item.Campaign__c != null &&  clientsMap.get(item.WhatId) != null){
				item.Subject = item.CampaignName__c + ' ' + clientsMap.get(item.WhatId).Name;
				item.CampaignMemberId__c = String.valueOf(item.Campaign__c ) +  clientsMap.get(item.WhatId).PersonContactId;
				if(item.Status__c == 'Completed' && item.Show__c == null) item.Show__c = 'Yes';
			}
		
		}
	}
	
	public static void upsertOTOs(List<Event> triggerNew){
		List<CampaignMember> otosToUpsert = new List<CampaignMember> ();
        Set<String> otosToUpsertIds = new Set<String> ();
        Map<String,Event> uniqueCampaignEvents = new Map<String,Event> ();
        
		clientsMap = getClientsMap( triggerNew);
        
        //for each event, if this is an OTO, upsert the campaigMember for the relevant campaign
		for(Event item :triggerNew){
			if( item.Campaign__c != null && clientsMap.get(item.WhatId) != null ){
                String contactId = clientsMap.get(item.WhatId).PersonContactId;
                uniqueCampaignEvents.put(String.valueOf(item.Campaign__c ) +  contactId,item); 
                
            }
		}
        List<CampaignMember> existingMembers = [select Id,UniqueUpsertId__c,AppointmentIds__c, AppointmentId__c From CampaignMember where UniqueUpsertId__c in :uniqueCampaignEvents.keySet()];
        Map<String,CampaignMember> existingMembersMap = new  Map<String,CampaignMember>  ();
        for(CampaignMember cm :existingMembers){
            existingMembersMap.put(cm.UniqueUpsertId__c , cm);
        }
        
        for(Event item :triggerNew){  
        	if( item.Campaign__c != null && clientsMap.get(item.WhatId) != null ){  
	            String contactId  = clientsMap.get(item.WhatId).PersonContactId;
	            String otoUniqueId = String.valueOf(item.Campaign__c ) + contactId;
	            CampaignMember existingMember = existingMembersMap.get(otoUniqueId);
	            
	            CampaignMember cm = new CampaignMember( CampaignId = item.Campaign__c , 
	                                                    //ContactId = contactID,
	                                                    Hidden__c = false,
	                                                        UniqueUpsertId__c = String.valueOf(item.Campaign__c ) +  contactId, 
	                                                        Status = item.Status__c == 'Completed' ? 'Show Yes' : 'Confirmed',
	                                                        AppointmentDateTime__c = item.StartDateTime );
	            if (existingMember == null){
	            	
	                cm.ContactId = contactId;
	                cm.AppointmentIds__c =  item.Id + ',';
	        	}
	        	else {
	        		if(existingMember.AppointmentIds__c == null  ) existingMember.AppointmentIds__c = '';
					if(existingMember.AppointmentIds__c.indexOf(item.Id) < 0 ){
						if(existingMember.AppointmentId__c != null)
						{
							item.addError(Label.OpenOTOErrorMessage);
						}
						else{
							cm.AppointmentIds__c = existingMember.AppointmentIds__c + item.Id + ',';
						}
						
					} 
					
	        	}
				if(item.Status__c != 'Completed'){
					cm.AppointmentId__c = item.Id;
				}
				else {
					cm.AppointmentId__c = '';					
				}
	    		//keep unique records in list
				if(!otosToUpsertIds.contains(cm.UniqueUpsertId__c)){
					otosToUpsertIds.add(cm.UniqueUpsertId__c);
					otosToUpsert.add(cm);
				}
        	}
       	}
		system.debug(otosToUpsert);
		if(otosToUpsert.size() > 0 ){
		    eventTriggerContext = true;
		    upsert otosToUpsert UniqueUpsertId__c;
		}
	}  
	
	public static void updateOrDeleteOTOs(List<Event> triggerOld){
		
		List<CampaignMember> otosToDelete = new List<CampaignMember> ();
		Set<String> otoIds = new Set<String>();
        
		for(Event item: triggerOld) {
            otoIds.add( item.CampaignMemberId__c );
        }
        
        List<CampaignMember> members = [select Id From CampaignMember where UniqueUpsertId__c in :otoIds];
        for(CampaignMember cm :members){
            cm.Status = 'Confirmed';
            cm.AppointmentDateTime__c = null;
            
        }
        update members;
	} 



		public static void updatePurchaseCampaignMembers(List<Event> Events, Map<Id,Event> EventsMap){ 
			Set<String> otoUniqueIds = new Set<String>();
            for (Event e: Events){
                if( (e.CampaignMemberId__c != null ) && ( Trigger.isDelete ||  (e.PurchaseEUR__c != EventsMap.get(e.Id).PurchaseEUR__c))){
                    otoUniqueIds.add(e.CampaignMemberId__c);
                }
            }

			if(otoUniqueIds.size() > 0){
				updatePurchaseCampaignMembers (otoUniqueIds);
			}
		}

	public static void updatePurchaseCampaignMembers (Set<String> otoUniqueIds){ 
		List<CampaignMember> campaignMembers = [Select Id, Purchase__c,UniqueUpsertId__c FROM CampaignMember where UniqueUpsertId__c in :otoUniqueIds];
		List<Event> events = [Select Id, PurchaseEUR__c,CampaignMemberId__c FROM Event where CampaignMemberId__c in :otoUniqueIds];
		Map<String,List<Event>> eventsMap = new Map<String,List<Event>>();
		System.debug(otoUniqueIds.size());
		for(Event ev :events){
			if(eventsMap.get(ev.CampaignMemberId__c) == null)
				eventsMap.put(ev.CampaignMemberId__c  , new List<Event>());
			
			eventsMap.get(ev.CampaignMemberId__c).add(ev);
		}
		

		for(CampaignMember cm :campaignMembers){
			
			List<Event> MyEvents = eventsMap.get(cm.UniqueUpsertId__c);
			cm.Purchase__c = 0;
			for (Event e: Events) {
				if(e.PurchaseEUR__c != null) {
					System.debug('chui pas null');
					cm.Purchase__c += e.PurchaseEUR__c;
				}
				
			}
		}

		update campaignMembers;
	}

}