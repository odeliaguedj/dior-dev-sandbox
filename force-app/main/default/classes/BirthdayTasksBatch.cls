global class BirthdayTasksBatch implements Database.Batchable<sObject>,Database.stateful {
    global  String Query;
    global  Date dateFilter = Date.today().addDays(14);
    global Integer year = Date.today().year();
    global String taskRTId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Action').getRecordTypeId();

    global Database.QueryLocator start(Database.BatchableContext BC){ 
        
        query = 'SELECT Id, Name, ownerid, Birthmonth__pc, Birthday__pc, NextBrithdayDate__pc FROM Account Where  NextBrithdayDate__pc = :dateFilter and CurrentSegment__pc in (\'Elite\', \'VVIC\', \'SUPER_ELITE\')' +
        ' and owner.isactive = true and owner.StaffCode__c != null ';
        System.debug(dateFilter);
        System.debug(query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('### try');
        List<Task> lstTask=new List<Task>();        
        try{
            System.debug('### try2');
            
           for(sObject o:scope){
                Account client = (Account)o;
                //String birthDay = client.Birthday__pc;
                //Integer day = Integer.valueOf(birthDay); 

                    Task task = new Task();
                    task.WhatId = client.Id;
                    task.OwnerId = client.OwnerId;
                    task.status ='new';
                    task.Subject = client.Name + '\'s Bday. Reminder';//Label.IC_Birthday_Reminder;
                    task.priority = 'Normal';
                    task.ActivityDate = client.NextBrithdayDate__pc;//date.newInstance(year, thisMonth, day);
                    task.BirthdayReminder__c = true;
                    task.RecordTypeId = taskRTId;
                    task.IsPinned__c = true;
                    task.IsShownInCalendar__c = true;
                    task.Type = 'Birthday';
                    //task.TECH_is_AutoSave__c = true;
                    lstTask.add(task);
                    System.debug('### task added to the list of task');
               
            }
            //insert lstTask;
            Database.SaveResult[] resultats = database.insert(lstTask, false);
            for(Database.SaveResult sr : resultats){
                if (!sr.isSuccess()) {
                    // Operation was not successful, so get the ID of the record that was processed
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('### an error has occurred where inserting task : ' + 
                                    err.getStatusCode() + '  :  ' + err.getMessage() + 
                                    ', task fields that affected this error: ' + err.getFields());
                    }
                }
            } 
            System.debug('tasks inserted');
            //insert lstTask;
        }
        catch (DmlException e){
            // Process exception here
            System.debug('#### '+e.getTypeName()+' Exception:'+e.getMessage()+' '+e.getStackTraceString());
        }   
    }

    global void finish(Database.BatchableContext BC){
        
    }
}