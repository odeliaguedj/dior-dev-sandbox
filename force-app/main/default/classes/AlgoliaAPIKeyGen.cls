/*
 * Created by israel on 4/11/2019.
 */
public class AlgoliaAPIKeyGen implements Schedulable{

    public Void execute(System.SchedulableContext context) {
        callWS();

    }
    @future(callout=true)
    public static void callWS() {
        ProductCatalog__c productCatalog = ProductCatalog__c.getOrgDefaults();

        String body = '{ "description" : "iOS App 72h key", "indexes": ["PRODUCTS_*","FRA","GBR"], "validity": 259200, "acl" : ["search","browse"] }';

        try {
//            if(Test.isRunningTest()){
            System.HttpResponse httpResponse = new HTTP_UTILS()
                    .post('https://'+ productCatalog.AlgoliaApplicationID__c +'-dsn.algolia.net/1/keys')
                    .header('X-Algolia-API-Key', productCatalog.AlgoliaAdminAPIKey__c)
                    .header('X-Algolia-Application-Id', productCatalog.AlgoliaApplicationID__c)
                    .body(body)
                    .call()
                    .getResponse();

            Map<String, Object> responseBody = (Map<String, Object>)JSON.deserializeUntyped(httpResponse.getBody());

            productCatalog.AlgoliaAPIKey__c = (String)responseBody.get('key');
            productCatalog.AlgoliaAPIKeyCreatedDate__c = Datetime.valueOf(((String)responseBody.get('createdAt')).replace('T',' '));

            update productCatalog;

//        }
    }
        catch (Exception e){
            System.debug(e.getMessage());
        }

    }
}