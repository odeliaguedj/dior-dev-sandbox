@isTest
public class CLT_UtilsPicklist_Test {
    
    @isTest static void testOne() {
        List<String> values = new List<String>();
        for (CLT_Picklist__mdt mdt : [SELECT Id, FieldName__c FROM CLT_Picklist__mdt]){
            values.add(mdt.FieldName__c);
        }
        CLT_UtilsPicklist.getPicklists(values);
    }
    
           // string query =  'Select Id, FieldName__c, ObjectName__c, Values__c, Exclude_Fields__c, ControllingFieldName__c, Take_All_Object_List__c ' + 
           //'From CLT_Picklist__mdt ' +
           //'WHERE Active__c = TRUE';
      
    @isTest 
    static void testMyPickListValues() {
        CLT_UtilsPicklist.MyPickListValues c = new CLT_UtilsPicklist.MyPickListValues();
		c.Label = 'label';
        c.Value = 'value';
    }
}