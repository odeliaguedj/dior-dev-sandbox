/*
This class manages the HTTPS requests of the Account object web service
 It's return all information about the customer
*/

global with sharing class CLT_ClientDetail implements CLT_RetailInterface{
    
    public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
     
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }   

    public Object retrieveGet(Map<String, String> params){
        String clientId = params.get('clientId');
        String contactId = CLT_Utils.getContactId(clientId);
        Map<String, Object> result = new Map<String, Object>();

		// products - task & activities -  notes
		result.put('clientFields', CLT_Utils_ClientDetails.getClientFields(clientId));
        result.put('addresses', CLT_Utils_ClientDetails.getAddresses(clientId));    
        result.put('relatives', CLT_Utils_ClientDetails.getRelatives(clientId));
		result.put('addresses', CLT_Utils_ClientDetails.getClientAddresses(clientId));
		result.put('purchases', CLT_Utils_ClientDetails.getPurchases(clientId));
		result.put('timelineItems', CLT_Utils_ClientDetails.getClientTimeline(clientId, contactId));
		result.put('notes', CLT_Utils_ClientDetails.getClientNotes(clientId));
        result.put('campaigns', CLT_Utils_ClientDetails.getCampaignMembers(contactId));
		Map<String, Object> clientFields = (Map<String, Object>)result.get('clientFields');
		String storeId = (String)clientFields.get('preferredStore1');
        result.put('kpi', CLT_Utils_ClientDetails.getKPI(clientId, storeId));
		System.debug(JSON.serialize(result));
		return result;
	}
}