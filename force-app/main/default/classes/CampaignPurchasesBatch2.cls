global class CampaignPurchasesBatch2  implements Database.Batchable<sObject>,Database.stateful{
	global  String Query;
	global  Date dateFilter;
   	global Integer interval;

   	global Database.QueryLocator start(Database.BatchableContext BC){ 
		
		if(interval == null) {
			interval = -7;
		}
		dateFilter = Date.today().addDays(interval); 
		//todayDate = DateTime.now().addDays(interval); 
      	system.debug(' \n\n\n\n  salesDateFilter in start 1'  + dateFilter + '\n\n\n\n ');
      	
      	/*List<TransactionLine__c> allTransactions;
      	if (salesDateFilter == null ){
      		allTransactions = [SELECT Id,SalesDate__c  
      							FROM TransactionLine__c 
      							WHERE Campaign__c =null 
      							AND Client__c != null 
      							AND SalesDate__c != null 
      							AND SalesDate__c < :Date.Today().addDays(30) 
      							AND createddate >= :todayDate //dernieres 24h
      							ORDER BY SalesDate__c ASC LIMIT 1];
			if (allTransactions.size() > 0){
				system.debug(allTransactions.size());
      			salesDateFilter = allTransactions[0].SalesDate__c; 
			}
      	}
      	*/
      	query = 'SELECT Id, Campaign__c, WhatId ,ActivityDateTime, ActivityDate , PurchaseEUR__c , OwnerId '+ 
    	         'FROM Event WHERE Campaign__c != null AND WhatId != null AND ActivityDateTime >= :dateFilter ' ; 
      	
      	return Database.getQueryLocator(query);
   	}

   	global void execute(Database.BatchableContext BC, List<Event> appointments){
   		
   		//List<Transaction__c> campaignSales = new List<Transaction__c>();
   		List<TransactionLine__c> campaignItems = new List<TransactionLine__c>();
   		Map<Id,Event> eventsToUpdate = new Map<Id, Event>();
   		Date minDate = dateFilter.addDays(0);
   		Date maxDate = dateFilter.addDays(0);
   		//system.debug(' \n\n\n\n  salesDateFilter in execute'  + salesDateFilter + '\n\n\n\n ');
   		Set<String> clientIds = new Set<String> ();
   		for (Event ev :appointments){
   			clientIds.add(ev.WhatId);
   		}
   		
   		
   		Map<Id,Account> clientsMap =  new Map<Id,Account>([SELECT Id,  (	SELECT Id, Campaign__c, Client__c ,SalesDate__c, SalesAmountIncludingTaxEUR__c , SA__c
   																		FROM TransactionLines__r 
					   													WHERE 	SalesDate__c  >= :dateFilter
																		) 
					   									FROM Account where Id in: clientIds]);
			for(Event  ev: appointments){
				ev.PurchaseEUR__c = 0;
				if(ev.WhatId != null && clientsMap.get(ev.WhatId) != null){
					Account client = clientsMap.get(ev.WhatId);
					if(client.TransactionLines__r.size() > 0 ){
						for(TransactionLine__c trans :client.TransactionLines__r){
							if(trans.SA__c == ev.OwnerId && trans.SalesDate__c == ev.ActivityDate){			
								//Update Transaction
								//trans.Campaign__c = campMem.CampaignId;
								//campaignSales.add(trans);
								
								//Update Transaction Lines
								//for(TransactionLine__c line :trans.Transaction_Lines__r){
								trans.Campaign__c = ev.Campaign__c;
								if(!campaignItems.contains(trans)){
									campaignItems.add(trans);
								}
								
								//}
						
								//update Campaign Member Total Sales
								//Need to be fixed: if batch is executed twice, it will count sales twice.
								
								//e.PurchaseEUR__c = e.PurchaseEUR__c == null ? 0 : e.PurchaseEUR__c;
								//e.PurchaseEUR__c = trans.SalesAmountIncludingTaxEUR__c;

								//Decimal totalPurchase = campMem.Purchase__c;
								ev.Status__c = 'Completed';
								ev.Show__c = 'Yes';
								ev.PurchaseEUR__c += trans.SalesAmountIncludingTaxEUR__c;
								eventsToUpdate.put(ev.Id,ev);
								
								
							}
							
						}
				}
			
			//if(campaignSales.size()> 0)
				//update campaignSales;
			
			}
		}  	

		if(campaignItems.size()> 0)
			update campaignItems;
				
		if(eventsToUpdate.size()> 0)
			update eventsToUpdate.values();	
    }

	global void finish(Database.BatchableContext BC){
		/*system.debug(' \n\n\n\n  salesDateFilter in finish'  + salesDateFilter + '\n\n\n\n ');
		List<TransactionLine__c> transactions = [SELECT Id,SalesDate__c FROM TransactionLine__c //ajoute le filtre de date de creation
								WHERE Campaign__c =null AND Client__c != null and SalesDate__c > :salesDateFilter AND createddate > :todayDate order by SalesDate__c ASC LIMIT 1];
		if (transactions.size() > 0){
  			salesDateFilter = transactions[0].SalesDate__c;
   			CampaignPurchasesBatch2 a = new CampaignPurchasesBatch2(); 
   			a.salesDateFilter = salesDateFilter;
			a.interval = interval;
			Database.executeBatch(a);
		}*/
   	}
}