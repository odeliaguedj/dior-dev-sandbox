/**
 * File         HTTP_UTILS.cls            
 * Date         Oct 2018               
 * Description  chainable httpRequest Util class.
 **********************************************************
 * History
 **********************************************************
 * Date:                ModifiedBy:             Description:                
 *
 */
public with sharing class HTTP_UTILS {

    public  String url;
    public  String method;
    public  HttpRequest request;
    private HttpResponse response;
    private Map<String,String> responseJsonParsed;
    private Map<String,String> headers; 
    private Map<String,String> urlParams;
    private static final Http http;
    static{
        http = new Http();
    }
    //Initialize the HttpReqest and headers
    public HTTP_UTILS(){
        this.request = new HttpRequest();
        this.headers = new Map<String,String>();
    }
    //Private method sets the method and url to the HttpRequest object
    private void set_uri_and_method(String method,String url){
        request.setMethod(method);
        request.setEndpoint(url);
    }
    // Set body
    public HTTP_UTILS body(String body){
        request.setBody(body);
        return this;
    }
    // Set the url for a get request.
    public HTTP_UTILS get(String url){
        set_uri_and_method('GET',url);
        return this;
    }
    //set the url for put request
    public HTTP_UTILS put(String url){
       set_uri_and_method('PUT',url);
       return this;      
    }
    //set the url for post request
    public HTTP_UTILS post(String url){
       set_uri_and_method('POST',url);
       return this; 
    }
    /*This is special header for DWOCAPI*/
    public HTTP_UTILS patch(String url){
       set_uri_and_method('PUT',url);
       return this; 
    }
    // Set the url for delete request
    public HTTP_UTILS del(String url){
       set_uri_and_method('DELETE',url);
       return this; 
    }
    // Set the request headers as value - key pair
    public HTTP_UTILS header(String key,String value){
        headers.put(key,value);
        return this;
    }
    // add all request headers as map<String,String> value-key
    public HTTP_UTILS addHeaders(Map<String,String> allHeaders){
        if(allHeaders == null || allHeaders.isEmpty()) return this;
        headers.putAll(allHeaders);
        return this;
    }
    public HTTP_UTILS timeout(Integer timeout){
        request.setTimeout(timeout);
        return this;
    }
    //Sends the request to the endpoint.
    public HTTP_UTILS call(){
        if(!headers.isEmpty()){
            for(String key : this.headers.keyset()) {
                request.setHeader(key,headers.get(key));
            }
        }
//        if(!Test.isRunningTest()){
            response = http.send(request);
//        }
        return this;
    }
    //Returns the response
    public HttpResponse getResponse(){
        return this.response;
    }
    // Returns request body
    public String responseBody(){
        return this.response.getBody();
    }
    // Returns the headers.
    public Map<String,String> getHeaders(){
        return this.headers;
    }
    //Returns the status of the response
    public String status(){
        return response.getStatus();
    }
    //Returns the statuscode of the response.
    public Integer statusCode(){
        return response.getStatusCode();
    }

    public Map<String,String> getParameters() {
        if (this.responseJsonParsed == null){
            String body = this.responseBody();
            Map<String,String> resultMap = new Map<String,String>();
            JSONParser parser = JSON.createParser(body);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                    String key = parser.getText();
                    parser.nextToken();
                    String value = parser.getText();

                    resultMap.put( key, value);
                }
            }
            this.responseJsonParsed = resultMap;
        }
        
        return this.responseJsonParsed;
    }

    public class Custom_Exception extends Exception { }
}