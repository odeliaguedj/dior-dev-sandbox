/*
 * This class is an apex controller used by the product carousel lightning component
 * Created by israel on 7/2/2019.
 */
public class ProductCarouselServerCtrl {


	@AuraEnabled
	public static List<Object> getImagesData(String clientId){
		String accessToken = HTTP_Integration_Utils.getAccessToken().AccessToken__c;
		List<Object> ret = new List<Object>();
		for (TransactionLine__c line : [SELECT Id, Product__r.ProductCode, CurrencyIsoCode, Product__r.Name, SalesDate__c, Product_Price__c, SA__r.Name, Store__r.Name,SkuCode__c
		 								FROM TransactionLine__c WHERE Client__c = :clientId]) {
			Map<String, Object> obj = new Map<String, Object>();

				obj.put('itemId', line.Id);
				obj.put('name', line.Product__r.Name);
				obj.put('date', line.SalesDate__c);
				obj.put('price', line.Product_Price__c);
				obj.put('sa', line.SA__r.Name);
				obj.put('currency', line.CurrencyIsoCode);
				obj.put('sku', line.SkuCode__c);
				obj.put('store', line.Store__r.Name);
				obj.put('image', getImage(accessToken, 'CAL44550_N0'));
//				obj.put('image', getImage(accessToken, line.ImageURLPath__c));

			ret.add(obj);
		}
		System.debug(ret);
		return ret;
	}

	public static String getImage(String accessToken, String productCode) {
		APIManagerConstants__c managerConstants = APIManagerConstants__c.getInstance();

		String url = managerConstants.URL__c + 'diamant-public/product/stylecolor/'+productCode+'/asset/'+productCode+'.jpg?rendition=Low_Resolution';
		
        HttpResponse response = new HTTP_UTILS().get(url)
				.header('Content-Type', 'application/json')
				.header('Ocp-Apim-Subscription-Key', managerConstants.ApimSubscriptionKey__c)
				.header('Ocp-Apim-Trace', managerConstants.ApimTrace__c)
				.header('Authorization', 'Bearer '+ accessToken)
				.call()
				.getResponse();
		System.debug(response.getStatusCode());

		return EncodingUtil.base64Encode(response.getBodyAsBlob());
 	}



}