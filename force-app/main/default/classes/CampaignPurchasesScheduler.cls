global class CampaignPurchasesScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        CampaignPurchasesBatch b = new CampaignPurchasesBatch();
        database.executebatch(b);
    }
}