global class BirthdayTasksScheduler implements Schedulable{

    public Void execute(System.SchedulableContext context) {
        Database.executeBatch(new BirthdayTasksBatch());
    }
   
}