/*
 * This class is a utils class for the communication with the DataHub server for the clients details
 */

public class HTTP_ADD_Contact {

	public static final string AUTH_TYPE;
	public static final string CONTENT_TYPE;
	public static final string APPLICATIONJSON;
	public static final string APPLICATION_URL_ENCODED;

	static{
		AUTH_TYPE = 'Authorization';
		CONTENT_TYPE = 'Content-Type';
		APPLICATIONJSON = 'application/json';
		APPLICATION_URL_ENCODED = 'application/x-www-form-urlencoded';
	}


	public static Map<String, Object> sendClients(List<Account> clients, List<ClientAddress__c> addresses, String urlSuffix, User me ) {
		Map<String, Object> payload = getDataHubPayload(clients, addresses, urlSuffix, me);
		String body = JSON.serialize(payload);

		HttpResponse response;
		try {
			response = send(body, urlSuffix);

			if (response.getStatusCode() == 200){
				// handle success
				Map<String,Object> resBody = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
				Map<String,Object> clientData = (Map<String, Object>)resBody.get('data');
				clientData.put('syncData', getSyncData(response.getBody(), body,'synced'));
				Map<String,Object> ret = convertKeysToApiName(clientData);

				return ret;
			}
			else{
				// handle error
				Timer.stop('Data Hub WS');
				Map<String, Object> resObj = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
				Integer code = (Integer)resObj.get('code');
				Map<String,Object> syncData = getSyncData( response.getBody(), body,'failed');

				return new Map<String, Object>{'error' => true,'code'=> code,'message'=> getMessage(resObj) , 'syncData' => syncData};


			}
		}
		catch (Exception ex){
			// handle errors
			System.debug(ex.getMessage());
			System.debug(ex.getStackTraceString());
			return new Map<String, Object>{'error' => true,'code'=> response.getStatusCode(),
											'message'=> response.getBody() ,
											'syncData' => getSyncData( response.getBody(), body,'failed')};
		}
	}


	private static Map<String, Object> convertKeysToApiName(Map<String, Object> clientData) {
		Map<String, Object> ret = new Map<String, Object>();
		Map<String, Data_Hub_Integration_Custom_Field__mdt> dataHubMapping = getDataHubMapping('DataHubField__c', true);

		for (String key : clientData.keySet()){
			Data_Hub_Integration_Custom_Field__mdt mapping = dataHubMapping.get(key);

			if (mapping != null && mapping.SFField__c != null && mapping.SaveFromDataHub__c && clientData.containsKey(mapping.DataHubField__c) && clientData.get(mapping.DataHubField__c) != null){
				Object value = getSFValue(clientData.get(mapping.DataHubField__c), mapping);
				ret.put(mapping.SFField__c, value);
			}
			else{
				ret.put(key, clientData.get(key));
			}
		}
		return ret;
	}

	private static Object getSFValue(Object value, Data_Hub_Integration_Custom_Field__mdt field) {
		if (field != null  && field.Is_Date__c != null && field.Is_Date__c){
			return Date.valueOf(((String)value));
		}
		return value;
	}


	private static Map<String, Object> getDataHubPayload(List<Account> clients, List<ClientAddress__c> addresses, String action, User me) {
		Map<String, Object> retObj = new Map<String, Object>();
		Map<String, Data_Hub_Integration_Custom_Field__mdt> dataHubMapping = getDataHubMapping('SFField__c', false);

		for (Account client : clients){
			for (String fieldName : dataHubMapping.keySet()) {
				Data_Hub_Integration_Custom_Field__mdt mapping = dataHubMapping.get(fieldName);

				if (mapping != null && mapping.DataHubField__c != null
						&&( mapping.SendOnCreate__c && action == 'add' || mapping.SendOnUpdate__c && action == 'update') ){
						retObj.put(mapping.DataHubField__c, client.get(fieldName) != null ? client.get(fieldName) : '' );
				}
			}
			retObj.put('address', getAddressesPayload(addresses, action));
		}
		
		String storeCode = me.POSStoreCode__c;
		if (action == 'add'){
			retObj.put('customerType','Walk-I');
			retObj.putAll( new Map<String, String>{'prefSalesPersonCode' => me.StaffCode__c, 'storeCode' => storeCode, 'originStoreCode' => storeCode, 'homeStoreCode' => storeCode });
		
		}
		else{
			retObj.put('customerId', getCustomerId(clients,me.Zone__c));
			retObj.putAll( new Map<String, String>{ 'storeCode' => storeCode });		
		}

		return retObj;
	}


	private static String getCustomerId(List<Account> clients, String zone) {
		List<ClientSource__c> clientSource = [SELECT SourceCode__c FROM ClientSource__c
												WHERE Client__c IN :clients AND ZoneFormula__c = :zone
													AND SourceCode__c != null LIMIT 1];

		return !clientSource.isEmpty() ? clientSource.get(0).SourceCode__c : '';
	}


	private static Object getAddressesPayload(List<ClientAddress__c> addresses, String action) {
		List<Data_Hub_Integration_Custom_Field__mdt> dataHubIntegrationFields = [SELECT SendOnUpdate__c, SendOnCreate__c, SFField__c, DataHubField__c, SaveFromDataHub__c
																				FROM Data_Hub_Integration_Custom_Field__mdt
																				WHERE Object__c = 'Address' ];
		String myZone = CLT_Utils.getMyUser().Zone__c;
		ClientAddress__c address;

		if(addresses == null){
			return getAddressItem(dataHubIntegrationFields, action, new ClientAddress__c());
		}

		for (ClientAddress__c item : addresses){
			if (item.IsMain__c && item.Zone__c == myZone){
				address = item;
			}
		}

		if (address == null && action == 'add' && !addresses.isEmpty() ) {
			// on create send at least one address record
			address = addresses.get(0);
		}
		if(address == null && action == 'update' ){
			address = new ClientAddress__c();
		}

		return getAddressItem(dataHubIntegrationFields, action, address);
	}


	private static Map<String, Data_Hub_Integration_Custom_Field__mdt> getDataHubMapping(String keyField, Boolean toSave) {
		Map<String, Data_Hub_Integration_Custom_Field__mdt> ret = new Map<String, Data_Hub_Integration_Custom_Field__mdt>();

		for(Data_Hub_Integration_Custom_Field__mdt field : [SELECT DataHubField__c, SFField__c, SaveFromDataHub__c, Object__c, SendOnCreate__c, SendOnUpdate__c, Is_Date__c FROM Data_Hub_Integration_Custom_Field__mdt WHERE Object__c = 'Account']){
			if(!toSave || field.SaveFromDataHub__c){
				ret.put((String)field.get(keyField), field);
			}
		}
		return ret;
	}


	// retrieved the message from the WS response
	public static String getMessage(Map<String, Object> resObj) {
		try {
			if (resObj.get('message') instanceof Map<String, Object>) {
				Map<String, Object> message = (Map<String, Object>)resObj.get('message');
				List<Object> details = (List<Object>)message.get('details');
				Map<String, Object> detailsObj = (Map<String, Object>)details.get(0);
				return (String)detailsObj.get('message');
			}
			else if (resObj.containsKey('error') && resObj.get('error') instanceof Map<String, Object>){
				Map<String, Object> error = (Map<String, Object>)resObj.get('error');
				return (String)error.get('Message');
			}
			return (String)resObj.get('message');

		}
		catch (Exception e){
			System.debug(e.getMessage());
		}
		return 'error retrieve message from response ';
	}



	public static Map<String, Object> getSyncData(String response, String request, String status) {
		return new Map<String, Object>{
				'Last_Sync_Date__pc' => System.now(),
				'Sync_Request__pc' => request,
				'Sync_Response__pc' => response,
				'Synchronization_Status__pc' => status

			};
	}

	public static Map<String,Object> writeOptin(List<Account> clients, String storeCode, String customerId) {
		Map<String,Object> ret = new Map<String, Object>();
		Map<String, Object> body = getWriteOptinPayload(clients.get(0), storeCode, customerId);

		ret.put('Sync_Optin_Request__pc', JSON.serialize(body));

		try{
			HttpResponse response = send(JSON.serialize(body), 'optin/write');
			ret.put('SyncOptinResponse__pc' ,response.getBody());
		}
		catch (Exception ex){
			System.debug(ex.getMessage());
			ret.put('SyncOptinResponse__c' ,ex.getMessage());
		}
		return ret;
	}


	public static String sendSignature(String customerId, String storeCode, String signature) {
		Map<String,Object> body = new Map<String, Object>();

		body.put('customerId', customerId);
		body.put('storeCode', storeCode);
		body.put('dtOptins', System.now());
		body.put('pSign', signature);

		HttpResponse response;
		try{
			response = send(JSON.serialize(body), 'optin/psign/write');

			if (response.getStatusCode() == 200){
				return response.getBody();
			}
			else{
				return response.getBody();
			}
		}
		catch (Exception ex){
			System.debug(ex.getMessage());
			return ex.getMessage() + '\n' + response.getBody();
		}
		return null;
	}


	public static HttpResponse send(String body, String urlSuffix) {
		APIManagerConstants__c integData = HTTP_Integration_Utils.getAccessToken();
		String accessToken = integData.AccessToken__c;
		String authorizationHeader = 'Bearer ' + accessToken;
		String url = integData.URL__c + 'datahubv1bis/customers/retail/' +	urlSuffix;//
		HttpResponse response;

		HTTP_UTILS httpUtils = new HTTP_UTILS();

		if (urlSuffix == 'add') {
			httpUtils.post(url);
		} else {
			httpUtils.put(url);
		}

		CLT_Utils.log(urlSuffix + ' request body', body);
		Timer.start('Data Hub WS');
		response = httpUtils.header(CONTENT_TYPE, APPLICATIONJSON)
				.header('Ocp-Apim-Subscription-Key', integData.ApimSubscriptionKey__c)
				.header('Ocp-Apim-Trace', integData.ApimTrace__c)
				.header(AUTH_TYPE, authorizationHeader)
				.timeout(60000)
				.body(body)
				.call()
				.getResponse();
		Timer.stop('Data Hub WS');
		CLT_Utils.log(urlSuffix + ' response body', response.getBody());

		return response;
	}


	public static Map<String, Object> getAddressItem(List<Data_Hub_Integration_Custom_Field__mdt> dataHubIntegrationFields, String action, ClientAddress__c address) {
		if (address == null){
			return  new Map<String, Object>();
		}
		Map<String, Object> addressItem = new Map<String, Object>();
		for (Data_Hub_Integration_Custom_Field__mdt field : dataHubIntegrationFields) {
			if ((field.SendOnCreate__c && action == 'add' || field.SendOnUpdate__c && action == 'update')) {
				addressItem.put(field.DataHubField__c, address.get(field.SFField__c) != null ? address.get(field.SFField__c) : '');
			}
		}
		return addressItem;
	}

	public static Map<String, Object> getWriteOptinPayload(Account client, String storeCode, String customerId) {
		Map<String, Object> body = new Map<String, Object>();

		body.put('storeCode', storeCode);
		body.put('bContactMailbox', client.OptinMailing__pc ? 1 : 0);
		body.put('bContactEmail', client.OptinEmailing__pc ? 1 : 0);
		body.put('bContactTel', client.OptinCalling__pc ? 1 : 0);
		body.put('bValidPrivPolicy', 1);
		body.put('bContactSocNetw', client.OptinSocialNetworking__pc ? 1 : 0);
		body.put('bContactInstMsg', client.OptinInstantMessaging__pc ? 1 : 0);
		body.put('dtOptins', System.now());
		body.put('customerId', customerId);
		return body;
	}
}