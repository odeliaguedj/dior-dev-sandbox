//Tamar s
@isTest
public with sharing class HTTP_UTILS_TEST {
    private static final String FAKE_RESPONSE_JSON = '... add JSON here ...';

    private class Mock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {

            // You could assert the request content here
           String method = 'del';
           String url = 'https://docs.google.com/';


           HTTPResponse res = new HTTPResponse();
            res.setBody(FAKE_RESPONSE_JSON);
            res.setStatus('200');
            res.setStatusCode(200);
           // res.setMethod(method);
           // res.setEndpoint(url);
            return res;
        }
    }

    @isTest
    public static void HTTP_UTILS_TEST() {

          Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Mock());
          HTTP_UTILS http = new HTTP_UTILS();
          HttpResponse response = new HttpResponse();
          String body = 'Test';
          String method = 'del';
          String url = 'https://docs.google.com/';
          String key= 'id';
          String value = 'Test';
          Map<String,String> allHeaders = new Map<String,String>();
          allHeaders.put('Id', '1234');
          Integer timeout = 20;
            http.body(body);
            http.get(url);
            http.put(url);
            http.post(url);
            http.patch(url);
            http.del(url);
            http.header(key,value);
            http.addHeaders(allHeaders);
            http.timeout(timeout);
            http.call();
        System.debug(http.getResponse());
            http.getResponse();
            http.getHeaders();
            
           //  http.status();
           //  http.statusCode();
           //  http.set_uri_and_method(method,url);
          //http.getParameters();


          Test.stopTest();
    }
}