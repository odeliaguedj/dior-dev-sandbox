@isTest
public class EventHandlerTest {
    @isTest 
    private static void testInsert() {
    
        Campaign cmp = new Campaign(name='Campaign_', TECH_LaunchAddMembersBatch__c = true, TECH_ContactIds__c = '!');
        insert cmp; 
        
        Contact contact = new Contact(LastName = 'LastName');
        insert contact;
        
        CampaignMember campaignMember = new CampaignMember(CampaignId= cmp.Id, CurrencyIsoCode='CAD', ContactId =  contact.Id, Status = 'Cannot Be Reached', AppointmentDateTime__c = System.now());
        insert campaignMember; 
        
        Event appointment = new Event (Type = 'OTO', Campaign__c = cmp.Id, CampaignMemberId__c = campaignMember.UniqueUpsertId__c, StartDateTime = DateTime.now(),  EndDateTime = DateTime.now());
        insert appointment;

    }    
    
    @isTest 
    private static void testDelete() {
    
        Campaign cmp = new Campaign(name='Campaign_', TECH_LaunchAddMembersBatch__c = true, TECH_ContactIds__c = '!');
        insert cmp; 
        
        Contact contact = new Contact(LastName = 'LastName');
        insert contact;
        
        CampaignMember campaignMember = new CampaignMember(CampaignId= cmp.Id, CurrencyIsoCode='CAD',  ContactId =  contact.Id, Status = 'Cannot Be Reached', AppointmentDateTime__c = System.now());
        insert campaignMember; 
        
        Event appointment = new Event (Type = 'OTO', Campaign__c = cmp.Id, StartDateTime = DateTime.now(), EndDateTime = DateTime.now());
        insert appointment;
        
        delete appointment;

    }    

}