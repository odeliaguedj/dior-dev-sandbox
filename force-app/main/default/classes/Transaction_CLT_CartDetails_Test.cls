//DiorTransaction

@IsTest
private class Transaction_CLT_CartDetails_Test {
@isTest static void test_method() {
   
    Cart__c cart=new Cart__c(Client__c='0010Q00000TDobbQAD',Store__c='a0K1t000000oVwTEAU',Status__c='ON_HOLD*',ActiveOwnerId__c='0050Q000002TwrAQAS');
    insert cart; 
    CartItem__c cartItem=new CartItem__c(ReferenceStyle__c='test',ColorCode__c='test',SizeCode__c='test',ColorGroupCode__c='ROUGE',SA__c='0050Q000002TwrAQAS',Amount__c=100,Quantity__c=1, SkuCode__c='test',ProductName__c='test',Cart__c=cart.Id);
    insert cartItem;
    CartItem__c cartItem2=new CartItem__c(ReferenceStyle__c='test2',ColorCode__c='test2',SizeCode__c='test2',ColorGroupCode__c='ROSE',SA__c='0050Q000002Twr0QAC',Amount__c=50,Quantity__c=10, SkuCode__c='test2',ProductName__c='test2',Cart__c=cart.Id);
    insert cartItem2;
    Transaction_CLT_CartDetails cartDetails = new  Transaction_CLT_CartDetails();
    
    Test.startTest();
    Map<String, Object> getResult=(Map<String, Object>)cartDetails.retrieveGet(new Map<String, String>{'cartId' => cart.Id});
    
    system.assertEquals(getResult.get('ClientId'),'0010Q00000TDobbQAD');
    system.assertEquals(getResult.get('StoreId'),'a0K1t000000oVwTEAU');
    system.assertEquals(getResult.get('ActiveOwnerId'),'0050Q000002TwrAQAS');
    List<Map<String, Object>> cartItemList= (List<Map<String, Object>>)getResult.get('CartItemList');
    system.assertEquals(cartItemList[0].get('Amount'),100);
    system.assertEquals(cartItemList[1].get('Amount'),50);
    system.assertEquals(cartItemList[0].get('ReferenceStyle'),'test');
    system.assertEquals(cartItemList[1].get('ReferenceStyle'),'test2');
    
    Object deleteResult=cartDetails.retrieveDelete(new Map<String, String>());
    system.assertEquals(deleteResult,null);
    Object postResult=cartDetails.retrievePost(new Map<String, String>(),new Map<String,Object>());
    system.assertEquals(postResult,null);
	Test.stopTest();
    }  
    
    
}