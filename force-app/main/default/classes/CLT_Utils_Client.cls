/*
This class is a utility class for performing common functions on client
*/
public class CLT_Utils_Client {

	 /**
  * Static method to check if client is exist in the DataHub for the current zone.
  * @param clients - clients to check .
  * @param zone .
  * @return true if the client is already created in DataHub for this zone .
  */
	public static Boolean isNew(List<Account> clients, String zone) {

		List<ClientSource__c> clientSources = [SELECT Id FROM ClientSource__c WHERE Client__c IN : clients AND ZoneFormula__c = :zone];
		return clientSources.isEmpty();
	}

	/**
 * Static method to send the client to the DataHub server.
 * @param clients - clients to send.
 * @param address - list of  ClientAddress__c related to the client.
 * @param me - the context user .
 * @param items - list of objects deserilized from the JSON in the request.
 * @param ret - return object .
 * @return Map<String, Object> contained details about the response from the DataHub server.
 */
	public static Map<String, Object> sendClientsToDataHub(List<Account> clients, List<ClientAddress__c> address, User me, List<Object> items, Map<String, Object> ret) {
		Map<String, Object> clientData;
		String storeCode = me.POSStoreCode__c ;

        try{
        
			if (isNew(clients, me.Zone__c)) {
				clientData = dataHubCreateClients(clients, address, me);
			}
			else {
				clientData = dataHubUpdateClients(clients, address, me);
			}
			addSyncData(clients, clientData);
			Map<String, Object> client = !items.isEmpty() ? (Map<String, Object>)items.get(0) : new Map<String, Object>();
			String signature = client.containsKey('signature') ? (String)client.get('signature') : '';

			if (clientData.containsKey('error')) {
				ret.put('error', clientData);
			}
			else {
				if (!String.isEmpty(signature)){
					String customerId = String.valueOf(clientData.get('customerId'));

					Map<String, Object> optinData = dataHubOptinClients(clients, storeCode, customerId);
					clientData.putAll(optinData);

					String res = dataHubAddSignature(items, storeCode, customerId);
					clientData.put('SyncSignatureResponse__pc', res);

				}

			}
			return clientData;

		}
        catch(Exception ex){
           CLT_Utils.log('error DataHub server', ex.getMessage());
			System.debug(ex.getStackTraceString());
			clientData = new Map<String, Object>{'error' => 'error DataHub server'};
        }
         return clientData;
	}

	/**
 * Static method to call the add client endpoint in the DataHub server.
 * @param clients - clients to send.
 * @param address - list of  ClientAddress__c related to the client.
 * @param me - the context user .
 * @return Map<String, Object> contained details about the response from the DataHub server.
 */
	private static Map<String,Object> dataHubCreateClients(List<Account> clients, List<ClientAddress__c> addresses, User me) {
		return HTTP_ADD_Contact.sendClients(clients, addresses, 'add', me);
	}

	/**
* Static method to call the update client endpoint in the DataHub server.
* @param clients - clients to send.
* @param address - list of  ClientAddress__c related to the client.
* @param me - the context user .
* @return Map<String, Object> contained details about the response from the DataHub server.
*/
	private static Map<String,Object> dataHubUpdateClients(List<Account> clients, List<ClientAddress__c> addresses, User me) {
		return HTTP_ADD_Contact.sendClients(clients, addresses, 'update', me);
	}

	/**
 * Static method to call the writeOptin endpoint in the DataHub server.
 * @param clients - clients to send.
 * @param storeCode .
 * @param customerId - the customerId of the client supplied by the DataHub server .
 * @return Map<String, Object> contained details about the response from the DataHub server.
 */
	private static Map<String, Object> dataHubOptinClients(List<Account> clients, String storeCode, String customerId) {

		return HTTP_ADD_Contact.writeOptin(clients, storeCode, customerId);
	}


	private static void addSyncData(List<Account> clients, Map<String, Object> clientData) {
		Map<String, Object> syncData = (Map<String, Object>)clientData.get('syncData');
		if (syncData == null) return ;
		for (Account acc : clients){
			for (String key : syncData.keySet()){
				acc.put(key, syncData.get(key));
			}
		}
	}

	/**
* Static method to call the addSignature endpoint in the DataHub server.
* @param clients - list of objects deserilized from the JSON in the request,
	 * contained also a "signature" property - the signature in a base64 format .
* @param storeCode .
* @param customerId - the customerId of the client supplied by the DataHub server .
* @return the response body from the DataHub server. if an error occurred while sending, it also concatenates the error message .
*/
	private static String dataHubAddSignature(List<Object> clients, String storeCode, String customerId) {
		if (clients == null) return null;

		Map<String, Object> client = (Map<String, Object>)clients.get(0);
		String signature = client != null ? (String)client.get('signature') : '';
		if(!String.isEmpty(signature))
			return HTTP_ADD_Contact.sendSignature(customerId, storeCode, signature);
		else
			return '';
	}

	private static void mergeClient(List<Account> clients, Map<String, Object> clientData) {
		Set<String> accountFields = Schema.Account.sObjectType.getDescribe().fields.getMap().keySet();

		for (Account client : clients){
			for (String key : clientData.keySet()){
				if (accountFields.contains(key.toLowerCase())){
					Object value = clientData.get(key);
					client.put(key, value);
				}
			}
		}
	}

	//insert/update the clients records
	public static void saveRecords(List<Object> items, User me, List<Account> clients, List<ClientAddress__c> address, List<FamilyMember__c> members, Map<String, Object> clientData) {
		mergeClient(clients, clientData);

		Boolean isNewClientInSF = clients.get(0).Id == null;

		upsert clients;

		if (CLT_Utils_Client.isNew(clients, me.Zone__c))
			createClientSource(me, clients, clientData);

		if (isNewClientInSF){
			Map<String, Object> client = (Map<String, Object>)items.get(0);
			String signature = client!= null ? (String)client.get('signature') : '';

			CLT_Utils_Client.saveSignature(signature, clients.get(0).Id);
		}


		saveAddresses(address, clients.get(0).Id);
		saveMembers(members, clients.get(0).Id);
	}


	/**
* Static method to create ClientSource__c and save it in Salesforce.
* @param me - the context user .
* @param clients - the clients who the ClientSource__c should be linked .
* @param clientData - contained details about the response from the DataHub server .
* @return void .
*/
	public static void createClientSource(User me, List<Account> clients, Map<String, Object> clientData) {

		if (clientData.containsKey('error')) {// sync failed
			return;
		}

		List<ClientSource__c> clientSourcesToInsert = new List<ClientSource__c>();

		clientSourcesToInsert.add(new ClientSource__c(Client__c = clients.get(0).Id,
																Zone__c = me.Zone__c,
																Source__c = 'XPERT ' + me.Zone__c.toUpperCase(),
																SourceCode__c = (String)clientData.get('customerId')));
		// in europe - add also a 'CEGID' client source
		if (me.Zone__c == 'EU'){
			ClientSource__c source = clientSourcesToInsert.get(0).clone();
			source.Source__c = 'CEGID';
			clientSourcesToInsert.add(source);
		}
		insert clientSourcesToInsert;

	}



	/**
 * Static method to save the signature of the client as a file in Salesforce.
 * @param signature - the signature to save .
 * @param clientId - Id of the client to who the signature should be linked .
 * @return void .
 */
	public static void saveSignature(String signature, String clientId) {

		if (String.isBlank(signature) ) return ;

		ContentVersion contentVersion = new ContentVersion(Title = 'Signature',
															PathOnClient = 'Signature.jpg',
															VersionData = EncodingUtil.base64Decode(signature));
		insert contentVersion;

		List<ContentVersion> ll = [SELECT Id,  ContentDocumentId FROM ContentVersion  WHERE Id = :contentVersion.Id];
		System.debug(ll);

		ContentDocumentLink documentLink = new ContentDocumentLink(ContentDocumentId = ll.get(0).ContentDocumentId,
																	LinkedEntityId = clientId ,
																	ShareType = 'v');
		insert documentLink;
	}


	/**
* Static method to save the FamilyMember related to the client in Salesforce.
* @param members - list of FamilyMember__c to save .
* @param clientId - Id of the client to who the signature should be linked .
* @return void .
*/
	private static void saveMembers(List<FamilyMember__c> members, Id clientId) {
		if (!members.isEmpty()){
			//The records that were not sent to the web service are deleted
			List<FamilyMember__c> oldMembers = [SELECT Id FROM FamilyMember__c WHERE Client__c = :clientId AND Id NOT IN :members];
			delete oldMembers;

			for (FamilyMember__c item : members) {
				item.Client__c = clientId;
			}
		}
		upsert members;
	}



	/**
* Static method to save the FamilyMember related to the client in Salesforce.
* @param members - list of FamilyMember__c to save .
* @param clientId - Id of the client to who the signature should be linked .
* @return void .
*/
	public static void saveAddresses(List<ClientAddress__c> address, Id clientId) {
		if (!address.isEmpty()){
			//The records that were not sent to the web service are deleted
			List<ClientAddress__c> oldAddress = [SELECT Id FROM ClientAddress__c WHERE Client__c = :clientId AND Id NOT IN :address];
			delete oldAddress;

			for (ClientAddress__c item : address) {
				item.Client__c = clientId;
			}
			upsert address;
		}
	}



	@future(callout=true)
	public static void sendClientsToDataHubFuture(String clientId){
		User me = CLT_Utils.getMyUser();

		List<String> fields = new List<String>();
		for (Data_Hub_Integration_Custom_Field__mdt item : [SELECT SFField__c
															FROM Data_Hub_Integration_Custom_Field__mdt
															WHERE Object__c = 'Account']){
			fields.add(item.SFField__c);
		}
		String queryString = 'SELECT ' + String.join(fields, ', ') + ' FROM Account WHERE Id = : clientId';
		List<Account> clientsToSync = Database.query(queryString);
		Map<String, Object> clientData = sendClientsToDataHub(clientsToSync, clientsToSync.get(0).ClientAddresses__r, me, new List<Object>(),	new Map<String, Object>());

		saveRecords(new List<Object>(), me, clientsToSync, new List<ClientAddress__c>(), new List<FamilyMember__c>(), clientData);
	}
}