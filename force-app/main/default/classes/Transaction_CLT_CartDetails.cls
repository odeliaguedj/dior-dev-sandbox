//Dior Transaction

global with sharing class Transaction_CLT_CartDetails implements CLT_RetailInterface{
    	
    
     public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
     
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }   
    
    public Object retrieveGet(Map<String, String> params){
        String objectName = 'CartItem__c';
        Set<String> itemIds = new Set<String>();
        String cartId = params.get('cartId');
        Map<String, Object> result = new Map<String, Object>();
        Cart__c CartDetail=[SELECT Client__c,Amount__c, Store__c, Status__c, ActiveOwnerId__c, ItemCount__c FROM Cart__c WHERE Id=:cartId];
        result.put('ClientId',CartDetail.Client__c);
        result.put('Amount',CartDetail.Amount__c);
        result.put('StoreId',CartDetail.Store__c);
        result.put('Status',CartDetail.Status__c);
        result.put('ActiveOwnerId',CartDetail.ActiveOwnerId__c);
        result.put('ItemCount',CartDetail.ItemCount__c);
        
        List<CartItem__c> CartItemList=[SELECT ReferenceStyle__c, ColorCode__c, SizeCode__c, ColorGroupCode__c, SA__c,Amount__c,
                                        	   Quantity__c,SkuCode__c,ProductName__c,Image_URL_Path__c
                                        FROM CartItem__c 
                                        WHERE Cart__c=:cartId
                                       ];
        for(CartItem__c item: CartItemList) {
			itemIds.add(item.Id);
		}
        if(itemIds.size() > 0) {
        
        	result.put('CartItemList',CLT_Utils_Mapping.getStandardWrapperList('CartItem__c', 'CartItem',
							' FROM ' + objectName + ' WHERE Id IN ' + CLT_Utils.convertToString(itemIds) + ' ORDER BY CreatedDate DESC'));
        }
        
        return result;
    }

}