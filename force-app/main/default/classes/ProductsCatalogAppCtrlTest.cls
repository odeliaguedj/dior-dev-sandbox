@isTest
private class ProductsCatalogAppCtrlTest {
	@isTest static void Test()
    {

        //getProductCatalogCountry
        String s= 'test'; 
        
        List<Product_Catalog_Country_Index__mdt> productList=ProductsCatalogAppCtrl.getProductCatalogCountry(s);
        system.assertEquals(productList[0].DeveloperName,'United_Arab_Emirates_Index');
        system.assertEquals(productList[0].MasterLabel,'United Arab Emirates Index');
        system.assertEquals(productList[0].Country__c,'ARE');
        system.assertEquals(productList[0].Index__c,'PRODUCTS_ARE');
        system.assertEquals(productList[0].Language__c,'en_UK');
        
        ContentDocument cd=new ContentDocument(Id='0691t00000Ae99R');
        ContentDocument cd2=new ContentDocument(Id='0691t00000Ae99Q');
        
        ContentVersion c=new ContentVersion(VersionData=Blob.valueOf('OMNI_1_WOM'),PathOnClient='0691t00000Ae99R',ContentDocumentId=cd.Id);
        insert c; 
        ContentVersion c2=new ContentVersion(VersionData=Blob.valueOf('OMNI_1_MEN'),PathOnClient='0691t00000Ae99Q',ContentDocumentId=cd2.Id);
        insert c2;
       
        ProductCatalog__c newProductCatalog=new ProductCatalog__c(Name='testName',DivisionsImages__c='{"OMNI_1_WOM":"'+c.ContentDocumentId+'", "OMNI_1_MEN":"'+c2.ContentDocumentId+'"}'); 
        insert newProductCatalog;
        
        //getProductCatalogData
        ProductCatalog__c productCatalog= ProductsCatalogAppCtrl.getProductCatalogData(s);
        system.assertEquals(newProductCatalog.Id,productCatalog.Id); 
		        
        
        //getDvisionsImages
        Map<String, String> mapProduct=ProductsCatalogAppCtrl.getDvisionsImages(s);
        system.assertEquals(mapProduct.containsKey('OMNI_1_MEN'),true);
        system.assertEquals(mapProduct.containsKey('OMNI_1_WOM'),true);
         
         
        //getAccessToken
        APIManagerConstants__c managerConstant=new APIManagerConstants__c(ClientSecret__c='TestSecret',ClientID__c='IdTest',Resource__c ='TestRessource',AccessToken__c='AccessTest');
        insert managerConstant;
        String s2='test';
        String token=ProductsCatalogAppCtrl.getAccessToken(s2);
        system.debug('token '+token);
        system.assertEquals(token,'AccessTest');
        
        
        //getImage
        String json='{"accessToken":"OMNI_1_WOM", "productCode":"0691t00000Ae99R"}';
        Test.setMock(HttpCalloutMock.class, new ImageHttpCalloutMock());

        Test.startTest();
        String image = ProductsCatalogAppCtrl.getImage(json);
        String imageDecoded=EncodingUtil.base64Decode(image).tostring();
        system.assertEquals(imageDecoded,'success');
        Test.stopTest(); 
        
        //getStore
        Store__c newStore=new Store__c(XpertStoreCode__c='5739857', Name='StoreName', StoreCode__c='5739857', PriceList__c='PriceListTest');
        insert newStore;
        Map<String, Store__c> mapStore=ProductsCatalogAppCtrl.getStore('["5739857", "35353"]');
        system.assertEquals(mapStore.get('5739857').Name,'StoreName');
        system.assertEquals(mapStore.get('5739857').XpertStoreCode__c,'5739857');
        system.assertEquals(mapStore.get('5739857').PriceList__c,'PriceListTest');
    }
}