/*
This class is a utility class for performing common functions
like UserInfo and run queries
*/
public with sharing class CLT_Utils {


    public static void log(string title, object o) {
        system.debug(LoggingLevel.INFO,'\n************** ' + title + ' *******************\n\n' + o + '\n\n\n*********************** END ' + title + '****************\n\n');
    }


    //get basic info about the current user
    static public User getMyUser() {   
        return [SELECT BaseStoreCode__c, Zone__c , IsManager__c, POSStoreCode__c, StaffCode__c, DefaultCurrencyIsoCode  FROM User WHERE Id = :UserInfo.getUserId()];
    }
	
    static public String getUserStoreId() {
        String storeCode = getMyUser().BaseStoreCode__c;
        List<Store__c> stores = [SELECT Id FROM Store__c WHERE StoreCode__c = :storeCode];

        return stores.size() > 0 ? stores[0].Id : null;
    }

    static public String getUserStoreCode() {
        return getMyUser().BaseStoreCode__c;
    }
	
    //Run a general query
    static public Object getObjectList(String objectName, String fields, String whereString){
        String query = 'SELECT ' + fields + ' From '+ objectName + whereString;
        Object objectList = Database.query(query);
        return objectList;
    }
	
    //get list of any field from list object
    public static set<String> getIdList(List<Sobject> items, String fieldName){
       Set<String> s = new Set<String>();
   
       for(Sobject ob : items){
            s.add((string)ob.get(fieldName));
       }
       return s;         
    }




    public static String getContactId(string accId){
        Account acc =  [SELECT Id, PersonContactId from Account Where Id =: accId limit 1];
        return acc.PersonContactId;
    }

    public static String convertToString(List<String> stringList){
        if(stringList.size() > 0){
            return '(\''+ String.join(stringList, '\',\'') + '\')';
        } else {
            return '()';
        }
          
    }  

    public static String convertToString(Set<String> stringSet){
        if(stringSet.size() > 0){
            List<String> stringList = new List<String>();
            stringList.addAll(stringSet);
            return '(\''+ String.join(stringList, '\',\'') + '\')';
        } else {
            return '()';
        }
          
    }  

    static public Map<String, map<string, object>> getDocuments(list<map<string, object>> items){
        
        set<String> parentIds = new set<String>();
        for(map<string, object> item : items){
            String itemId = String.valueOf(item.get('id'));
            parentIds.add(itemId);
        }

        return getDocuments(parentIds);      
    } 

    static public Map<String, map<string, object>> getDocuments(set<String> parentIds){
        Map<String, map<string, object>> documents = new Map<String, map<string, object>>();

        if(parentIds.size() == 0){
            return null;
        }
        
        string soqlCondition = convertToString(parentIds);
        
        list<map<string, object>> docs =  CLT_Utils_Mapping.getStandardWrapperList('ContentDocumentLink', 'Document', 'FROM ContentDocumentLink WHERE LinkedEntityId IN ' + soqlCondition); 
        
        for(map<string, object> doc : docs){
            string parentId = String.valueOf(doc.get('parentId'));
            documents.put(parentId, doc);  
        }
        return documents;        
    } 
    
    	public static String getLabel(String obj, String field, String value){
            for(PicklistEntry str : Schema.getGlobalDescribe().get(obj).getDescribe().fields.getMap().get(field).getDescribe().getPicklistValues()){
                if (str.getValue() == value){
                    return str.getLabel();
                }
            }
		return null;
	}
    
     public static String checkRunningUser(Map<String,Object> body){
        if(body.get('runningUser')!=null){
            return body.get('runningUser').tostring();
        }
        else{
            return UserInfo.getUserId();
        }
    }

}