/*
* This class manages the HTTPS requests of the Clients web service
*/

public with sharing class CLT_Clients implements clt_retailInterface {
    //on Delete request
    public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    //on Post request
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){

        return upsertItems(body);
    }

 	// upsert items
    public Map<String, Object> upsertItems(Map<String,Object> body) {
        Map<String, Object> ret = new Map<String, Object>();
        String objectName =  (String) body.get('objectName');
        List<Object> items = (List<Object>) body.get('items');

        List<Object> itemsClientOnly = retrieveClients(items);
        String clientCountry = getClientCountry(itemsClientOnly);
        User me = CLT_Utils.getMyUser();

        Map<String, Object> client = (Map<String, Object>)itemsClientOnly.get(0);
        List<Account> clients = getAccounts(objectName, itemsClientOnly, me, clientCountry);
        List<ClientAddress__c> address = (List<ClientAddress__c>)getSobjectsList((List<Object>)client.get('addresses'), 'ClientAddress__c', 'ClientAddress');
        List<FamilyMember__c> members = (List<FamilyMember__c>)getSobjectsList((List<Object>)client.get('relatives'), 'FamilyMember__c', 'FamilyMember');

        Map<String, Object> clientData = CLT_Utils_Client.sendClientsToDataHub(clients, address, me, items, ret);

        CLT_Utils_Client.saveRecords(items, me, clients, address, members, clientData);


        ret.put('client', CLT_Utils_ClientDetails.getClient(clients.get(0).Id));
        ret.put('clientFields', CLT_Utils_ClientDetails.getClientFields(clients.get(0).Id));

        return ret;

        }

    //get Clients
    private List<Object> retrieveClients(List<Object> items) {
        List<Object> ret = new List<Object>();
        for (Object item : items){
            ret.add(((Map<String, Object>)item).get('client'));
        }
        return ret;
    }


    private List<Object> getSobjectsList(List<Object> items, String objectName, String objectTitle){

        if (items == null){
            return new List<Object>();
        }

        List<Map<String, Object>> mapObjects = CLT_Utils_Mapping.getMapObjectsByWrapperItems(objectName, objectTitle, items);

        List<Object> sobjectList = (List<Object>) JSON.deserialize(JSON.serialize(mapObjects), Type.forName('List<' + objectName + '>'));

        return sobjectList;

    }


    public Object retrieveGet(Map<String, String> params){

        return new Map<String, Object> {
            'clients' => getClients(params)
        };
    }
    // get clients that match the params parameter value
    List<Map<String, Object>> getClients(Map<String, String> params){

       /* Boolean isSOQL = params.containsKey('Id') && params.get('Id').length() > 3;
        if (!isSOQL){
            return (List<Map<String, Object>>) new CLT_Clients2().getClients(params);
        }

        List<String> conditions = new List<String>();

        Map<String, List<String>> fieldsNamesMap = new Map<String, List<String>>();
        fieldsNamesMap.put('FirstName', new List<String>{'FirstName', 'FirstNameEU__pc', 'FirstNameUS__pc', 'FirstNameJP__pc', 'FirstNameAsia__pc'} );
        fieldsNamesMap.put('LastName', new List<String>{'LastName', 'LastNameEU__pc', 'LastNameUS__pc', 'LastNameJP__pc', 'LastNameAsia__pc'} );
        fieldsNamesMap.put('Phone', new List<String>{'Phone', 'Phone2__pc', 'Phone3__pc'} );
        fieldsNamesMap.put('PersonEmail', new List<String>{'PersonEmail', 'Email2__pc', 'Email3__pc'} );
        fieldsNamesMap.put('Id', new List<String>{'UCRInternalId__c'} );
        fieldsNamesMap.put('Nationality__pc', new List<String>{'Nationality__pc'} );
        fieldsNamesMap.put('PassportNumber', new List<String>{'PassportNumber__pc'} );
        fieldsNamesMap.put('BillingCountry', new List<String>{'PersonMailingCountry'} );
        for(String field: params.keySet()) {
			
            String value = params.get(field).trim();
            if(value == '')
                continue;
            String condition;
            List<String> subCondition = new List<String>();
            for (String fieldName : fieldsNamesMap.get(field.trim())){
                switch on field.toLowerCase() {
                    when 'firstname', 'lastname', 'id' {
                        condition = fieldName + ' LIKE \'' + value + '%\'';
                    }
                    when else {
                        condition = fieldName + ' = \'' + value + '\'';
                    }
                }
                subCondition.add(condition);
            }
            condition = '( ' + String.join(subCondition, ' OR ') +' ) ';
            if(condition != NULL)
                conditions.add(condition);
        }
        String query = CLT_Utils_Query.getClientsQuery(false);
        if(!conditions.isEmpty()) {
            query += ' AND ' + String.join(conditions, ' AND ');
        }
        query +=' LIMIT 20';
        system.debug(query);
        system.debug(system.now());
//        List<Account> accounts = Database.query(query);*/
        List<Account> accounts = searchClients(params);
        system.debug(system.now());
        return CLT_Utils_Mapping.getStandardWrapperListFromObjectList('Account', 'Client', accounts);
    }

    private List<Account> searchClients(Map<String,String> params) {

        Boolean isSOQL = params.containsKey('Id') && params.get('Id').length() > 3;
        if (!isSOQL){
            return (List<Account>) new CLT_Clients2().getClients(params);
        }

        String query = CLT_Utils_Query.getClientsQuery(false);
        List<String> conditionsList = getConditions(params);

        if(!conditionsList.isEmpty()) {
            query += ' AND CurrentSegment__pc != null  AND CurrentSegment__pc != \'NO_SEGMENT\'  AND ' + String.join(conditionsList, ' AND ');
        }
        query +=' LIMIT 20';
        system.debug(query);
        system.debug(system.now());
        //        List<Account> accounts = Database.query(query);
        return Database.query(query);

    }

    private List<Account> getAccounts(String objectName, List<Object> items, User me, String clientCountry) {

        List<String> objectTitles = new String[]{'ClientFields', 'Client', 'Client_Zone_' + me.Zone__c};
        
        List<Map<String, Object>> mapClients = CLT_Utils_Mapping.getMapObjectsByWrapperItems(objectName, objectTitles, items);
        List<Account> clients = (List<Account>) JSON.deserialize(JSON.serialize(mapClients), List<Account>.class);

        Id myStoreId = Utils.getStoreId(me.BaseStoreCode__c);

        for (Account account : clients) {
            if (account.Id == null){
                account.PreferredSA1__pc = me.Id;
                account.PreferredStore1__pc = myStoreId;
                //account.DiorJourneys__pc = 'Walk In';
                account.CurrentSegment__pc = 'Prospect';
                account.PersonMailingCountry = clientCountry;
            }
            account.Optin__pc = isOptin(account);
        }
        
        //set Field Updates before sending to DataHub  - Birthday field must be calculated
        AccountHandler.updateClientFields(clients,null,false);

        return clients;
    }
    
    private Boolean isOptin(Account account){
        return account.OptinCalling__pc || account.OptinMailing__pc || account.OptinSocialNetworking__pc || account.OptinInstantMessaging__pc || account.OptinEmailing__pc;
    }

    private String getClientCountry(List<Object> itemsClientOnly){
        try{
            Map<String,Object> client = ( Map<String,Object>) itemsClientOnly.get(0);
            List<Object> addresses = (List<Object>) client.get('addresses');
            Map<String,Object> address = (Map<String, Object>) addresses.get(0);
            String country = CLT_Utils.getLabel('ClientAddress__c', 'Country__c', (String) address.get('country'));
            return country;
        }
        catch(Exception ex){
            CLT_Utils.log('error get client country', ex.getMessage());
        }
        return '';
       
    }


    private String getSingleCondition(String fieldName, String paramKey, String paramValue) {
        switch on paramKey.toLowerCase() {
            when 'firstname', 'lastname', 'id' {
                return fieldName + ' LIKE \'' + paramValue + '%\'';
            }
            when else {
                return fieldName + ' = \'' + paramValue + '\'';
            }
        }
    }


    public static Map<String, List<String>> getFieldNamesMap() {
        Map<String, List<String>> fieldsNamesMap = new Map<String, List<String>>();
        fieldsNamesMap.put('FirstName', new List<String>{'FirstName', 'FirstNameEU__pc', 'FirstNameUS__pc', 'FirstNameJP__pc', 'FirstNameAsia__pc'});
        fieldsNamesMap.put('LastName', new List<String>{'LastName', 'LastNameEU__pc', 'LastNameUS__pc', 'LastNameJP__pc', 'LastNameAsia__pc'});
        fieldsNamesMap.put('Phone', new List<String>{'Phone', 'Phone2__pc', 'Phone3__pc'});
        fieldsNamesMap.put('PersonEmail', new List<String>{'PersonEmail', 'Email2__pc', 'Email3__pc'});
        fieldsNamesMap.put('Id', new List<String>{'UCRInternalId__c'});
        fieldsNamesMap.put('Nationality__pc', new List<String>{'Nationality__pc'});
        fieldsNamesMap.put('PassportNumber', new List<String>{'PassportNumber__pc'});
        fieldsNamesMap.put('BillingCountry', new List<String>{'PersonMailingCountry'});
        return fieldsNamesMap;
    }

    public List<String> getConditions(Map<String, String> params) {
        Map<String, List<String>> fieldsNamesMap = getFieldNamesMap();
        List<String> conditionsList = new List<String>();

        for (String field : params.keySet()) {
            String value = params.get(field).trim();
            if (value == '')
                continue;
            String condition;
            List<String> subCondition = new List<String>();
            for (String fieldName : fieldsNamesMap.get(field.trim())) {
                //                condition = getSingleCondition(fieldName, field, value);
                subCondition.add(getSingleCondition(fieldName, field, value));
            }
            condition = '( ' + String.join(subCondition, ' OR ') + ' ) ';
            if (condition != NULL)
                conditionsList.add(condition);
        }
        return conditionsList;
    }
}