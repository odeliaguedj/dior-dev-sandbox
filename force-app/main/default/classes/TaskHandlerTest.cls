/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TaskHandlerTest {
	@TestSetup
    static void makeData()
    {
        List <Task> tasksToInsert=new List<Task>();
        Task tsk1=new Task();
        tasksToInsert.add(tsk1);
        Task tsk2=new Task(Status__c='Completed');
        tasksToInsert.add(tsk2);
        insert tasksToInsert;
    }

    @isTest
    public static void test() {
        List<Task> tsk1=[SELECT id,Status,Status__c FROM Task WHERE Status__c!='Completed'];
        List<Task> tsk2=[SELECT id,Status,Status__c FROM Task WHERE Status__c='Completed'];
        Test.startTest();
        system.assertEquals('Open', tsk1.get(0).Status);
        system.assertEquals('Completed', tsk2.get(0).Status);
        Test.stopTest();

    }
    static testMethod void setClientLastContactDate() {  
		
		/*ACCOUNT*/
		Account acc1 = new Account(LastName = 'serge',LastContactDate__pc = date.today(),Salutation ='1',FirstName='TEST1');   
    	insert acc1;
    	
    	Account acc2 = new Account(LastName = 'serge',LastContactDate__pc = date.today().addDays(5),Salutation ='2',FirstName='TEST2');   
    	insert acc2;
		
		/*TASK*/
		Task task1 = new Task(Type = 'OTO', MassOutreach__c = false, ActivityDate = date.today().addDays(5), whatid = acc1.Id); 
		insert task1;

		Task task2 = new Task(Type = 'OTO', MassOutreach__c = false, ActivityDate = date.today(), whatid = acc2.Id); 
		insert task2;
    }
    
}