public with sharing class CLT_Utils_Query {
    public static String getClientsQuery(boolean getMyClientsOnly){

        User me = CLT_Utils.getMyUser();
        String zone = me.Zone__c;

        String clientFields = CLT_Utils_Mapping.getFieldsListString('Account','Client');
        
        String clientZoneFields = '';
        if (String.isNotEmpty(zone)){
            clientZoneFields = CLT_Utils_Mapping.getFieldsListString('Account','Client_Zone_' + zone);
        }
        String clientCurrencyFields =  CLT_Utils_Mapping.getFieldsListString('Account','Client_Currency_' + me.DefaultCurrencyIsoCode);

        clientFields += String.isNotEmpty(clientZoneFields) ? ', ' + clientZoneFields : ' ' ;
        clientFields += String.isNotEmpty(clientCurrencyFields) ? ', ' + clientCurrencyFields : ' ' ;
        String ret = 'SELECT ' + clientFields + ' FROM Account WHERE Id != \'\'';//PreferredStore1__pr.StoreCode__c = \'' + myStoreCode + '\' ';
        if (getMyClientsOnly){
            ret += getMyClientsCondition(me);
        }
        ret += ' AND CurrentSegment__pc != null  AND CurrentSegment__pc != \'NO_SEGMENT\' ';
        return ret;
    }

    public static string getMyClientsCondition(User me){
        return ' AND PreferredSA1__pc = \'' + me.Id + '\' ';
    }

    public static string getMyClientsMembersCondition(User me){
        return ' AND Contact.Account.Optin__pc = true  ';
        //return ' AND Contact.PreferredStore1__r.StoreCode__c = \'' + me.BaseStoreCode__c + '\' AND Contact.Account.Contactable__pc = true  ';
    }

    public static string getAccountMyStoreCondition(string storeCode){
        return ' AND Store__pr.StoreCode__c = \'' + storeCode + '\'';
    }
}