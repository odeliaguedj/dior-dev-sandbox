//created by Leah Schachter 14/05/19
@isTest
public with sharing class CLT_CampaignMember_Test {
    @TestSetup
    static void makeData()
    {
        Campaign cmpn = new Campaign(Name='cmpn');
        insert cmpn;

        Contact contact = new Contact(LastName = 'LastName');
        insert contact;

        CampaignMember campaignMember = new CampaignMember(CampaignId=cmpn.Id, CurrencyIsoCode='CAD', ContactId =  contact.Id, Status = 'Completed', AppointmentDateTime__c = System.now(), UniqueUpsertId__c = cmpn.Id);
        System.debug('here:'+cmpn.id);
        insert campaignMember;


        

    }

    @isTest
    public static void test() {
        CampaignMember cmpnMember =[SELECT id , Status, CampaignId FROM CampaignMember LIMIT 1];
        Contact contact =[SELECT id FROM Contact LIMIT 1];
        Map<String, String> params = new Map<String, String> {'objectName' => 'CampaignMember' }; //empty map
        
        List<Object> myList = new List<Object>{new Map<String, Object>{'status'=> cmpnMember.Status, 
        																'clientListId' => cmpnMember.CampaignId ,
        																'contactId'=> contact.Id }};
        //String jsonStr = '[{ "name" : "item1"}]';
        Map<String,Object> body= new Map<String,Object> { 'objectName'=> 'CampaignMember','items'=> myList};
        System.debug('aaaaa body:' + body);
        CLT_CampaignMember cm =new CLT_CampaignMember();

        Test.startTest();
        Object obj1= cm.retrieveDelete(params);
        Object obj2= cm.retrievePost(params,body);
        Object obj3= cm.retrieveGet(params);
        Test.stopTest();

    }
}