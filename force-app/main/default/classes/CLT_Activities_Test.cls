@isTest
private class  CLT_Activities_Test {    
    @isTest static void test_method_one() {
        
        Task tsk=new Task();
        insert tsk;
        
        Map<String, String> params = new Map<String, String>();
        params.put('objectName', 'Task');
        params.put('id', tsk.Id);
         
      List<Object> myListTask = (List<Object>)JSON.deserializeUntyped(
'[ { "client": { "phone": "454545", "turnover": 0, "optInCalling": true, "firstName": "Laura", "title": "Mrs", "nationalityLabel": "ALB", "lastPurchaseAmount": 0, "title_LBL": "Mrs.", "totalItemsBought": 0, "nationalityLabel_LBL": "Albania", "lastName": "Stone" }, "signature": "iVBORw0KGgoAAAAN" } ]');
           
        Map<String,Object> body= new Map<String,Object> { 'objectName'=> 'Task','items'=> myListTask};
            
        List<Object> myList = new List<Object>{new Map<String, Object>{'isAllDayEvent' => true,'durationInMinutes'=>80, 'activityDate' => System.today()}};
        Map<String,Object> body2=new Map<String,Object> { 'objectName'=> 'Event','items'=>myList};
        
        Map<String,Object> body3= new Map<String,Object> { 'objectName'=> 'Task','items'=> myListTask,'runningUser'=> '0050Q000002SM4eQAG'};
        Map<String,Object> body4= new Map<String,Object> { 'objectName'=> 'Event','items'=>myList,'runningUser'=> '0050Q000002SM4eQAG'};
        CLT_Activities cltActivities = new CLT_Activities();
        
        Test.startTest();
        cltActivities.retrieveDelete(params);
       // System.assertEquals(check, true);
        cltActivities.retrieveGet(params);
        cltActivities.retrieveGet(new Map<String, String>{'runningUser' => '0051t000001wvzmAAA'});
        cltActivities.retrievePost(params, body);
        
        cltActivities.retrievePost(params, body2);
        cltActivities.retrievePost(params, body3);
        cltActivities.retrievePost(params, body4);
        Test.stopTest();
    }
    
}