@IsTest
private class CLT_ClientDetail_Test {
	@isTest static void test_method_one() {

        Id personAccountRTId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        Account account = new Account(LastName = 'client',RecordTypeId=personAccountRTId );
		insert account;
        
		CLT_ClientDetail clientDetails = new  CLT_ClientDetail();
		Test.startTest();
		clientDetails.retrieveGet(new Map<String, String>{'clientId' => account.Id});
		clientDetails.retrieveDelete(new Map<String, String>());
		clientDetails.retrievePost(new Map<String, String>(),new Map<String,Object>());

		Test.stopTest();
	}
}