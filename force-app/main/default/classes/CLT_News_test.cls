//Created by Rachel Braverman

@isTest
public class CLT_News_test {
    
    @isTest
    private static void retrieveDeleteReturnNull() {
        final CLT_RetailInterface clt = new CLT_News();
        system.assertEquals(null, clt.retrieveDelete(new Map<String, String>()));
    }
    
    @isTest
    private static void retrievePostReturnNull() {
        final CLT_RetailInterface clt = new CLT_News();
        system.assertEquals(null, clt.retrievePost(new Map<String, String>(), new Map<String, Object>()));
    }

    @isTest
    private static void retrieveGetReturnEmptyMap() {
        final CLT_News clt = new CLT_News();
        
        Test.startTest();
        Map<String, Object> actual = clt.retrieveGet(new Map<String, String>());        
        system.assertEquals(2, actual.size());  
        Test.stopTest();
    }
}