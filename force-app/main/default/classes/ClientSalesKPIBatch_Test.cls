@isTest
global class ClientSalesKPIBatch_Test {
    
    public static String CRON_EXP = '0 0 3 ? * * *';
    
    @testSetup
    private static void setUp() {
        
		TestUtils.createPersonAccounts();
		TestUtils.createTransactionLines();
        
    }
    
    @isTest
    private static void test() {
        ClientSalesKPIBatch batch = new ClientSalesKPIBatch();
        batch.salesDateFilter = System.today();
        Test.startTest();
        Database.executeBatch(batch);
        Test.stopTest();
    }

    public TestMethod static void ClientSalesKPISchedulerTest() {
        Test.startTest();
         String jobId = System.schedule('ClientSalesKPISchedulerTest',CRON_EXP , 
            new ClientSalesKPIScheduler());    
        Test.stopTest();
    }
}