//Tamar s
@isTest
public with sharing class AlgoliaAPIKeyGenTest {
   @isTest
    public static void AlgoliaAPIKeyGenTest() {
                 String scheduleTime = '0 0 0 20 6 ? 2020';

         Test.startTest();
      
         String jobId = System.schedule('Warehouse Time To Schedule to Test', scheduleTime, new AlgoliaAPIKeyGen());
         CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                            FROM CronTrigger WHERE id = :jobId];

         System.assertEquals(scheduleTime, ct.CronExpression);

         System.assertEquals(0, ct.TimesTriggered);

         Test.stopTest();
    }
}