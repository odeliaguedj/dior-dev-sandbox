@isTest
public with sharing class ClientSalesKPISchedulerTest {
    public static String CRON_EXP = '0 0 3 ? * * *';
    public TestMethod static void ClientSalesKPISchedulerTest() {
        Test.startTest();
         String jobId = System.schedule('ClientSalesKPISchedulerTest',CRON_EXP , 
            new ClientSalesKPIScheduler());    
        Test.stopTest();
    }
}