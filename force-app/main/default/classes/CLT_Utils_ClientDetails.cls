///*
// * Created by Israel Zygelboim on 28/02/2019.
// */
public class CLT_Utils_ClientDetails {

    String runningUser;
    
    public static Map<String, Object> getClient(String clientId) {
        Map<String, Object> clients = CLT_Utils_Mapping.getStandardWrapperList('Account', 'Client', 'FROM Account WHERE Id = \'' + clientId + '\'')[0];

        Map<String, Object> clientsZone = getClientZoneData(clientId);
        clients.putAll(clientsZone);

        return clients;
    }

    public static Map<String, Object> getClientFields(String clientId) {
        Map<String, Object> clients = CLT_Utils_Mapping.getStandardWrapperList('Account', 'ClientFields', 'FROM Account WHERE Id = \'' + clientId + '\'')[0];

        String currencyCode = getCurrencyCode();
        Map<String, Object> clientsKpi = CLT_Utils_Mapping.getStandardWrapperList('Account', 'ClientFields_'+ currencyCode, 'FROM Account WHERE Id = \'' + clientId + '\'')[0];
        addUserCurrency(clientsKpi, currencyCode);

        clients.putAll(clientsKpi);
        Map<String, Object> clientsZone = getClientZoneData(clientId);
        clients.putAll(clientsZone);

        return clients;
    }

    public static  List<Map<String, Object>> getAddresses(String clientId){
        return CLT_Utils_Mapping.getStandardWrapperList('ClientAddress__c', 'Addr',
                        'FROM ClientAddress__c WHERE Client__c =  \''+ clientId + '\' ORDER BY CreatedDate DESC');
    }

    public static  List<Map<String, Object>> getRelatives(String clientId){
        return CLT_Utils_Mapping.getStandardWrapperList('FamilyMember__c', 'FamilyMember',
                        'FROM FamilyMember__c WHERE Client__c =  \''+ clientId + '\' ORDER BY CreatedDate DESC');
    }

    public static  List<Map<String, Object>> getPurchases(String clientId){
        return CLT_Utils_Mapping.getStandardWrapperList('TransactionLine__c', 'TransactionLine',
                        'FROM TransactionLine__c WHERE Transaction__r.Client__c =  \''+ clientId + '\' AND Transaction__r.SalesDate__c != null ORDER BY Transaction__r.SalesDate__c DESC');
    }

    public static  List<Map<String, Object>> getCampaignMembers(String contactId){
        return CLT_Utils_Mapping.getStandardWrapperList('CampaignMember', 'CampaignMember',
                        'FROM CampaignMember WHERE ContactId =  \''+ contactId + '\' ORDER BY AppointmentDateTime__c DESC');
    }

    public static List<Object> getClientTimeline(String clientId, String contactId) {
        List<Object> tasks = CLT_Utils_Mapping.getStandardWrapperList('Task', 'Task',
                        'FROM Task WHERE WhatId = \'' + clientId + '\' AND (ActivityDate >= LAST_N_YEARS:5 OR (ActivityDate = NULL AND CreatedDate >= LAST_N_YEARS:5))  ORDER BY ActivityDate, CreatedDate DESC');

        List<Object> events = CLT_Utils_Mapping.getStandardWrapperList('Event', 'Event',
                        'FROM Event WHERE WhatId = \'' + clientId + '\' AND (ActivityDate >= LAST_N_YEARS:5 OR (ActivityDate = NULL AND CreatedDate >= LAST_N_YEARS:5))  ORDER BY ActivityDate, CreatedDate DESC');

        tasks.addAll(events);

        return tasks;
    }

    public static List<Map<String, Object>> getClientNotes(String clientId) {
        string userId = UserInfo.getUserId();

        return CLT_Utils_Mapping.getStandardWrapperList('Note__c','Note',
                        ' FROM Note__c WHERE Client__c = \'' + clientId + '\' AND CreatedById = \'' + userId + '\' ORDER BY LastModifiedDate DESC');
    }

    public static List<Map<String, Object>> getClientAddresses(String clientId) {
        return CLT_Utils_Mapping.getStandardWrapperList('ClientAddress__c', 'ClientAddress',
                        ' FROM ClientAddress__c WHERE Client__c = \'' + clientId + '\' ORDER BY LastModifiedDate DESC');
    }

    //helpers functions
    private static void addUserCurrency(Map<String, Object> values, String currencyCode) {

        if (currencyCode == UserInfo.getDefaultCurrency()){
            return ;
        }
        Map<String,Decimal> userCurrencyValues = new Map<String, Decimal>();
        Decimal conversionFactor = getConversionFactor(currencyCode);

        for (String key : values.keySet()){
            userCurrencyValues.put(key +'Converted', ((Decimal)values.get(key)) * conversionFactor );
        }

        values.putAll(userCurrencyValues);
        values.put('kpiCurrency',currencyCode);
    }

    private static Decimal getConversionFactor(String currencyCode) {
        Decimal fromValue = 1;
        Decimal toValue = 0;

        for (CurrencyType type : [SELECT ConversionRate, IsoCode, IsCorporate
                                    FROM CurrencyType
                                    WHERE IsoCode IN(:UserInfo.getDefaultCurrency(),:currencyCode)]) {
            if (type.IsoCode == currencyCode ){
                fromValue = type.ConversionRate;
            }
            if (type.IsoCode == UserInfo.getDefaultCurrency() ){
                toValue = type.ConversionRate;
            }
        }

        return fromValue == 0 ? fromValue :  toValue / fromValue;
    }

    private static String getCurrencyCode() {
        List<CLT_Currency_Mapping__mdt> cltCountryGlobalSetting = [SELECT KPI_Currency__c
                                                                        FROM CLT_Currency_Mapping__mdt WHERE  User_Currency__c = : UserInfo.getDefaultCurrency()];
        if (!cltCountryGlobalSetting.isEmpty()){
            return cltCountryGlobalSetting.get(0).KPI_Currency__c;
        }
        return 'EUR';
    }

    public static Map<String, Object> getKPI(String clientId, String mainStoreId) {
        Map<String, Object> ret = new Map<String, Object>();

        List<AggregateResult> transactionResult = [SELECT MAX(SalesAmountIncludingTax__c) maxAmount, AVG(SalesAmountIncludingTax__c) avgAmount, AVG(No_Of_Products__c) upt
                                        FROM Transaction__c
                                        WHERE Client__c = :clientId];
        List<AggregateResult> mainStoreResult = [SELECT MAX(SalesAmountIncludingTax__c) maxAmount, AVG(SalesAmountIncludingTax__c) avgAmount, AVG(No_Of_Products__c) upt
                                        FROM Transaction__c
                                        WHERE Client__c = :clientId AND Store__c = :mainStoreId];
        List<AggregateResult> productsResult = [SELECT MAX(SalesAmountIncludingTax__c) maxAmount FROM TransactionLine__c WHERE Client__c = :clientId];

        List<AggregateResult> mainStoreLast12Months = [SELECT SUM(SalesAmountIncludingTax__c) amount
                                                        FROM TransactionLine__c
                                                        WHERE Client__c = :clientId AND Store__c = :mainStoreId AND SalesDate__c = LAST_N_MONTHS:12];
        System.debug(clientId);
        System.debug(transactionResult);
        System.debug(mainStoreResult);
        System.debug(productsResult);
        System.debug(mainStoreLast12Months);
        ret.put('maxTransactionPrice',!transactionResult.isEmpty() ? transactionResult.get(0).get('maxAmount'): 0);
        ret.put('maxProductPrice',!productsResult.isEmpty() ? productsResult.get(0).get('maxAmount') : 0);
        //ret.put('avgBsket', !transactionResult.isEmpty() ? transactionResult.get(0).get('avgAmount') : 0);
        ret.put('upt', !transactionResult.isEmpty() ? transactionResult.get(0).get('upt') : 0);
        ret.put('turnoverLast12Months', 'from fields :)');
        ret.put('mainStoreAvgBsket', !mainStoreResult.isEmpty() ? mainStoreResult.get(0).get('avgAmount') : 0);
        ret.put('mainStoreUpt', !mainStoreResult.isEmpty() ? mainStoreResult.get(0).get('upt') : 0);
        ret.put('mainStoreTurnoverLast12Months', !mainStoreLast12Months.isEmpty() ? mainStoreLast12Months.get(0).get('amount') : 0);

        return ret;
    }
    public static Map<String, Object> getClientZoneData(String clientId) {
        String zone = CLT_Utils.getMyUser().Zone__c;

        if (String.isEmpty(zone)){
            return new Map<String, Object>();
        }

        Map<String, Object> clientsZone = CLT_Utils_Mapping.getStandardWrapperList('Account', 'Client_Zone_' + zone,
                        'FROM Account WHERE Id = \'' + clientId + '\'')[0];

        return clientsZone;
    }
}