//Tamar S
@isTest
private class UserStoreHandlerTest {   

    static testMethod void updateUserStoresTest() {
        
        /*STORE*/
        Store__c store1 = new Store__c (Name='HUGO', CreatedById = UserInfo.getUserId(),StoreCode__c = '123');
        insert store1;
        
        Store__c store2 = new Store__c (Name='JEAN', CreatedById = UserInfo.getUserId(),StoreCode__c = '456');
        insert store2;

        Store__c store3 = new Store__c (Name='Test', CreatedById = UserInfo.getUserId(),StoreCode__c = '789');
        insert store3;

        Store__c store4 = new Store__c (Name='Test 2', CreatedById = UserInfo.getUserId(),StoreCode__c = '1011');
        insert store4;
        		
        /*USER-STORE*/
        UserStore__c userStore1 = new UserStore__c(Store__c = store1.Id, User__c = UserInfo.getUserId(), CurrentStore__c = true);
        insert userStore1;
        
        UserStore__c userStore2 = new UserStore__c(Store__c = store2.Id, User__c = UserInfo.getUserId(), CurrentStore__c = true);
        insert userStore2;




        Set<Id> userIds = new Set<Id>();
		Set<Id> userStoreIds = new Set<Id>();
        List<UserStore__c> userStoresToProcess = new List<UserStore__c>();
        userStoresToProcess.add(userStore1);
        userStoresToProcess.add(userStore2);
        userIds.add(userStore1.User__c);
        userIds.add(userStore2.User__c);
        userStoreIds.add(userStore1.Id);
        userStoreIds.add(userStore2.Id);


        Map<Id,User> users = new Map<Id,User> ([SELECT  Id, BaseStoreCode__c ,(select Id,User__c,CurrentStore__c,Store__c FROM Stores__r 
																														WHERE CurrentStore__c = true 
																														AND Id not in :userStoreIds) 
												FROM User WHERE Id IN :userIds  ]);

        boolean check = true;
         for(UserStore__c us : userStoresToProcess)
         {
            User tempUser = users.get(us.User__c);
             for(UserStore__c st : tempUser.Stores__r)
             {
                 if(st.CurrentStore__c == true)
                    check = false;           
             }
         }                                       
    System.assertEquals(check,true);

    }
}