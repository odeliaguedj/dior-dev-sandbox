/*
This class manages the HTTPS requests of the user object web service
 */
public with sharing class CLT_User implements clt_retailInterface{
    
    public Object retrieveDelete(Map<String, String> params){
        return null;
    }
    
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }

    public Object retrieveGet(Map<String, String> params){
        String storeId = params.get('storeId');
        return CLT_Utils_Mapping.getStandardWrapperList('User', 'User', ' FROM User Where BaseStoreCode__c =\''+storeId +'\'');
    }
}