/*
* This class manages the HTTPS requests to the configuration data web service
*/

public with sharing class CLT_Config implements clt_retailInterface{
    
    
    //on delete requset
    public Object retrieveDelete(Map<String, String> params){
        return null;    
    }
    //on post
    public Object retrievePost(Map<String, String> params, Map<String,Object> body){
        return null;
    }
	//on get requset -build Wrapper object to return
    public Object retrieveGet(Map<String, String> params){
        return new ConfigWrapper(getPicklists(params), getMappings(), getProductCatalogIndices(), getProductCatalogFacets(), getProductCatalogFilters(), getSalesAssociates(), getActiveSOQL(), getEmailTemplatesFields(), getUserInfos(params));
    }

    public list<clt_UtilsPicklist.PicklistWrapper> getPicklists(Map<String, String> params){
        List<String> picklistNameItems = new list<String>();
        String picklistsNames = params.get('picklists');    
        if(picklistsNames != null){
            picklistNameItems = picklistsNames.split(';');
        }

        return clt_UtilsPicklist.getPicklists(picklistNameItems);
    }

    public List<Map<String, Object>> getMappings(){
       return CLT_Utils_Mapping.getStandardWrapperList('CLT_FieldsMapping__mdt', 'CLT_FieldsMapping__mdt', 'FROM CLT_FieldsMapping__mdt WHERE Send_to_Clienteling_App__c = true');
    }
    
    public List<Map<String, Object>> getProductCatalogIndices(){
        return CLT_Utils_Mapping.getStandardWrapperList('Product_Catalog_Country_Index__mdt', 'Product_Catalog_Country_Index__mdt', 'FROM Product_Catalog_Country_Index__mdt');
    }
    
    public List<Map<String, Object>> getProductCatalogFacets(){
        return CLT_Utils_Mapping.getStandardWrapperList('Product_Catalog_Featured_Facet__mdt', 'Product_Catalog_Featured_Facet__mdt', 'FROM Product_Catalog_Featured_Facet__mdt');
    }

    public List<Map<String, Object>> getProductCatalogFilters(){
        return CLT_Utils_Mapping.getStandardWrapperList('Product_Catalog_Featured_Filter__mdt', 'Product_Catalog_Featured_Filter__mdt', 'FROM Product_Catalog_Featured_Filter__mdt');
    }

    public List<Map<String, Object>> getSalesAssociates(){
        string myStoreCode = CLT_Utils.getUserStoreCode();  

        return CLT_Utils_Mapping.getStandardWrapperList('User', 'User', ' FROM User Where BaseStoreCode__c =\'' + myStoreCode + '\'');
    }

    public List<Map<String,String>> getActiveSOQL(){
        List<Map<String,String>> queries = new List<Map<String,String>>();
        
        queries.add(new Map<String,String>{
            'query' => CLT_Utils_Query.getClientsQuery(true),
            'name' => 'Clients',
            'id' => 'clientQuery'
        });   
        return queries;     
    }

    public List<Map<String,String>> getEmailTemplatesFields(){
        List<CLT_EmailTemplateField__mdt> fields = [
            SELECT Id, Object__c, FieldWrapperName__c, StringToReplace__c 
            FROM CLT_EmailTemplateField__mdt 
            WHERE Active__c = TRUE
        ];
        List<Map<String,String>> mappingFields = new List<Map<String,String>>();
        
        for(CLT_EmailTemplateField__mdt field: fields) {
            mappingFields.add(new Map<String,String>{
                'id' => field.Id,
                'object' => field.Object__c,
                'field' => field.FieldWrapperName__c,
                'strToReplace' => field.StringToReplace__c
            });
        }
           
        return mappingFields;     
    }

    public Map<String, Object> getUserInfos(Map<String, String> params){
        APIManagerConstants__c APIManagerConstants = APIManagerConstants__c.getOrgDefaults();
        ProductCatalog__c ProductCatalog = ProductCatalog__c.getOrgDefaults();

        string myStoreCode = CLT_Utils.getUserStoreCode();  
        String UserId=CLT_Utils.checkRunningUser(params);
        Map<String, Object> resultMapping = CLT_Utils_Mapping.getStandardWrapperList('User', 'User', 'FROM User WHERE Id = \'' + UserId + '\'')[0];

        if(String.IsNotEmpty(myStoreCode)){
            List<Map<String, Object>> storeMappingList = CLT_Utils_Mapping.getStandardWrapperList('Store__c', 'Store__c', 'FROM Store__c WHERE StoreCode__c = \'' + myStoreCode + '\'');
            if(storeMappingList.size()  > 0){
                Map<String, Object> storeMapping = storeMappingList[0];
                resultMapping.putAll(storeMapping);
            }
        }

        resultMapping.put('APIManagerApimSubscriptionKey', APIManagerConstants.ApimSubscriptionKey__c);
        resultMapping.put('APIManagerApimTrace', APIManagerConstants.ApimTrace__c);
        resultMapping.put('APIManagerClientID', APIManagerConstants.ClientID__c);
        resultMapping.put('APIManagerClientSecret', APIManagerConstants.ClientSecret__c);
        resultMapping.put('APIManagerLoginURL', APIManagerConstants.loginURL__c);
        resultMapping.put('APIManagerResource', APIManagerConstants.Resource__c);
        resultMapping.put('APIManagerURL', APIManagerConstants.URL__c);
        
        resultMapping.put('ProductCatalogAlgoliaAPIKey', ProductCatalog.AlgoliaAPIKey__c);
        resultMapping.put('ProductCatalogAlgoliaApplicationID', ProductCatalog.AlgoliaApplicationID__c);
        resultMapping.put('ProductCatalogAPIDiorcomStockURL', ProductCatalog.APIDiorcomStockURL__c);
        resultMapping.put('ProductCatalogDivisionsImages', ProductCatalog.DivisionsImages__c);
       
        
        
        return resultMapping;
    }

 	// ConfigWrapper object
    public class ConfigWrapper{
        public List<clt_UtilsPicklist.PicklistWrapper> picklists {get; set;}
        public List<Map<String, Object>> mappings {get; set;}
        public List<Map<String, Object>> productCatalogIndices {get; set;}
        public List<Map<String, Object>> productCatalogFacets {get; set;}
        public List<Map<String, Object>> productCatalogFilters {get; set;}
        public List<Map<String, Object>> salesAssociates {get; set;}
        public List<Map<String, String>> queries {get;set;}
        public List<Map<String, String>> emailTemplatesFields {get;set;}
        public Map<String, Object> userInfos {get;set;}

        //Constructor
        public ConfigWrapper(list<clt_UtilsPicklist.PicklistWrapper> picklists, List<Map<String, Object>> mappings,  List<Map<String, Object>> productCatalogIndices,  List<Map<String, Object>> productCatalogFacets,  List<Map<String, Object>> productCatalogFilters, List<Map<String, Object>> salesAssociates, List<Map<String,String>> queries, List<Map<String,String>> emailTemplatesFields, Map<String,object> userInfos){
            this.picklists = picklists;
            this.mappings = mappings;
            this.productCatalogIndices = productCatalogIndices;
            this.productCatalogFacets = productCatalogFacets;
            this.productCatalogFilters = productCatalogFilters;
            this.salesAssociates = salesAssociates;
            this.queries = queries;
            this.emailTemplatesFields = emailTemplatesFields;
            this.userInfos = userInfos;
        }
    } 
}