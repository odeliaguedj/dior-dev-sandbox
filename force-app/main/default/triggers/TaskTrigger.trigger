trigger TaskTrigger on Task (after insert, after update, before insert, before update) {
 
	if (Utils.canTrigger('TaskTrigger')) {
		if(Trigger.isBefore ){
			TaskHandler.setTaskFields(trigger.new);
				
		}
			
		else if (TRigger.isAfter){
			TaskHandler.setClientLastContactDate(trigger.new);
		}
	}

}