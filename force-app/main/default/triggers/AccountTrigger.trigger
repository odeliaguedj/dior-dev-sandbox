trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
	if (Trigger.isBefore){
		AccountHandler.updateClientFields(trigger.new, trigger.oldMap, true);
	}
	if (Trigger.isAfter && (!System.isFuture() && !System.isBatch())) {
		CLT_Utils_Client.sendClientsToDataHubFuture(Trigger.new.get(0).Id);
	}

}