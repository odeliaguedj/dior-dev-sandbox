trigger CampaignTrigger on Campaign (before insert, before update, after insert, after update) {
    
    if(trigger.isBefore){
    	if(trigger.isInsert){
    		CampaignHandler.updateStoreCampaignFields(trigger.new);
    	}
    	CampaignHandler.updateCampaignFields(trigger.new);  
    }
	if (trigger.isAfter && trigger.isUpdate){
		system.debug('\n\n\n\n     YEHIEL        \n\n\n\n'  );
		CampaignHandler.updateParentCampaigns(trigger.new, trigger.oldMap);
		//CampaignHandler.createCampaignSharingOnInsert(trigger.new);
	}
	
	if (trigger.isAfter && trigger.isInsert){
		system.debug('\n\n\n\n     YEHIEL        \n\n\n\n'  );
		CampaignHandler.createCampaignSharingOnInsert(trigger.new);
	}

}