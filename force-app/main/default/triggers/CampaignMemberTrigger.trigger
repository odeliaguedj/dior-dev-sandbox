trigger CampaignMemberTrigger on CampaignMember (before insert,before update,after update) {
    
    if(Trigger.isBefore){
	    CampaignMemberHandler.setCampaignMembersFields(trigger.new,trigger.oldMap);
    }
    
    if(Trigger.isUpdate && Trigger.isAfter && !EventHandler.eventTriggerContext){
        CampaignMemberHandler.updateOTOAppointments(trigger.new, trigger.oldMap);
    }

       
   
}