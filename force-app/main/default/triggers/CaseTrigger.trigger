trigger CaseTrigger on Case (before insert, before update) {
	if(trigger.isBefore) {
        if(trigger.IsInsert) {
            // To get priority field from the parent Case.
            CaseHandler.getCasePriority(trigger.new, null);
            CaseHandler.setCaseFieldsOnInsert(trigger.new);
            CaseHandler.fillFieldsFromProductData(trigger.new, null);
        }
        if(trigger.IsUpdate) {
            // To get priority field from the parent Case.
            CaseHandler.getCasePriority(trigger.new, trigger.oldmap);
            CaseHandler.fillFieldsFromProductData(trigger.new, trigger.oldMap);
        }
    }
}