trigger UserStoreTrigger on UserStore__c (after update, after insert) {
    UserStoreHandler.updateUserStores (trigger.new, trigger.oldMap);
}