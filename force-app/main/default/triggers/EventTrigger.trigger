trigger EventTrigger on Event (before insert, before update, after insert, after update, after delete) {
    
    if(Trigger.isBefore){
        EventHandler.setEventFields(trigger.new);
    }
    else if(Trigger.isAfter){
        if  ( Trigger.isInsert  ){
            if(!CampaignMemberHandler.campaignMemberTriggerContext){
                EventHandler.upsertOTOs(trigger.new);
            }
        }
        else if(Trigger.isUpdate){
            if(!CampaignMemberHandler.campaignMemberTriggerContext){
                EventHandler.upsertOTOs(trigger.new);
            }

            EventHandler.updatePurchaseCampaignMembers(trigger.new, trigger.oldMap);
        }
        else if(Trigger.isDelete){
        	if(!CampaignMemberHandler.campaignMemberTriggerContext){
            	EventHandler.updateOrDeleteOTOs(trigger.old);
        	}
	        EventHandler.updatePurchaseCampaignMembers(trigger.old, null);
        }
    }

   

}